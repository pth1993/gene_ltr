import numpy as np
from scipy.spatial.distance import cdist
from scipy.stats import spearmanr, pearsonr
from collections import Counter


def get_drug_list(input_file):
    with open(input_file) as f:
        drug_list = f.readline().strip().split(',')
    return drug_list


def get_l1000_gene(input_file):
    with open(input_file) as f:
        l1000_gene = f.readline().strip().split(',')[5:]
    return  l1000_gene


def read_drug_signature(input_file, idx):
    drug_sig = []
    with open(input_file, 'r') as f:
        for line in f:
            line = line.strip().split(',')
            line = [float(i) for i in line]
            drug_sig.append(line)
    drug_sig = np.array(drug_sig)
    drug_sig = drug_sig[:, idx]
    return drug_sig


def read_hq_drug_signature(input_file, idx):
    drug_sig = []
    drug_id = []
    with open(input_file, 'r') as f:
        for line in f:
            line = line.strip().split(',')
            drug_id.append(line[0])
            line = [float(i) for i in line[1:]]
            drug_sig.append(line)
    drug_sig = np.array(drug_sig)
    drug_sig = drug_sig[:, idx]
    return drug_sig, drug_id


def get_disease_signature(input_file, l1000_gene):
    idx = []
    fc = []
    fcse = []
    with open(input_file, 'r') as f:
        for line in f:
            line = line.strip().split(',')
            g = line[0].strip('"')
            if g in l1000_gene and 'NA' not in line:
                id = l1000_gene.index(g)
                idx.append(id)
                fc.append(float(line[2]))
                fcse.append(float(line[3]))
    return idx, fc, fcse


def get_disease_signature_all(input_file, l1000_gene):
    idx = []
    fc = []
    fcse = []
    with open(input_file, 'r') as f:
        for line in f:
            line = line.strip().split(',')
            g = line[7]
            if g in l1000_gene and 'NA' not in line:
                id = l1000_gene.index(g)
                idx.append(id)
                fc.append(float(line[2]))
                fcse.append(float(line[3]))
    return idx, fc, fcse


def compute_correlation_old(drug_sig, disease_sig):
    return 1 - cdist(drug_sig, disease_sig, metric='correlation')


def compute_correlation(drug_sig, disease_sig):
    drug_sig = np.array(drug_sig)
    disease_sig = np.array(disease_sig)
    out = np.zeros((len(drug_sig), len(disease_sig)), dtype=np.float)
    for i, d1 in enumerate(drug_sig):
        for j, d2 in enumerate(disease_sig):
            out[i, j] = spearmanr(d1, d2)[0]
    return out


def get_candidate_drug_id(correlation, gsea, drug_list, num_candidate):
    # tmp = drug_list.index('DB00503')
    # print(tmp)
    drug_list = np.array(drug_list)
    correlation_candidate_idx = np.argsort(correlation[:,0])[:num_candidate]
    gsea_candidate_idx = np.argsort(gsea)[:num_candidate]
    # print(np.argsort(np.argsort(correlation[:, 0]))[906])
    return drug_list[correlation_candidate_idx].tolist(), drug_list[gsea_candidate_idx].tolist()


def get_gsea_score(predicted_file, hq_file):
    score = []
    with open(predicted_file, 'r') as f:
        for line in f:
            score.append(float(line.strip()))
    with open(hq_file, 'r') as f:
        for line in f:
            score.append(float(line.strip()))
    return np.array(score)


def compare_signature(ind_file, all_file, l1000_gene):
    ind_sig = dict()
    with open(ind_file, 'r') as f:
        f.readline()
        for line in f:
            line = line.strip().split(',')
            if 'NA' not in line:
                ind_sig[line[0].replace('"', '')] = float(line[2])
    all_sig = dict()
    with open(all_file, 'r') as f:
        f.readline()
        for line in f:
            line = line.strip().split(',')
            if 'NA' not in line:
                all_sig[line[7]] = float(line[2])
    common_gene = set(ind_sig.keys()).intersection(set(all_sig.keys())).intersection(set(l1000_gene))
    common_gene = sorted(list(common_gene))
    ind_sig_new = []
    all_sig_new = []
    for g in common_gene:
        ind_sig_new.append(ind_sig[g])
        all_sig_new.append(all_sig[g])
    print(spearmanr(ind_sig_new, all_sig_new)[0])


# l1000_gene = get_l1000_gene('data/signature_0_7.csv')
# compare_signature('data/covid_data/covid_signature.csv', 'data/covid_data/covid_signature_all.csv', l1000_gene)


if __name__ == '__main__':
    filter = {"time": "24H", "pert_id": ['BRD-U41416256', 'BRD-U60236422'], "pert_type": ["trt_cp"],
              "cell_id": ["MCF7", "A375", "HT29", "PC3", "HA1E", "YAPC", "HELA", "A549"],
              "pert_idose": ["0.04 um", "0.12 um", "0.37 um", "1.11 um", "3.33 um", "10.0 um"]}
    drug_sig_file_list = ['data/covid_data/predicted_a375.csv', 'data/covid_data/predicted_a549.csv',
                          'data/covid_data/predicted_ha1e.csv', 'data/covid_data/predicted_hela.csv',
                          'data/covid_data/predicted_ht29.csv', 'data/covid_data/predicted_mcf7.csv',
                          'data/covid_data/predicted_pc3.csv', 'data/covid_data/predicted_yapc.csv']
    hq_drug_sig_file_list = ['data/covid_data/hq_a375.csv', 'data/covid_data/hq_a549.csv',
                             'data/covid_data/hq_ha1e.csv', 'data/covid_data/hq_hela.csv',
                             'data/covid_data/hq_ht29.csv', 'data/covid_data/hq_mcf7.csv',
                             'data/covid_data/hq_pc3.csv', 'data/covid_data/hq_yapc.csv']
    predicted_gsea_file_list = ['data/covid_data/predicted_a375_GSEA.csv', 'data/covid_data/predicted_a549_GSEA.csv',
                                'data/covid_data/predicted_ha1e_GSEA.csv', 'data/covid_data/predicted_hela_GSEA.csv',
                                'data/covid_data/predicted_ht29_GSEA.csv', 'data/covid_data/predicted_mcf7_GSEA.csv',
                                'data/covid_data/predicted_pc3_GSEA.csv', 'data/covid_data/predicted_yapc_GSEA.csv']
    hq_gsea_file_list = ['data/covid_data/hq_a375_GSEA.csv', 'data/covid_data/hq_a549_GSEA.csv',
                         'data/covid_data/hq_ha1e_GSEA.csv', 'data/covid_data/hq_hela_GSEA.csv',
                         'data/covid_data/hq_ht29_GSEA.csv', 'data/covid_data/hq_mcf7_GSEA.csv',
                         'data/covid_data/hq_pc3_GSEA.csv', 'data/covid_data/hq_yapc_GSEA.csv']
    l1000_gene = get_l1000_gene('data/signature_0_7.csv')
    drug_list = get_drug_list('data/covid_data/covid-19_drug_list_refine.csv')
    # idx, fc, fcse = get_disease_signature('data/covid_data/covid_signature.csv', l1000_gene)
    idx, fc, fcse = get_disease_signature_all('data/covid_data/covid_signature_all.csv', l1000_gene)
    # drug_sig = read_drug_signature('data/covid_data/predicted_a549.csv', idx)
    # gt_sig, drug_list_gt = get_ground_truth_signature('data/signature_0_7.csv', filter, idx)
    # correlation = compute_correlation(gt_sig, [fc, fcse])
    # drug_sig, exp_info = get_ground_truth_signature('data/signature_0_7.csv', filter)
    # print(len(exp_info))

    output_total_correlation = []
    output_total_gsea = []
    for f1, f2, f3, f4 in zip(drug_sig_file_list, hq_drug_sig_file_list, predicted_gsea_file_list, hq_gsea_file_list):
        drug_sig = read_drug_signature(f1, idx)
        hq_drug_sig, hq_drug_list = read_hq_drug_signature(f2, idx)
        drug_list = drug_list + hq_drug_list
        drug_sig = np.concatenate((drug_sig, hq_drug_sig), axis=0)
        print(np.shape(drug_sig))
        correlation = compute_correlation(drug_sig, [fc, fcse])
        # print(np.min(correlation[:,0]))
        gsea = get_gsea_score(f3, f4)
        output_correlation, output_gsea = get_candidate_drug_id(correlation, gsea, drug_list, num_candidate=100)
        output_total_correlation += output_correlation
        output_total_gsea += output_gsea
    print(Counter(output_total_correlation))
    # print(Counter(output_total_gsea))
