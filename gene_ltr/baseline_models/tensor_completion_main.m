% addpath
% cd tensor_completion;
currentFolder = pwd;
addpath(genpath(strcat(currentFolder, '/tensor_completion')));
%% Data preparation
data = load('tensor_completion/data_high_quality_tradition.mat')
T = data.data;
W = data.index;
X=T.*W;
r=[5, 5, 5];
%% Run two algorithms
% TTWOPT- quick convergence
%[X_hat_wopt,G_wopt]=T3C(X,W,'Alg','TTWOPT','Rank',r,'MaxIter', 1e3,'Tol', 1e-4);
% TTSGD - able to deal large-scale data and low complexity
[X_hat_sgd,G_sgd]=T3C(X,W,'Alg','TTSGD','Rank',r,'MaxIter', 5e7,'Tol', 1e-4);
% save data
save('tensor_completion/out_high_quality_tradition_5.mat','X_hat_sgd');
