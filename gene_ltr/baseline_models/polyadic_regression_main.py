import numpy as np
import scipy.io as sio
from sklearn.metrics import mean_squared_error
from scipy.stats import pearsonr, spearmanr


def read_drug(input_file, drug_length):
    with open(input_file, 'r') as f:
        drug = dict()
        for line in f:
            line = line.strip().split(',')
            assert len(line) == drug_length + 1, "Wrong format"
            bin_vec = [float(i) for i in line[2:]]
            drug[line[0]] = bin_vec
    return drug


def read_gene(input_file):
    with open(input_file, 'r') as f:
        gene = []
        for line in f:
            line = line.strip().split(',')
            assert len(line) == 129, "Wrong format"
            gene.append([float(i) for i in line[1:]])
    return np.transpose(np.asarray(gene, dtype=np.float64))


def choose_mean_example(examples):
    num_example = len(examples)
    mean_value = (num_example - 1) / 2
    indexes = np.argsort(examples, axis=0)
    indexes = np.argsort(indexes, axis=0)
    indexes = np.mean(indexes, axis=1)
    distance = (indexes - mean_value)**2
    index = np.argmin(distance)
    return examples[index]


def read_data_2_matlab(input_file, filter, drug, gene):
    data = dict()
    pert_id = []
    cell_id = []
    with open(input_file, 'r') as f:
        f.readline()
        for line in f:
            line = line.strip().split(',')
            assert len(line) == 983, "Wrong format"
            if filter["time"] in line[0] and line[1] not in filter["pert_id"] and line[2] in filter["pert_type"] \
                    and line[3] in filter["cell_id"] and line[4] in filter["pert_idose"]:
                pert_id.append(line[1])
                cell_id.append(line[3])
                ft = ','.join(line[1:5])
                lb = [float(i) for i in line[5:]]
                try:
                    data[ft].append(lb)
                except KeyError:
                    data[ft] = [lb]
    pert_id = sorted(list(set(pert_id)))
    cell_id = sorted(list(set(cell_id)))

    drug_ft = []
    for id in pert_id:
        drug_ft.append(drug[id])
    drug_ft = np.transpose(np.asarray(drug_ft, dtype=np.float64))
    index = []
    for i in range(len(drug_ft)):
        if set(drug_ft[i]) == {0.0, 1.0}:
            index.append(i)
    drug_ft = drug_ft[index]
    print('Drug feature shape: %s' % str((np.shape(drug_ft))))

    pert_id_dict = dict(zip(pert_id, list(range(len(pert_id)))))
    cell_id_dict = dict(zip(cell_id, list(range(len(cell_id)))))

    matlab_data_train = []
    matlab_data_dev = []
    matlab_data_test = []
    num_pert_id = len(pert_id)
    fold_size = int(num_pert_id/5)
    train_pert = pert_id[:fold_size * 3]
    dev_pert = pert_id[fold_size*3: fold_size*4]
    test_pert = pert_id[fold_size * 4:]

    for ft, lb in sorted(data.items()):
        ft = ft.split(',')
        if ft[0] in pert_id_dict.keys():
            pert_id = pert_id_dict[ft[0]]
            cell_id = cell_id_dict[ft[2]]
            if len(lb) == 1:
                if ft[0] in train_pert:
                    for i in range(978):
                        matlab_data_train.append([pert_id+1, cell_id+1, i+1, lb[0][i]])
                elif ft[0] in dev_pert:
                    for i in range(978):
                        matlab_data_dev.append([pert_id+1, cell_id+1, i+1, lb[0][i]])
                elif ft[0] in test_pert:
                    for i in range(978):
                        matlab_data_test.append([pert_id+1, cell_id+1, i+1, lb[0][i]])
            else:
                if ft[0] in train_pert:
                    for i in range(978):
                        matlab_data_train.append([pert_id+1, cell_id+1, i+1, choose_mean_example(lb)[i]])
                elif ft[0] in dev_pert:
                    for i in range(978):
                        matlab_data_dev.append([pert_id+1, cell_id+1, i+1, choose_mean_example(lb)[i]])
                elif ft[0] in test_pert:
                    for i in range(978):
                        matlab_data_test.append([pert_id+1, cell_id+1, i+1, choose_mean_example(lb)[i]])
    matlab_data_train = np.array(matlab_data_train, dtype=np.float64)
    matlab_data_dev = np.array(matlab_data_dev, dtype=np.float64)
    matlab_data_test = np.array(matlab_data_test, dtype=np.float64)
    print(np.shape(matlab_data_train))
    print(np.shape(matlab_data_dev))
    print(np.shape(matlab_data_test))
    output = dict()
    output['data_train'] = matlab_data_train
    output['data_dev'] = matlab_data_dev
    output['data_test'] = matlab_data_test
    output['drug'] = drug_ft
    output['gene'] = gene
    output['cell'] = np.identity(len(filter["cell_id"]))
    sio.savemat('../data/data_high_quality.mat', output)


def load_data_from_matlab(input_file_1, input_file_2):
    predict = sio.loadmat(input_file_1)['Y_test_out'][:, 0].reshape(-1, 978)
    original = sio.loadmat(input_file_2)['data_test'][:, 3].reshape(-1, 978)
    # print(sio.loadmat(input_file_2)['data_test'][:,1])
    return original, predict


def precision_k(label_test, label_predict, k):
    num_pos = 100
    num_neg = 100
    label_test = np.argsort(label_test, axis=1)
    label_predict = np.argsort(label_predict, axis=1)
    precision_k_neg = []
    precision_k_pos = []
    neg_test_set = label_test[:, :num_neg]
    pos_test_set = label_test[:, -num_pos:]
    neg_predict_set = label_predict[:, :k]
    pos_predict_set = label_predict[:, -k:]
    for i in range(len(neg_test_set)):
        neg_test = set(neg_test_set[i])
        pos_test = set(pos_test_set[i])
        neg_predict = set(neg_predict_set[i])
        pos_predict = set(pos_predict_set[i])
        precision_k_neg.append(len(neg_test.intersection(neg_predict)) / k)
        precision_k_pos.append(len(pos_test.intersection(pos_predict)) / k)
    return np.mean(precision_k_neg), np.mean(precision_k_pos)


def rmse(label_test, label_predict):
    return mean_squared_error(label_test, label_predict, squared=False)


def correlation(label_test, label_predict, correlation_type):
    if correlation_type == 'pearson':
        corr = pearsonr
    elif correlation_type == 'spearman':
        corr = spearmanr
    else:
        raise ValueError("Unknown correlation type: %s" % correlation_type)
    score = []
    for lb_test, lb_predict in zip(label_test, label_predict):
        score.append(corr(lb_test, lb_predict)[0])
    return np.mean(score)


if __name__ == '__main__':
    filter = {"time": "24H", "pert_id": ['BRD-U41416256', 'BRD-U60236422'], "pert_type": ["trt_cp"],
              "cell_id": ["A375", "HT29", "MCF7", "PC3", "HA1E", "YAPC", "HELA"],
              "pert_idose": ["10.0 um"]}
    drug = read_drug('../data/drugs_pubchem.csv', 881)
    gene = read_gene('../data/gene_vector.csv')
    read_data_2_matlab('../data/signature_0_7.csv', filter, drug, gene)
    precision_degree = [10, 20, 50, 100]
    original, predict = load_data_from_matlab('polyadic_regression/out_high_quality_1500.mat',
                                              'polyadic_regression/data_high_quality.mat')
    print(np.shape(original))
    print(original[0], predict[0])
    rmse = rmse(original, predict)
    print('RMSE: %.4f' % rmse)
    pearson = correlation(original, predict, 'pearson')
    print('Pearson\'s correlation: %.4f' % pearson)
    spearman = correlation(original, predict, 'spearman')
    print('Spearman\'s correlation: %.4f' % spearman)
    for k in precision_degree:
        precision_neg, precision_pos = precision_k(original, predict, k)
        print("Precision@%d Positive: %.4f" % (k, precision_pos))
        print("Precision@%d Negative: %.4f" % (k, precision_neg))
