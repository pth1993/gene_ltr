import numpy as np
import scipy.io as sio
import subprocess
import shlex
from sklearn.metrics import mean_squared_error
from scipy.stats import pearsonr, spearmanr
from sklearn.model_selection import train_test_split


# with open('../data/train.txt', 'r') as f:
#     data = dict()
#     for line in f:
#         line = line.strip().split()
#         try:
#             data[line[1]].append(int(line[0]))
#         except:
#             data[line[1]] = [int(line[0])]
# a = []
# b = []
# for key in data:
#     tmp = data[key]
#     tmp = np.bincount(tmp)
#     a.append(sum(tmp[1:]/sum(tmp)))
#     b.append(sum(tmp))
# print(np.mean(a))
# print(np.min(b))


def choose_mean_example(examples):
    num_example = len(examples)
    mean_value = (num_example - 1) / 2
    indexes = np.argsort(examples, axis=0)
    indexes = np.argsort(indexes, axis=0)
    indexes = np.mean(indexes, axis=1)
    distance = (indexes - mean_value)**2
    index = np.argmin(distance)
    return examples[index]


def read_data_2_matlab(train_file, test_file, dev_pert_id, test_pert_id, filter):
    data_train = dict()
    data_test = dict()
    train_pert_id = []
    cell_id = []
    pert_idose = []
    with open(train_file, 'r') as f:
        f.readline()
        for line in f:
            line = line.strip().split(',')
            assert len(line) == 983, "Wrong format"
            if filter["time"] in line[0] and line[1] not in (filter["pert_id"] + dev_pert_id + test_pert_id) \
                    and line[2] in filter["pert_type"] and line[3] in filter["cell_id"] and line[4] in \
                    filter["pert_idose"]:
                train_pert_id.append(line[1])
                cell_id.append(line[3])
                pert_idose.append(line[4])
                ft = ','.join(line[1:5])
                lb = [float(i) for i in line[5:]]
                try:
                    data_train[ft].append(lb)
                except KeyError:
                    data_train[ft] = [lb]

    with open(test_file, 'r') as f:
        f.readline()
        for line in f:
            line = line.strip().split(',')
            assert len(line) == 983, "Wrong format"
            if filter["time"] in line[0] and line[1] in test_pert_id and line[2] in filter["pert_type"] \
                    and line[3] in filter["cell_id"] and line[4] in filter["pert_idose"]:
                ft = ','.join(line[1:5])
                lb = [float(i) for i in line[5:]]
                try:
                    data_test[ft].append(lb)
                except KeyError:
                    data_test[ft] = [lb]
    train_pert_id = list(set(train_pert_id))
    pert_id = sorted(train_pert_id + test_pert_id)
    cell_id = sorted(list(set(cell_id)))
    pert_idose = sorted(list(set(pert_idose)))
    pert_id_dict = dict(zip(pert_id, list(range(len(pert_id)))))
    cell_id_dict = dict(zip(cell_id, list(range(len(cell_id)))))
    pert_idose_dict = dict(zip(pert_idose, list(range(len(pert_idose)))))
    matlab_data = np.zeros((len(pert_id), len(cell_id), len(pert_idose), 978), dtype=np.float64)
    matlab_index = np.zeros((len(pert_id), len(cell_id), len(pert_idose), 978), dtype=np.float64)
    matlab_index_test = np.zeros((len(pert_id), len(cell_id), len(pert_idose)), dtype=np.float64)
    for ft, lb in sorted(data_train.items()):
        ft = ft.split(',')
        pert_id = pert_id_dict[ft[0]]
        cell_id = cell_id_dict[ft[2]]
        pert_idose = pert_idose_dict[ft[3]]
        if len(lb) == 1:
            matlab_data[pert_id, cell_id, pert_idose] = lb[0]
        else:
            matlab_data[pert_id, cell_id, pert_idose] = choose_mean_example(lb)
        matlab_index[pert_id, cell_id, pert_idose] = np.ones(978)
    for ft, lb in sorted(data_test.items()):
        ft = ft.split(',')
        pert_id = pert_id_dict[ft[0]]
        cell_id = cell_id_dict[ft[2]]
        pert_idose = pert_idose_dict[ft[3]]
        if len(lb) == 1:
            matlab_data[pert_id, cell_id, pert_idose] = lb[0]
        else:
            matlab_data[pert_id, cell_id, pert_idose] = choose_mean_example(lb)
        matlab_index_test[pert_id, cell_id, pert_idose] = 1
    output = dict()
    output['data'] = matlab_data
    output['index'] = matlab_index
    # sio.savemat('tensor_completion/data_high_quality.mat', output)
    return matlab_data, matlab_index, matlab_index_test


def read_data_2_matlab_tradition(train_file, filter):
    data = dict()
    pert_id = []
    cell_id = []
    pert_idose = []
    with open(train_file, 'r') as f:
        f.readline()
        for line in f:
            line = line.strip().split(',')
            assert len(line) == 983, "Wrong format"
            if filter["time"] in line[0] and line[1] not in filter["pert_id"] \
                    and line[2] in filter["pert_type"] and line[3] in filter["cell_id"] and line[4] in \
                    filter["pert_idose"]:
                pert_id.append(line[1])
                cell_id.append(line[3])
                pert_idose.append(line[4])
                ft = ','.join(line[1:5])
                lb = [float(i) for i in line[5:]]
                try:
                    data[ft].append(lb)
                except KeyError:
                    data[ft] = [lb]

    pert_id = sorted(list(set(pert_id)))
    cell_id = sorted(list(set(cell_id)))
    pert_idose = sorted(list(set(pert_idose)))
    pert_id_dict = dict(zip(pert_id, list(range(len(pert_id)))))
    cell_id_dict = dict(zip(cell_id, list(range(len(cell_id)))))
    pert_idose_dict = dict(zip(pert_idose, list(range(len(pert_idose)))))
    matlab_data = np.zeros((len(pert_id), len(cell_id), len(pert_idose), 978), dtype=np.float64)
    matlab_index = np.zeros((len(pert_id), len(cell_id), len(pert_idose), 978), dtype=np.float64)
    matlab_index_test = np.zeros((len(pert_id), len(cell_id), len(pert_idose)), dtype=np.float64)
    keys = sorted(data.keys())
    train_key, test_key = train_test_split(keys, test_size=0.2, random_state=75)
    is_test = False
    for ft, lb in sorted(data.items()):
        if ft in test_key:
            is_test = True
        ft = ft.split(',')
        pert_id = pert_id_dict[ft[0]]
        cell_id = cell_id_dict[ft[2]]
        pert_idose = pert_idose_dict[ft[3]]
        if len(lb) == 1:
            matlab_data[pert_id, cell_id, pert_idose] = lb[0]
        else:
            matlab_data[pert_id, cell_id, pert_idose] = choose_mean_example(lb)
        if is_test:
            matlab_index_test[pert_id, cell_id, pert_idose] = 1
        else:
            matlab_index[pert_id, cell_id, pert_idose] = np.ones(978)
        is_test = False
    output = dict()
    output['data'] = matlab_data
    output['index'] = matlab_index
    output['index_test'] = matlab_index_test
    sio.savemat('tensor_completion/data_high_quality_tradition.mat', output)


def load_data_from_matlab(original_file, predicted_file):
    predict = sio.loadmat(predicted_file)['X_hat_sgd']
    data = sio.loadmat(original_file)
    origin = data['data']
    index = data['index']
    index_test = data['index_test']
    return predict, origin, index, index_test


def load_data_from_matlab_old(predicted_file):
    predict = sio.loadmat(predicted_file)['X_hat_sgd']
    return predict


def precision_k(label_test, label_predict, k):
    num_pos = 100
    num_neg = 100
    label_test = np.argsort(label_test, axis=0)
    label_predict = np.argsort(label_predict, axis=0)
    neg_test_set = label_test[:num_neg]
    pos_test_set = label_test[-num_pos:]
    neg_predict_set = label_predict[:k]
    pos_predict_set = label_predict[-k:]
    neg_test = set(neg_test_set)
    pos_test = set(pos_test_set)
    neg_predict = set(neg_predict_set)
    pos_predict = set(pos_predict_set)
    neg = len(neg_test.intersection(neg_predict)) / k
    pos = len(pos_test.intersection(pos_predict)) / k
    return neg, pos


def rmse_calculation(label_test, label_predict):
    return np.sqrt(mean_squared_error(label_test, label_predict))


def correlation(label_test, label_predict, correlation_type):
    if correlation_type == 'pearson':
        corr = pearsonr
    elif correlation_type == 'spearman':
        corr = spearmanr
    else:
        raise ValueError("Unknown correlation type: %s" % correlation_type)
    score = []
    score.append(corr(label_test, label_predict)[0])
    return np.mean(score)


def evaluate(original, predict, index, index_test):
    original_out = []
    predict_out = []
    neg_100_total = []
    pos_100_total = []
    pearson_total = []
    spearman_total = []
    rmse_total = []
    data_shape = np.shape(original)
    for i in range(data_shape[0]):
        for j in range(data_shape[1]):
            for k in range(data_shape[2]):
                # if np.mean(index[i, j, k]) == 1:
                if index_test[i, j, k] == 1:
                    # print('original')
                    # print(original[i, j, k])
                    # print('predict')
                    # print(predict[i, j, k])
                    original_out.append(original[i, j, k])
                    predict_out.append(predict[i, j, k])
                    neg_100, pos_100 = precision_k(original[i, j, k], predict[i, j, k], 100)
                    rmse = rmse_calculation(original[i, j, k], predict[i, j, k])
                    pearson = correlation(original[i, j, k], predict[i, j, k], 'pearson')
                    spearman = correlation(original[i, j, k], predict[i, j, k], 'spearman')
                    neg_100_total.append(neg_100)
                    pos_100_total.append(pos_100)
                    pearson_total.append(pearson)
                    spearman_total.append(spearman)
                    rmse_total.append(rmse)
    print('Pearson\'s correlation: %.4f' % np.mean(pearson_total))
    print('Spearman\'s correlation: %.4f' % np.mean(spearman_total))
    print('RMSE: %.4f' % np.mean(rmse_total))
    print('Precision@100 Positive: %.4f' % np.mean(pos_100_total))
    print('Precision@100 Negative: %.4f' % np.mean(neg_100_total))
    np.savetxt("../predicted/ttwopt_model_ground_truth.csv", np.array(original_out), delimiter=",", fmt='%.6f')
    np.savetxt("../predicted/ttwopt_model_predict.csv", np.array(predict_out), delimiter=",", fmt='%.6f')


def get_eval_pert_id(input_file):
    with open(input_file) as f:
        test_pert_id = f.readline().strip().split(',')[1:]
        dev_pert_id = f.readline().strip().split(',')[1:]
        return dev_pert_id, test_pert_id


if __name__ == '__main__':
    filter = {"time": "24H", "pert_id": ['BRD-U41416256', 'BRD-U60236422'], "pert_type": ["trt_cp"],
              "cell_id": ["A375", "HT29", "MCF7", "PC3", "HA1E", "YAPC", "HELA"],
              "pert_idose": ["0.04 um", "0.04 um", "0.12 um", "0.37 um", "1.11 um", "3.33 um", "10.0 um"]}
    dev_pert_id, test_pert_id = get_eval_pert_id('../data/pert_id_eval.txt')
    origin, index, index_test = read_data_2_matlab('../data/signature_0_7.csv', '../data/signature_test.csv',
                                                     dev_pert_id, test_pert_id, filter)

    # read_data_2_matlab_tradition('../data/signature_0_7.csv', filter)
    #
    # subprocess.check_call(shlex.split('matlab -nodesktop -nosplash -r "tensor_completion_main; exit;"'))

    predict = load_data_from_matlab_old('tensor_completion/out_high_quality_5.mat')

    # predict, origin, index, index_test = load_data_from_matlab('tensor_completion/data_high_quality_tradition.mat',
    #                                                            'tensor_completion/out_high_quality_tradition_5.mat')
    evaluate(origin, predict, index, index_test)
