import numpy as np
from datetime import datetime
from sklearn.linear_model import SGDRegressor


def read_drug(input_file):
    with open(input_file, 'r') as f:
        drug = dict()
        for line in f:
            line = line.strip().split(',')
            assert len(line) == 883, "Wrong format"
            bin_vec = [float(i) for i in line[2:]]
            drug[line[0]] = bin_vec
    return drug


def read_gene(input_file):
    with open(input_file, 'r') as f:
        gene = []
        for line in f:
            line = line.strip().split(',')
            assert len(line) == 129, "Wrong format"
            gene.append([float(i) for i in line[1:]])
    return np.asarray(gene, dtype=np.float64)


def choose_mean_example(examples):
    num_example = len(examples)
    mean_value = (num_example - 1) / 2
    indexes = np.argsort(examples, axis=0)
    indexes = np.argsort(indexes, axis=0)
    indexes = np.mean(indexes, axis=1)
    distance = (indexes - mean_value)**2
    index = np.argmin(distance)
    return examples[index]


def convert_2_vector(ft, pert_id_dict, cell_id_dict, pert_idose_dict):
    pert_id_ft = pert_id_dict[ft[0]]
    cell_id_ft = cell_id_dict[ft[2]]
    pert_idose_ft = pert_idose_dict[ft[3]]
    # exp_ft = np.concatenate((pert_id_ft, cell_id_ft, pert_idose_ft), axis=0).reshape(1, -1).repeat(978, axis=0)
    # ft = np.concatenate((exp_ft, gene), axis=1)
    return pert_id_ft, cell_id_ft, pert_idose_ft


def read_data(input_file, filter, drug, gene):
    data = dict()
    pert_id = []
    cell_id = []
    pert_idose = []
    with open(input_file, 'r') as f:
        f.readline()
        for line in f:
            line = line.strip().split(',')
            assert len(line) == 983, "Wrong format"
            if filter["time"] in line[0] and line[1] not in filter["pert_id"] and line[2] in filter["pert_type"] \
                    and line[3] in filter["cell_id"] and line[4] in filter["pert_idose"]:
                pert_id.append(line[1])
                cell_id.append(line[3])
                pert_idose.append(line[4])
                ft = ','.join(line[1:5])
                lb = [float(i) for i in line[5:]]
                try:
                    data[ft].append(lb)
                except KeyError:
                    data[ft] = [lb]
    pert_id = sorted(list(set(pert_id)))
    cell_id = sorted(list(set(cell_id)))
    pert_idose = sorted(list(set(pert_idose)))

    drug_ft = []
    for id in pert_id:
        drug_ft.append(drug[id])
    drug_ft = np.asarray(drug_ft, dtype=np.float64)
    index = []
    for i in range(np.shape(drug_ft)[1]):
        if set(drug_ft[:, i]) == {0.0, 1.0}:
            index.append(i)
    drug_ft = drug_ft[:, index]
    pert_id_dict = dict(zip(pert_id, drug_ft))
    cell_id_dict = dict(zip(cell_id, np.eye(len(cell_id))))
    pert_idose_dict = dict(zip(pert_idose, np.eye(len(pert_idose))))
    num_pert_id = len(pert_id)
    fold_size = int(num_pert_id/10)
    train_pert = pert_id[:fold_size * 9]
    test_pert = pert_id[fold_size * 9:]
    pert_id_ft_train = []
    cell_id_ft_train = []
    pert_idose_ft_train  = []
    pert_id_ft_test = []
    cell_id_ft_test = []
    pert_idose_ft_test  = []
    label_train = []
    label_test = []
    for i, (ft, lb) in enumerate(sorted(data.items())):
        # if i%1000 == 0:
        #     print(i)
        ft_split = ft.split(',')
        # ft = convert_2_vector(ft_split, pert_id_dict, cell_id_dict, pert_idose_dict, gene)
        pert_id_ft, cell_id_ft, pert_idose_ft = convert_2_vector(ft_split, pert_id_dict, cell_id_dict,
                                                                 pert_idose_dict)
        if len(lb) == 1:
            if ft_split[0] in train_pert:
                # feature_train = np.concatenate((feature_train, ft), axis=0)
                pert_id_ft_train.append(pert_id_ft)
                cell_id_ft_train.append(cell_id_ft)
                pert_idose_ft_train.append(pert_idose_ft)
                label_train.append(lb[0])
            elif ft_split[0] in test_pert:
                # feature_test = np.concatenate((feature_test, ft), axis=0)
                pert_id_ft_test.append(pert_id_ft)
                cell_id_ft_test.append(cell_id_ft)
                pert_idose_ft_test.append(pert_idose_ft)
                label_test.append(lb[0])
        else:
            if ft_split[0] in train_pert:
                # feature_train = np.concatenate((feature_train, ft), axis=0)
                pert_id_ft_train.append(pert_id_ft)
                cell_id_ft_train.append(cell_id_ft)
                pert_idose_ft_train.append(pert_idose_ft)
                label_train.append(choose_mean_example(lb))
            elif ft_split[0] in test_pert:
                # feature_test = np.concatenate((feature_test, ft), axis=0)
                pert_id_ft_test.append(pert_id_ft)
                cell_id_ft_test.append(cell_id_ft)
                pert_idose_ft_test.append(pert_idose_ft)
                label_test.append(choose_mean_example(lb))
    pert_id_ft_train = np.array(pert_id_ft_train)
    pert_id_ft_test = np.array(pert_id_ft_test)
    cell_id_ft_train = np.array(cell_id_ft_train)
    cell_id_ft_test = np.array(cell_id_ft_test)
    pert_idose_ft_train = np.array(pert_idose_ft_train)
    pert_idose_ft_test = np.array(pert_idose_ft_test)
    feature_train = np.concatenate((pert_id_ft_train, cell_id_ft_train, pert_idose_ft_train), axis=1)
    feature_test = np.concatenate((pert_id_ft_test, cell_id_ft_test, pert_idose_ft_test), axis=1)
    # label_train = np.array(label_train).reshape(1, -1)
    # label_test = np.array(label_test).reshape(1, -1)
    # feature_train = np.concatenate((feature_train, np.repeat(gene, num_train, axis=0)), axis=1)
    # feature_test = np.concatenate((feature_test, np.repeat(gene, num_test, axis=0)), axis=1)
    return feature_train, feature_test, np.array(label_train), np.array(label_test)


def precision_k(label_test, label_predict, k):
    num_pos = 100
    num_neg = 100
    label_test = np.argsort(label_test, axis=1)
    label_predict = np.argsort(label_predict, axis=1)
    precision_k_neg = []
    precision_k_pos = []
    neg_test_set = label_test[:, :num_neg]
    pos_test_set = label_test[:, -num_pos:]
    neg_predict_set = label_predict[:, :k]
    pos_predict_set = label_predict[:, -k:]
    for i in range(len(neg_test_set)):
        neg_test = set(neg_test_set[i])
        pos_test = set(pos_test_set[i])
        neg_predict = set(neg_predict_set[i])
        pos_predict = set(pos_predict_set[i])
        precision_k_neg.append(len(neg_test.intersection(neg_predict)) / k)
        precision_k_pos.append(len(pos_test.intersection(pos_predict)) / k)
    return np.mean(precision_k_neg), np.mean(precision_k_pos)


if __name__ == '__main__':
    start_time = datetime.now()
    filter = {"time": "24H", "pert_id": ['BRD-U41416256', 'BRD-U60236422'], "pert_type": ["trt_cp"],
              "cell_id": ["A375", "HT29", "MCF7", "PC3", "HA1E", "YAPC", "HELA"],
              "pert_idose": ["0.04 um", "0.04 um", "0.12 um", "0.37 um", "1.11 um", "3.33 um", "10.0 um"]}
    patk = [10, 20, 50, 100]
    model = SGDRegressor(penalty='l1')
    num_epoch = 10
    batch_size = 256
    drug = read_drug('../data/drugs.csv')
    gene = read_gene('../data/gene_vector.csv')
    feature_train, feature_test, label_train, label_test = read_data('../data/signature.csv', filter, drug, gene)
    for epoch in range(num_epoch):
        print('Epoch %d' % (epoch+1))
        index = np.random.permutation((len(feature_train)))
        predict = np.empty(0)
        print('Train')
        for i, start_idx in enumerate(range(0, len(feature_train), batch_size)):
            # print(i)
            excerpt = index[start_idx:start_idx + batch_size]
            ft = feature_train[excerpt]
            lb = label_train[excerpt]
            g = gene.repeat(len(ft), axis=0)
            ft = ft.repeat(978, axis=0)
            ft = np.concatenate((ft, g), axis=1)
            lb = lb.reshape(-1)
            model.partial_fit(ft, lb)
        print('Predict')
        for i, start_idx in enumerate(range(0, len(feature_test), batch_size)):
            excerpt = slice(start_idx, start_idx + batch_size)
            ft = feature_test[excerpt]
            g = gene.repeat(len(ft), axis=0)
            ft = ft.repeat(978, axis=0)
            ft = np.concatenate((ft, g), axis=1)
            predict = np.concatenate((predict, model.predict(ft)), axis=0)
        predict = predict.reshape(-1, 978)
        for k in patk:
            precision_neg, precision_pos = precision_k(label_test, predict, k)
            print("Precision@%d Positive: %.4f" % (k, precision_pos))
            print("Precision@%d Negative: %.4f" % (k, precision_neg))
    end_time = datetime.now()
    print(end_time - start_time)