function mat = GET_SAMPLES_BY_FEATURES(path, file)

maxval = -1;
%% first get the rowsnum and the max number of features
fid = fopen(strcat(path,file));

linectr = 0;
tline = fgetl(fid);
while ischar(tline)
    
    cols = str2double(regexp(tline,'\d*','match')');
    maxval = max([maxval; cols]);
    linectr = linectr + 1;
    
    tline = fgetl(fid);
end
fclose(fid);

%% then read the matrix for real
mat = zeros(linectr, maxval);
clearvars -EXCEPT mat path file

fid = fopen(strcat(path,file));

linectr = 0;
tline = fgetl(fid);
while ischar(tline)
    
    cols = str2double(regexp(tline,'\d*','match')');
    linectr = linectr + 1;
    mat(linectr, cols) = 1;
    tline = fgetl(fid);
end
fclose(fid);