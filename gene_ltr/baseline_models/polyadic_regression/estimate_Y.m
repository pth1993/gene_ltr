function Y = estimate_Y(X,b,w,F,C, sparseY, idx, IND_MODE)

%% init
n = zeros(3,1);
d = zeros(3,1);
for i = 1:3
    [d(i),n(i)] = size(X{i});
end
C{1,2,3} = tensor(C{1,2,3});

% %% vectorized comp. of Y
% applyToGivenCol = @(func, matrix) @(col) func(matrix(:, col));
% applyToCols = @(func, matrix) arrayfun(applyToGivenCol(func, matrix), 1:size(matrix,2))';
%
% combos = combvec(X{1},X{2},X{3}); % each column will be an appended combination of (i,j,k) cols of X{1},X{2},X{3} respectively
%
% Y = zeros(n(1),n(2),n(3));
% Y = applyToCols(@(x) obj_fun(x_vec_to_cell(x, d),b,w,S), combos);
%
% Y = reshape(Y, n(1),n(2),n(3));

%% compute for all possible combinations of data points
if (sparseY == true)
    
    assert(size(idx, 2)==3);
    
    Y = zeros(size(idx, 1), 1);
    
    for i=1: size(idx, 1)
        xtmp = {};
        xtmp{1} = X{1}(:,idx(i,1));
        xtmp{2} = X{2}(:,idx(i,2));
        xtmp{3} = X{3}(:,idx(i,3));
        
        xF{1} = xtmp{1}'*F{1};
        xF{2} = xtmp{2}'*F{2};
        xF{3} = xtmp{3}'*F{3};
        
        Y(i) = obj_fun(xtmp,b,w,C,xF,IND_MODE);
    end
else
    
    Y = zeros(n(1),n(2),n(3));
    for i = 1:n(1)
        x1 = X{1}(:,i);
        for j = 1:n(2)
            x2 = X{2}(:,j);
            for k = 1:n(3)
                x3 = X{3}(:,k);
                
                xtmp = {};
                xtmp{1} = x1;
                xtmp{2} = x2;
                xtmp{3} = x3;
                
                xF{1} = xtmp{1}'*F{1};
                xF{2} = xtmp{2}'*F{2};
                xF{3} = xtmp{3}'*F{3};
                
                Y(i,j,k) = obj_fun(xtmp,b,w,C,xF,IND_MODE);
                
            end
        end
    end
end

end


% function [x] = x_vec_to_cell(x, d)
%
% longvec = x;
% x = {};
%
% x{1} = longvec(1:d(1)); longvec(1:d(1)) = [];
% x{2} = longvec(1:d(2)); longvec(1:d(2)) = [];
% x{3} = longvec(1:d(3));