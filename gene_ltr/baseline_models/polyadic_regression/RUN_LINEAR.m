clearvars -except DESIGNMAT TESTMAT VALIDATIONMAT X testset trainset validationset; close all;
for ctr=1:5
EHR_DATA = 0;
ROOTPATH = '/Users/joachim/Dropbox/GeorgiaTech/';
FOLDS = -1; %holdout method
COLD_START = 1;
SUBSETS_DATA = 0;
SUBSETS_FEATURES = 0;
VALIDATE = 0;

if (EHR_DATA)
    path = strcat(ROOTPATH, 'health_data/mimic3_preprocessed/out/');
    IND_MODE = [];
    load(strcat(path,'tensor_data_design_EHR.mat'));
else
    path = strcat(ROOTPATH, 'health_data/LINCS_drug_ind_gene_exp/');
    IND_MODE = 3; assert(length(IND_MODE) == 1);
    if (COLD_START)
        %load(strcat(path,'tensor_data_design_coldstart_GENEXPR.mat'));
    else
        load(strcat(path,'tensor_data_design_GENEXPR.mat'));
    end
end

meanY = mean(trainset(:, end));

%LM2 = ridge(trainset(:, end) - meanY, DESIGNMAT, k);
%LM = fitglm(DESIGNMAT{i}, trainset{i}(:, end) - meanY, 'linear');

glmnetopts.nlambda = 100;
glmnetopts.alpha = 0;
glmnetfit = glmnet(DESIGNMAT, trainset(:, end) - meanY, [], glmnetopts);

if (VALIDATE)
    glmnetpred = glmnetPredict(glmnetfit, VALIDATIONMAT, []);
else
    glmnetpred = glmnetPredict(glmnetfit, TESTMAT, []);
end

for kk = 1: size(glmnetpred, 2)
    
    LM = glmnetpred(:, kk);
    
    %% test
    LM_PRED = meanY + LM;
    
    if (VALIDATE)
        LM_ERR = eval_fun(LM_PRED, validationset(:,end));
        LM_SPEAR = corr(LM_PRED, validationset(:,end), 'type', 'Spearman');
    else
        LM_ERR = eval_fun(LM_PRED, testset(:,end));
        LM_SPEAR = corr(LM_PRED, testset(:,end), 'type', 'Spearman');
    end
    
    ridge_1_err(kk) = LM_ERR;
    ridge_1_spear(kk) = LM_SPEAR;
    
end
% figure;
% plot(glmnetfit.lambda, ridge_1_err, '-o')
% figure;
% plot(glmnetfit.lambda, ridge_1_spear, '-o')

cross_validated_idx = 87;
glmnetfit.lambda(cross_validated_idx)
lin_spearman(ctr) = ridge_1_spear(cross_validated_idx);
lin_mse(ctr) = ridge_1_err(cross_validated_idx);
end
save('linear_coldstart_results.mat','lin_spearman','lin_mse');