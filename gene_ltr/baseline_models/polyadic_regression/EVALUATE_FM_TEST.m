clear all; close all
ROOTPATH = '/Users/joachim/Dropbox/GeorgiaTech/';
%FM_OUT_DIR = 'output_test/';
FM_OUT_DIR = 'output_test/5';
FOLDS = 5;

EHR_DATA = 0;
COLD_START = 1;

if (EHR_DATA)
    path = strcat(ROOTPATH, 'health_data/mimic3_preprocessed/out/');
    path_for_FM = strcat(ROOTPATH, 'papers/polyadic_regression/exps_3way/competitors/FM/EHR');
    load(strcat(path,'tensor_data_EHR.mat'));
else
    path = strcat(ROOTPATH, 'health_data/LINCS_drug_ind_gene_exp/');
    if (COLD_START)
        path_for_FM = strcat(ROOTPATH, 'papers/polyadic_regression/exps_3way/competitors/FM/GENEXPR_COLDSTART/');
        load(strcat(path,'tensor_data_coldstart_GENEXPR.mat'));
    else
        path_for_FM = strcat(ROOTPATH, 'papers/polyadic_regression/exps_3way/competitors/FM/GENEXPR/');
        load(strcat(path,'tensor_data_GENEXPR.mat'));
    end
    
end

if (COLD_START)
    ranks = 2;
else
    ranks = [2 4 6 8 10 11];
end

for j=1:length(ranks)
    
    FM_PRED = load(strcat(path_for_FM, strcat(FM_OUT_DIR, sprintf('/output_%d.txt',ranks(j)))));
    % de-center responses based on the mean of the y_i's of the training
    load(strcat(path_for_FM, sprintf('meanY_%d.mat', 1)));
    FM_PRED = meanY + FM_PRED;
    
    FM_ERR(j) = eval_fun(FM_PRED, testset(:,end))
    FM_SPEAR(j) = corr(FM_PRED, testset(:,end), 'type', 'Spearman')
    
end

% fm_rank = ranks;
% fm_spear = FM_SPEAR;
% fm_mse = FM_ERR;
% path = '/Users/joachim/Dropbox/GeorgiaTech/papers/polyadic_regression/exps_3way/results_accum_sett1/';
% save(strcat(path,'fm.mat'), 'fm_rank', 'fm_spear', 'fm_mse');