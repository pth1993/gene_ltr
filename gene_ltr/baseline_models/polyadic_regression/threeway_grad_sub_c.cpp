#include "armaMex.hpp"
#include "math.h"
#include <assert.h>
#ifdef OMPINCL
#include <omp.h>
#endif

/*
mat inner_grad_with_C(mat dx, mat x1, mat x2, mat x3, double b, mat w1, mat w2,mat w3, double Y, mat F1,mat F2,mat F3,mat C12,mat C13,mat C23, cube C123){

	mat xF1 = x1.t()*F1;
	mat xF2 = x2.t()*F2;
	mat xF3 = x3.t()*F3;

	mat obj_val = obj_fun(x1,x2,x3,b,w1,w2,w3,C12,C13,C23,C123,xF1,xF2,xF3);

	double err = 2 * ( obj_val(0,0) - Y);
	//double err = exp(obj_val(0,0)) - Y;

	mat db ;
	db << 1 <<endr;        
	mat tempdw = join_vert( x1, x2 );
	mat dw = join_vert( tempdw, x3 );

	mat grdten[3];
	#pragma omp parallel for num_threads(3)
	for (int i=0; i<3 ; i++){
	   grdten[i] =  grad_of_tensor(x1,x2,x3,C123, i+1 ,xF1,xF2,xF3); // Caution, the elements will be at positions 0,1,2 respectively
	}

	mat dF1 = x1 * xF2 * C12.t() + x1 * xF3 * C13.t() + grdten[0];
	mat dF2 = x2 * xF1 * C12 + x2 * xF3 * C23.t() + grdten[1];
	mat dF3 = x3 * xF2 * C23     + x3 * xF1 * C13     + grdten[2];
	
	mat dC12 = xF1.t()*xF2;
	mat dC13 = xF1.t()*xF3;
	mat dC23 = xF2.t()*xF3;

	cube dC123 = zeros(F1.n_cols,F2.n_cols,F3.n_cols); // CAUTION WITH PARTIAL INDUCTION WHEN F{i} may have not been set !!!!
	for(int i3 = 0;i3<F3.n_cols;i3++)
	{
		dC123.slice(i3) = xF1.t() * xF2 * xF3(i3);
	}

	mat v1 = join_vert( db, dw );
	mat v2 = join_vert( v1, vectorise(dF1) );
	mat v3 = join_vert( v2, vectorise(dF2) );
	mat v4 = join_vert( v3, vectorise(dF3) );
	mat v5 = join_vert( v4, vectorise(dC12) );
	mat v6 = join_vert( v5, vectorise(dC13) );
	mat v7 = join_vert( v6, vectorise(dC23) );
	mat v8 = join_vert( v7, vectorise(dC123) );

	dx = dx + err * v8;
	return dx;
}*/

mat inner_grad_2(double CPP_IND_MODE, double flag_opt_C_GD, mat x1, mat x2, mat x3, double b, mat w1, mat w2,mat w3,
	double Y, mat F1,mat F2,mat F3,mat C12,mat C13,mat C23, cube C123){

	// needed for all later operations (TODO: for loop)
	mat xF1, xF2, xF3;
    if (CPP_IND_MODE != 1)
    	xF1 = x1.t()*F1;
    else
    	xF1 = x1.t();

    if (CPP_IND_MODE != 2)
		xF2 = x2.t()*F2;
    else
    	xF2 = x2.t();

    if (CPP_IND_MODE != 3)
		xF3 = x3.t()*F3;
    else
    	xF3 = x3.t();

	// related to dF1, dF2, dF3 calculation
	mat sum_cols_1, sum_cols_2;
	vec v_3;
	if (CPP_IND_MODE != 1)         
		sum_cols_1 = zeros(xF1.n_cols, C123.n_slices);
	if (CPP_IND_MODE != 2)        
		sum_cols_2 = zeros(xF2.n_cols, C123.n_slices);
	if (CPP_IND_MODE != 3)   
		v_3 = zeros(C123.n_slices,1);

    // related to dC123 calculation
    cube dC123;
	if (flag_opt_C_GD==1)
		dC123 = zeros(xF1.n_cols,xF2.n_cols,xF3.n_cols);
	// related to obj func calculation
	vec f2(C123.n_slices);

	//#pragma omp parallel for num_threads(C123.n_slices)
	for(int i = 0;i<C123.n_slices;i++){  

		// related to dF1, dF2, dF3 calculation  
		if (CPP_IND_MODE != 1)         
			sum_cols_1.col(i) = xF3(i) * C123.slice(i) * xF2.t();
		if (CPP_IND_MODE != 2)
			sum_cols_2.col(i) = xF3(i) * C123.slice(i).t() * xF1.t();
		if (CPP_IND_MODE != 3){
			mat tmp_3 = xF1 * C123.slice(i) * xF2.t(); 
			v_3(i) = tmp_3(0,0);
		}

        // related to dC123 calculation
        if (flag_opt_C_GD==1)
			dC123.slice(i) = xF1.t() * xF2 * xF3(i);

  		//related to obj func calculation
		mat tmp = xF1 * C123.slice(i) * xF2.t() * xF3(i);
		f2(i) = tmp(0, 0);
	}

	mat dF1, dF2, dF3;
	if (CPP_IND_MODE != 1)
		dF1 = x1 * sum(sum_cols_1, 1).t();
	if (CPP_IND_MODE != 2)
		dF2 = x2 * sum(sum_cols_2, 1).t();
	if (CPP_IND_MODE != 3)
		dF3 = x3 * v_3.t();

	// related to obj func calculation
	mat obj_val;
	obj_val << b << endr;
	//obj_val = obj_val + w1.t()*x1 + w2.t()*x2 + w3.t()*x3;
	if (CPP_IND_MODE == -1)
    	obj_val = obj_val + w1.t()*x1 + w2.t()*x2 + w3.t()*x3;
    else if (CPP_IND_MODE == 1)
    	obj_val = obj_val + w2.t()*x2 + w3.t()*x3;
    else if (CPP_IND_MODE == 2)
    	obj_val = obj_val + w1.t()*x1 + w3.t()*x3;
    else //CPP_IND_MODE = 3
    	obj_val = obj_val + w1.t()*x1 + w2.t()*x2;
	obj_val = obj_val + xF1*C12*xF2.t() + xF1*C13*xF3.t() + xF2*C23*xF3.t();
	obj_val(0, 0) = obj_val(0, 0) + accu(f2);
	double err = 2 * ( obj_val(0,0) - Y);

	// construction of parameters to return
	mat db ;
	db << 1 <<endr;        

	mat dw;
	if (CPP_IND_MODE == -1){
		mat tempdw = join_vert( x1, x2 );
		dw = join_vert( tempdw, x3 );
	}
    else if (CPP_IND_MODE == 1)
    	dw = join_vert( x2, x3 );
    else if (CPP_IND_MODE == 2)
    	dw = join_vert( x1, x3 );
    else //CPP_IND_MODE = 3
    	dw = join_vert( x1, x2 );	

    if (CPP_IND_MODE != 1)
		dF1 = x1 * xF2 * C12.t() + x1 * xF3 * C13.t() + dF1; //add to existing dF
	if (CPP_IND_MODE != 2)
		dF2 = x2 * xF1 * C12 + x2 * xF3 * C23.t() + dF2;
	if (CPP_IND_MODE != 3)
		dF3 = x3 * xF2 * C23 + x3 * xF1 * C13 + dF3;

	mat dC12, dC13, dC23;
	if (flag_opt_C_GD==1){
		dC12 = xF1.t()*xF2;
		dC13 = xF1.t()*xF3;
		dC23 = xF2.t()*xF3;
	}

	mat v1 = join_vert( db, dw ), v4;
	if (CPP_IND_MODE == -1){
		mat v2 = join_vert( v1, vectorise(dF1) );
		mat v3 = join_vert( v2, vectorise(dF2) );
		v4 = join_vert( v3, vectorise(dF3) );
	}
    else if (CPP_IND_MODE == 1){
		mat v2 = join_vert( v1, vectorise(dF2) );
		v4 = join_vert( v2, vectorise(dF3) );
    }	
    else if (CPP_IND_MODE == 2){
    	mat v2 = join_vert( v1, vectorise(dF1) );
		v4 = join_vert( v2, vectorise(dF3) );
    }
	else{
		mat v2 = join_vert( v1, vectorise(dF1) );
		v4 = join_vert( v2, vectorise(dF2) );
	}

	mat v8;
	if (flag_opt_C_GD==1){
		mat v5 = join_vert( v4, vectorise(dC12) );
		mat v6 = join_vert( v5, vectorise(dC13) );
		mat v7 = join_vert( v6, vectorise(dC23) );
		v8 = join_vert( v7, vectorise(dC123) );
	}
	else
		v8 = v4;

	return err * v8;
}

mat threeway_grad_sub(double CPP_IND_MODE,double flag_opt_C_GD,mat X1, mat X2, mat X3, double b,mat w1, mat w2, mat w3,mat F1,mat F2,mat F3,mat C12,mat C13,mat C23, cube C123,int dim_x,cube Y){

	// mat dx = zeros(dim_x,1);
	long sizeY = X1.n_cols*X2.n_cols*X3.n_cols;
	mat parts_of_sum = zeros(dim_x, sizeY);

	#pragma omp parallel for
	for(int i = 0;i<sizeY;i++){

		uvec multi_idx = ind2sub(size(Y), i);
	    mat x1 = X1.col(multi_idx(0)); //CAUTION: C++ starts indexing from Zero, in contrast to Matlab
	    mat x2 = X2.col(multi_idx(1));
	    mat x3 = X3.col(multi_idx(2));

	    parts_of_sum.col(i) = inner_grad_2(CPP_IND_MODE, flag_opt_C_GD, x1, x2, x3, b,w1, w2, w3, Y(multi_idx(0), multi_idx(1), multi_idx(2)),F1, F2,F3,C12, C13, C23, C123);
	}
	return parts_of_sum * ones(sizeY, 1);

}

mat threeway_grad_sub(double CPP_IND_MODE,double flag_opt_C_GD,mat X1, mat X2, mat X3, double b,mat w1, mat w2, mat w3,mat F1,mat F2,mat F3,mat C12,mat C13,mat C23, cube C123,int dim_x, mat idx, vec Y)
{
	//mat dx = zeros(dim_x,1);
	mat parts_of_sum = zeros(dim_x, idx.n_rows);

	#pragma omp parallel for
	for(int i = 0;i<idx.n_rows;i++){

	    mat x1 = X1.col(idx(i, 0)-1); //CAUTION: C++ starts indexing from Zero, in contrast to Matlab
	    mat x2 = X2.col(idx(i, 1)-1);
	    mat x3 = X3.col(idx(i, 2)-1);

	    parts_of_sum.col(i) = inner_grad_2(CPP_IND_MODE, flag_opt_C_GD, x1, x2, x3, b,w1, w2, w3, Y(i),F1, F2,F3,C12, C13, C23, C123);
	}

	return parts_of_sum * ones(idx.n_rows, 1); 
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	int argctr = 0;
	double CPP_IND_MODE = armaGetDouble(prhs[argctr++]);
	assert(CPP_IND_MODE == -1 || (CPP_IND_MODE > 0 && CPP_IND_MODE < 4));
	
	double flag_opt_C_GD = armaGetDouble(prhs[argctr++]);
	assert(flag_opt_C_GD==0 || flag_opt_C_GD==1);

	mat X1 = armaGetPr(prhs[argctr++]);   
	mat X2 = armaGetPr(prhs[argctr++]); 
	mat X3 = armaGetPr(prhs[argctr++]); 
	double b = armaGetDouble(prhs[argctr++]);

	mat w1,w2,w3;
	if (CPP_IND_MODE==1)
		argctr++;
	else
		w1 = armaGetPr(prhs[argctr++]);
	if (CPP_IND_MODE==2)
		argctr++;
	else
		w2 = armaGetPr(prhs[argctr++]);
	if (CPP_IND_MODE==3)
		argctr++;
	else
		w3 = armaGetPr(prhs[argctr++]);

	mat F1, F2, F3;
	if (CPP_IND_MODE==1)
		argctr++;
	else
		F1 = armaGetPr(prhs[argctr++]); 
	if (CPP_IND_MODE==2)
		argctr++;
	else
		F2 = armaGetPr(prhs[argctr++]); 
	if (CPP_IND_MODE==3)
		argctr++;
	else
		F3 = armaGetPr(prhs[argctr++]); 

	mat C12 = armaGetPr(prhs[argctr++]); 
	mat C13 = armaGetPr(prhs[argctr++]); 
	mat C23 = armaGetPr(prhs[argctr++]); 

    cube C123(C12.n_rows, C12.n_cols, C13.n_cols);
    int ro = C123.n_rows, co = C123.n_cols, sl = C123.n_slices;
    if (ro==1 && co==1 && sl==1)
        C123 = armaGetDouble(prhs[argctr++]); 
    else if (sl==1){
        mat tempten = armaGetPr(prhs[argctr++]);
        C123.slice(0) = tempten;
    }
    else
        C123 = armaGetCubePr(prhs[argctr++]); 

	int dim_x = armaGetDouble(prhs[argctr++]); 
	double sparseY = armaGetDouble(prhs[argctr++]); 

	mat dx; 
	if (sparseY == 0){ 
		cube Y = armaGetCubePr(prhs[argctr++]); 
		dx = threeway_grad_sub(CPP_IND_MODE,flag_opt_C_GD,X1,X2,X3,b,w1,w2,w3,F1,F2,F3,C12,C13,C23,C123,dim_x,Y); 
	} 
	else{ 

		vec Y = armaGetPr(prhs[argctr++]); 
		mat idx = armaGetPr(prhs[argctr++]); 
		dx = threeway_grad_sub(CPP_IND_MODE,flag_opt_C_GD,X1,X2,X3,b,w1,w2,w3,F1,F2,F3,C12,C13,C23,C123,dim_x,idx, Y); 
	} 
	mat mat_out = dx; 
	plhs[0] = armaCreateMxMatrix(mat_out.n_rows, mat_out.n_cols); 

	armaSetPr(plhs[0], mat_out); 

	return; 
}