function [DESIGNMAT] = BUILDMAT(myset, X)
for i=1:3
    d(i) = size(X{i}, 1);
end

DESIGNMAT = zeros(size(myset, 1), sum(d));
for j=1: size(DESIGNMAT, 1)
    tmp = [];
    for q=1:3
        tmp = [tmp X{q}(:,myset(j,q))'];
    end
    DESIGNMAT(j, :) = tmp;
end