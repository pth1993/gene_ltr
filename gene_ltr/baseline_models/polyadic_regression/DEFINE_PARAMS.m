function [FEATURE_RANK, opts, prox_param] = DEFINE_PARAMS(myopts, aa, myfileid, IND_MODE, EHR_DATA, COLD_START)

opts = [];
prox_param = [];

idxctr = 1;
%% feature rank
FEATURE_RANK(1) = myopts.FEATURE_RANK_1(aa(idxctr));
idxctr = idxctr + 1;
FEATURE_RANK(2) = myopts.FEATURE_RANK_2(aa(idxctr));
idxctr = idxctr + 1;
FEATURE_RANK(3) = myopts.FEATURE_RANK_3(aa(idxctr));
idxctr = idxctr + 1;
fprintf(myfileid, 'feature ranks: %d %d %d\n', FEATURE_RANK);
m = [FEATURE_RANK(1) FEATURE_RANK(2) FEATURE_RANK(3)];

%% init vector of parameters
if (EHR_DATA)
    opts.init_zero = 0;
    opts.initial_scale = []; opts.initrandn = [];
    
    opts.initial_scale = 1.0e-1;
    opts.initrandn = 0;
    
%     switch myopts.MY_INIT(aa(idxctr))
%         case 1
%             opts.initial_scale = 1.0e-1;  % initial_scale for the paramter for optimization, i.e., (b,w1,w3,F1,F3,C12,C13,C23,C123) will be initialized using [0,1] Uniform random numbers multiplied by the initial_scale
%             opts.initrandn = 0;
%         case 2
%             opts.initial_scale = 1.0e-1;  % initial_scale for the paramter for optimization, i.e., (b,w1,w3,F1,F3,C12,C13,C23,C123) will be initialized using [0,1] Uniform random numbers multiplied by the initial_scale
%             opts.initrandn = 1;
%         case 3
%             opts.initial_scale = 1.0e-2;  % initial_scale for the paramter for optimization, i.e., (b,w1,w3,F1,F3,C12,C13,C23,C123) will be initialized using [0,1] Uniform random numbers multiplied by the initial_scale
%             opts.initrandn = 0;
%         case 4
%             opts.initial_scale = 1.0e-2;  % initial_scale for the paramter for optimization, i.e., (b,w1,w3,F1,F3,C12,C13,C23,C123) will be initialized using [0,1] Uniform random numbers multiplied by the initial_scale
%             opts.initrandn = 1;
%         otherwise
%             assert(1>2);
%     end
    fprintf(myfileid, 'initial_scale: %f, initrandn: %d\n', opts.initial_scale, opts.initrandn);
    % idxctr = idxctr + 1;
else
    opts.init_zero = 0;
    opts.initial_scale = []; opts.initrandn = [];
    
    opts.initial_scale = 1.0e-2;
    opts.initrandn = 0;
    
    % switch myopts.MY_INIT(aa(idxctr))
    %     case 1
    %         opts.initial_scale = 1.0e-1;  % initial_scale for the paramter for optimization, i.e., (b,w1,w3,F1,F3,C12,C13,C23,C123) will be initialized using [0,1] Uniform random numbers multiplied by the initial_scale
    %         opts.initrandn = 0;
    %     case 2
    %         opts.initial_scale = 1.0e-1;  % initial_scale for the paramter for optimization, i.e., (b,w1,w3,F1,F3,C12,C13,C23,C123) will be initialized using [0,1] Uniform random numbers multiplied by the initial_scale
    %         opts.initrandn = 1;
    %     case 3
    %         opts.initial_scale = 1.0e-2;  % initial_scale for the paramter for optimization, i.e., (b,w1,w3,F1,F3,C12,C13,C23,C123) will be initialized using [0,1] Uniform random numbers multiplied by the initial_scale
    %         opts.initrandn = 0;
    %     case 4
    %         opts.initial_scale = 1.0e-2;  % initial_scale for the paramter for optimization, i.e., (b,w1,w3,F1,F3,C12,C13,C23,C123) will be initialized using [0,1] Uniform random numbers multiplied by the initial_scale
    %         opts.initrandn = 1;
    %     otherwise
    %         assert(1>2);
    % end
    fprintf(myfileid, 'initial_scale: %f, initrandn: %d\n', opts.initial_scale, opts.initrandn);
    % idxctr = idxctr + 1;
end

%% init with HOSVD or not
opts.init_hosvd = 0;
% if (aa(idxctr) == 1)
%     opts.init_hosvd = 1;
% else
%     opts.init_hosvd = 0;
% end
fprintf(myfileid, 'init with hosvd: %d\n', opts.init_hosvd);
% idxctr = idxctr + 1;

%% whether we will keep non-negative F matrices
if (EHR_DATA)
    opts.POSITIVE_F = 1;
%     if (myopts.POSITIVE_F(aa(idxctr))==1)
%         opts.POSITIVE_F = 1;
%     else
%         opts.POSITIVE_F = 0;
%     end
    fprintf(myfileid, 'positive_F: %d\n', opts.POSITIVE_F);
    % idxctr = idxctr + 1;
else
    opts.POSITIVE_F = 1;
    % if (myopts.POSITIVE_F(aa(idxctr))==1)
    %     opts.POSITIVE_F = 1;
    % else
    %     opts.POSITIVE_F = 0;
    % end
    fprintf(myfileid, 'positive_F: %d\n', opts.POSITIVE_F);
    % idxctr = idxctr + 1;
end


%% prox_param setting
% REGUL = 'L2';
% if (aa(idxctr)==1)
%     prox_param.C.method = REGUL;
%     fprintf(myfileid, 'C regularized\n');
% else
%     prox_param.C.method = [];
%     fprintf(myfileid, 'C not regularized\n');
% end
% idxctr = idxctr + 1;


prox_param.C{1,2}.method = [];
% coefficient of regularization
% for i=1: 3
%     if (i==IND_MODE)
%         continue;
%     end
%     prox_param.w{i}.method = 'EN';  % regularization method, options are : 'L2' for L2 norm; 'L1' for L1 norm;  'EN' for Elastic Net;  'GS' for Group Sparsity, which is only valid for matrix;
%     prox_param.F{i}.method = 'EN';  % regularization method for Fi
%
%     %for L1
%     switch myopts.REG_VALUE_L1(aa(idxctr))
%         case 1
%             prox_param.w{i}.param(1) = 0;
%             prox_param.F{i}.param(1) = 0;
%         case 2
%             prox_param.w{i}.param(1) = 1/(d(i)*m(i));
%             prox_param.F{i}.param(1) = 1/(d(i)*m(i));
% %         case 2
% %             prox_param.w{i}.param(1) = 1;
% %             prox_param.F{i}.param(1) = 1;
%         otherwise
%             assert(1>2);
%     end
% end
% idxctr = idxctr + 1;

if (EHR_DATA)
    %possvals1 = logspace(-14, 5, 20);
    possvals1 = logspace(-6, 1, 8);
else    
    possvals1 = [0 logspace(-5, 3, 9)];
    %possvals1 = [0 logspace(-1, 1, 3)];
end

for i=1: 3
    if (i==IND_MODE)
        continue;
    end
    
    prox_param.w{i}.method = 'L2';  % regularization method, options are : 'L2' for L2 norm; 'L1' for L1 norm;  'EN' for Elastic Net;  'GS' for Group Sparsity, which is only valid for matrix;
    prox_param.w{i}.param(1) = possvals1(myopts.REG_VALUE_L2_w(aa(idxctr)));
    fprintf(myfileid, 'L2 regularization: w{%d} regularizer is %e \n',i, prox_param.w{i}.param(1));
    
end
idxctr = idxctr + 1;

if (EHR_DATA)
    %possvals2 = logspace(-14, 5, 20);
    possvals2 = logspace(-6, 1, 8);
else    
    possvals2 = [0 logspace(-5, 3, 9)];
    %possvals2 = [0 logspace(-1, 1, 3)];
end

for i=1: 3
    if (i==IND_MODE)
        continue;
    end
    
    prox_param.F{i}.method = 'L2';  % regularization method for Fi
    prox_param.F{i}.param(1) = possvals2(myopts.REG_VALUE_L2_F(aa(idxctr)));
    fprintf(myfileid, 'L2 regularization: F{%d} regularizer is %e \n', i, prox_param.F{i}.param(1));
    
end
idxctr = idxctr + 1;
