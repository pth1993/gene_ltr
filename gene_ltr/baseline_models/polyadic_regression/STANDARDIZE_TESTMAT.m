function [testmat] = STANDARDIZE_TESTMAT(testmat, mu, sigma)

for j=1: size(testmat, 2)
    testmat(:, j) = (testmat(:, j) - mu(j))./sigma(j);
end