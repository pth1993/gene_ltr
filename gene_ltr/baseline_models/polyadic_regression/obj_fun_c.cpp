#include "armaMex.hpp"
#include "math.h"
#include <assert.h>
//#ifdef OMPINCL
//#include <omp.h>
//#endif


mat obj_fun(double CPP_IND_MODE, mat x1, mat x2, mat x3, double b,mat w1,mat w2,mat w3,mat C12,mat C13,mat C23, cube C123,mat xF1,mat xF2,mat xF3)
{
    mat val;
    val << b << endr;
    if (CPP_IND_MODE == -1)
    	val = val + w1.t()*x1 + w2.t()*x2 + w3.t()*x3;
    else if (CPP_IND_MODE == 1)
    	val = val + w2.t()*x2 + w3.t()*x3;
    else if (CPP_IND_MODE == 2)
    	val = val + w1.t()*x1 + w3.t()*x3;
    else //CPP_IND_MODE = 3
    	val = val + w1.t()*x1 + w2.t()*x2;

    val = val + xF1*C12*xF2.t() +
                    xF1*C13*xF3.t() +
                        xF2*C23*xF3.t();    
    
    double f2 = 0;
    //#pragma omp parallel for reduction(+:f2)
    for(int i = 0;i<C123.n_slices;i++){

    	mat tmp = xF1 * C123.slice(i) * xF2.t() * xF3(i);
        f2 = f2 + tmp(0, 0);
    }
 	
 	val(0, 0) = val(0, 0) + f2;
    return val; 
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]){

     int argctr = 0;
     double CPP_IND_MODE = armaGetDouble(prhs[argctr++]);
     assert(CPP_IND_MODE == -1 || (CPP_IND_MODE > 0 && CPP_IND_MODE < 4));

     mat x1 = armaGetPr(prhs[argctr++]);
     mat x2 = armaGetPr(prhs[argctr++]);
     mat x3 = armaGetPr(prhs[argctr++]);
     double b = armaGetDouble(prhs[argctr++]);
     
     mat w1,w2,w3;
     if (CPP_IND_MODE==1)
     	argctr++;
     else
     	w1 = armaGetPr(prhs[argctr++]);
     if (CPP_IND_MODE==2)
     	argctr++;
     else
     	w2 = armaGetPr(prhs[argctr++]);
 	 if (CPP_IND_MODE==3)
     	argctr++;
     else
    	w3 = armaGetPr(prhs[argctr++]);

     mat C12 = armaGetPr(prhs[argctr++]);
     mat C13 = armaGetPr(prhs[argctr++]);
     mat C23 = armaGetPr(prhs[argctr++]);

    cube C123(C12.n_rows, C12.n_cols, C13.n_cols);
    int ro = C123.n_rows, co = C123.n_cols, sl = C123.n_slices;
    if (ro==1 && co==1 && sl==1)
        C123 = armaGetDouble(prhs[argctr++]); 
    else if (sl==1){
        mat tempten = armaGetPr(prhs[argctr++]);
        C123.slice(0) = tempten;
    }
    else
        C123 = armaGetCubePr(prhs[argctr++]); 

     mat xF1 = armaGetPr(prhs[argctr++]);
     mat xF2 = armaGetPr(prhs[argctr++]);
     mat xF3 = armaGetPr(prhs[argctr++]);

     mat val = obj_fun(CPP_IND_MODE,x1,x2,x3,b,w1,w2,w3,C12,C13,C23,C123,xF1,xF2,xF3);
     mat mat_out = val;             
     plhs[0] = armaCreateMxMatrix(mat_out.n_rows, mat_out.n_cols);

     armaSetPr(plhs[0], mat_out);
  
  return;
}
