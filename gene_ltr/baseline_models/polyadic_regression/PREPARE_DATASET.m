function PREPARE_DATASET(ROOTPATH, FOLDS, EHR_DATA, COLD_START, SUBSETS_DATA, SUBSETS_FEATURES)
assert(~(SUBSETS_DATA && SUBSETS_FEATURES));

trainRatio = 0.6;
valRatio = 0.2;
testRatio = 0.2;
assert(trainRatio+valRatio+testRatio==1);

PREPARE_FOR_FM = 1;

%% rng fix
rng('default');

%% prepare
if (EHR_DATA)
    path = strcat(ROOTPATH, 'health_data/mimic3_preprocessed/out/');
    path_for_FM = strcat(ROOTPATH, 'papers/polyadic_regression/exps_3way/competitors/FM/EHR');
    data = load(strcat(path,'tensor.txt'));
    % X{1} = GET_SAMPLES_BY_FEATURES(path, 'X_DX.txt')';
    % X{2} = GET_SAMPLES_BY_FEATURES(path, 'X_MEDS.txt')';
    % X{3} = GET_SAMPLES_BY_FEATURES(path, 'X_PR.txt')';
    load(strcat(path,'X_DX_MEDS_PR.mat'));
else
    path = strcat(ROOTPATH, 'health_data/LINCS_drug_ind_gene_exp/');
    if (COLD_START)
        path_for_FM = strcat(ROOTPATH, 'papers/polyadic_regression/exps_3way/competitors/FM/GENEXPR_COLDSTART/');
    else
        path_for_FM = strcat(ROOTPATH, 'papers/polyadic_regression/exps_3way/competitors/FM/GENEXPR/');
    end
    %load(strcat(path,'LINCS_small_g50.mat'));
    %load(strcat(path,'LINCS_small.mat'));
    if (COLD_START)
        load(strcat(path,'LINCS_coldstart.mat'));
    else
        load(strcat(path,'LINCS_top10tiss.mat'));
    end
end

%% truncate non-informative features
% X = TRUNCATE_FEATURES(X, EHR_DATA);

%% set size of vars
for i=1:3
    d(i) = size(X{i}, 1);
    n(i) = size(X{i}, 2);
end

%% train-validation-test sets
if (COLD_START) % cold start setting on DRUGS !
    assert(length(unique(data(:,1)))==n(1));
    [trainInd,valInd,testInd] = dividerand(1:n(1),trainRatio,valRatio,testRatio);
    trainset = data(ismember(data(:,1), trainInd), :);
    validationset = data(ismember(data(:,1), valInd), :);
    testset = data(ismember(data(:,1), testInd), :);
else
    % CV = cvpartition(size(data, 1),'KFold',FOLDS);
    [trainInd,valInd,testInd] = dividerand(size(data, 1),trainRatio,valRatio,testRatio);
    trainset = data(trainInd, :);
    validationset = data(valInd, :);
    testset = data(testInd, :);
end

if (SUBSETS_DATA)
    portions = [2 4 8];
    for i=1:length(portions)
        trainset_subsets{portions(i)} = datasample(trainset, round(size(trainset, 1)/portions(i)));
    end
    save(strcat(path,'subsets_tensor_data_GENEXPR.mat'), 'X', 'trainset_subsets', 'validationset', 'testset', '-v7.3');
    
    % for linear regression
    for i=1:length(portions)
        DESIGNMAT_SUBSETS{portions(i)} = BUILDMAT(trainset_subsets{portions(i)}, X);
    end
    VALIDATIONMAT = BUILDMAT(validationset, X);
    TESTMAT = BUILDMAT(testset, X);
    save(strcat(path,'subsets_tensor_data_design_GENEXPR.mat'), 'X', 'trainset_subsets', 'validationset', 'testset', 'DESIGNMAT_SUBSETS', 'VALIDATIONMAT', 'TESTMAT', '-v7.3');
    
    if (PREPARE_FOR_FM)
        for i=1: length(portions) %write only training set's subsets on disk
            WRITE_FOR_COMPET(DESIGNMAT_SUBSETS{portions(i)}, trainset_subsets{portions(i)}, path_for_FM, 1, 1, portions(i), []); % last argument is irrelevant if k-fold CV is not used
        end
    end
    return;
end
if (SUBSETS_FEATURES)
    portions = [2 4 8];
    for i=1:length(portions)
        newd = sum(d)/portions(i); % this is the new approx. number of features
        X_subsets{portions(i)} = X;
        X_subsets{portions(i)}{1} = datasample(X{1}, round(newd/2 - d(3)));
        X_subsets{portions(i)}{2} = compute_mapping(X{2}', 'PCA', round(newd/2))';
    end
    save(strcat(path,'feature_subsets_tensor_data_GENEXPR.mat'), 'X_subsets', 'trainset', 'validationset', 'testset', '-v7.3');
    
    % for linear regression
    for i=1:length(portions)
        DESIGNMAT_SUBSETS{portions(i)} = BUILDMAT(trainset, X_subsets{portions(i)});
    end
    VALIDATIONMAT = BUILDMAT(validationset, X);
    TESTMAT = BUILDMAT(testset, X);
    save(strcat(path,'feature_subsets_tensor_data_design_GENEXPR.mat'), 'X_subsets', 'trainset', 'validationset', 'testset', 'DESIGNMAT_SUBSETS', 'VALIDATIONMAT', 'TESTMAT', '-v7.3');
    
    if (PREPARE_FOR_FM)
        for i=1: length(portions) %write only training set's subsets on disk
            WRITE_FOR_COMPET(DESIGNMAT_SUBSETS{portions(i)}, trainset, path_for_FM, 1, 1, [], portions(i)); % last argument is irrelevant if k-fold CV is not used
        end
    end
    return;
end

% for linear regression
DESIGNMAT = BUILDMAT(trainset, X);
VALIDATIONMAT = BUILDMAT(validationset, X);
TESTMAT = BUILDMAT(testset, X);

% for i=1: FOLDS
%
%     %% specify train & test sets
%     trainset{i} = data(training(CV, i),:);
%     testset{i} = data(~training(CV, i),:);
%
%     %% competing approaches- build design and test matrix explicitly
%     DESIGNMAT{i} = BUILDMAT(trainset{i}, X);
%     TESTMAT{i} = BUILDMAT(testset{i}, X);
%
%     if (PREPARE_FOR_FM)
%         % prepare data for FM, first train
%         WRITE_FOR_COMPET(DESIGNMAT{i}, trainset{i}, path_for_FM, 1, i);
%         % then test
%         WRITE_FOR_COMPET(TESTMAT{i}, testset{i}, path_for_FM, 0, i);
%     end
%
% end


%% write
if (EHR_DATA)
    save(strcat(path,'tensor_data_EHR.mat'), 'X', 'trainset', 'validationset', 'testset', '-v7.3');
    save(strcat(path,'tensor_data_design_EHR.mat'), 'X', 'trainset', 'validationset', 'testset', 'DESIGNMAT', 'VALIDATIONMAT', 'TESTMAT', '-v7.3');
else
    if (COLD_START)
        save(strcat(path,'tensor_data_coldstart_GENEXPR.mat'), 'X', 'trainset', 'validationset', 'testset', '-v7.3');
        save(strcat(path,'tensor_data_design_coldstart_GENEXPR.mat'), 'X', 'trainset', 'validationset', 'testset', 'DESIGNMAT', 'VALIDATIONMAT', 'TESTMAT', '-v7.3');
    else
        save(strcat(path,'tensor_data_GENEXPR.mat'), 'X', 'trainset', 'validationset', 'testset', '-v7.3');
        save(strcat(path,'tensor_data_design_GENEXPR.mat'), 'X', 'trainset', 'validationset', 'testset', 'DESIGNMAT', 'VALIDATIONMAT', 'TESTMAT', '-v7.3');
    end
end

if (PREPARE_FOR_FM)
    WRITE_FOR_COMPET(DESIGNMAT, trainset, path_for_FM, 1, 1, []); % last argument is irrelevant if k-fold CV is not used
    WRITE_FOR_COMPET(TESTMAT, testset, path_for_FM, 0, 1, []);
    WRITE_FOR_COMPET(VALIDATIONMAT, validationset, path_for_FM, 2, 1, []);
end