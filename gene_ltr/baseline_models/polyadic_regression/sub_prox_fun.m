function X = sub_prox_fun(X,t,method,param)

if strcmp(method,'L1')
  X = sign(X) .* max(abs(X) - t*param(1),0);
elseif strcmp(method,'L2')
  X = X./(1+t*param(1));
elseif strcmp(method,'EN')    
  X = sign(X) .* max(abs(X) - t*param(1),0)./(1+t*param(2));  
elseif strcmp(method,'GS')       
  if  size(X,2)<2
      error('To choose Group Sparsity as the type of proximal operator, the second size of X should be bigger than 1')
  end
  norm_L2_2 = max(eps,sum(X.*X,2).^0.5);
  coef = max(0,1 - param(1)./norm_L2_2);
  X = bsxfun(@times,X,coef);
end

end