function opts = opt_C_close_form(x, opts)

n = zeros(3,1);
d = zeros(3,1);
for i = 1:3
    [d(i),n(i)] = size(opts.X{i});
end
m = opts.m;
Y = opts.Y;

if opts.sparseY==false
    numel_loss = numel(Y);  % number of all targets
else
    numel_loss = size(Y, 1); % non-zero number
end

numel_C = 0;
for i = 1:2
    for j = i+1:3
        numel_C = numel_C + m(i)*m(j);
    end
end
numel_C = numel_C + prod(m);

% re-organize the parameters into the forms of (b,w,F,C) from the vector x.
b = x(1); x(1) = [];
w = {};
for i=1: 3
    if (i==opts.IND_MODE)
        continue;
    end
    w{i} = x(1:d(i)); x(1:d(i)) = [];
end

F = {};
for i=1: 3
    if (i==opts.IND_MODE)
        F{i} = eye(d(i));
        continue;
    end
    F{i} = reshape(x(1:d(i)*m(i)),[d(i) m(i)]); x(1:d(i)*m(i)) = [];
end

X = opts.X;
if opts.use_c_sub
    if (~isempty(opts.IND_MODE))
        w{opts.IND_MODE} = [];
    end
    % C++ version
    if (opts.sparseY)
        [data,Y_C] = opt_C_close_form_sub_c(opts.CPP_IND_MODE,X{1},X{2},X{3},b,w{1},w{2},w{3},F{1},F{2},F{3}, numel_loss, numel_C, opts.sparseY, Y(:,end), Y(:,1:end-1));
    else
        [data,Y_C] = opt_C_close_form_sub_c(opts.CPP_IND_MODE,X{1},X{2},X{3},b,w{1},w{2},w{3},F{1},F{2},F{3}, numel_loss, numel_C, opts.sparseY, Y);
    end
else
    [data,Y_C] = opt_C_close_form_sub(X,b,w,F,Y, numel_loss, numel_C, opts.sparseY, opts.IND_MODE);
end

x = data \ Y_C;
% x = pinv(data)*Y_C;

C = {};
for i = 1:2
    for j = i+1:3
        C{i,j} = reshape(x(1:m(i)*m(j)),[m(i) m(j)]); x(1:m(i)*m(j)) = [];
    end
end
C{1,2,3} = reshape(x(1:m(1)*m(2)*m(3)),[m(1) m(2) m(3)]); x(1:m(1)*m(2)*m(3)) = [];

opts.C = C;

% eval check every 100 iters
if (opts.evalFlag)
    opts.evalFlag = 0;
    
    if (opts.CENTER_Y)
        Y_out = opts.meanY + estimate_Y(X, b, w, F, C, opts.sparseY, opts.validationset(:,1:end-1), opts.IND_MODE);
    else
        Y_out = estimate_Y(X, b, w, F, C, opts.sparseY, opts.validationset(:,1:end-1), opts.IND_MODE);
    end
    pr_mae_validation = mean(abs(Y_out - opts.validationset(:,end)));
    pr_err_validation = eval_fun(Y_out, opts.validationset(:,end));
    pr_spear_validation = corr(Y_out, opts.validationset(:,end), 'type', 'Spearman');
    fprintf(opts.myfileid, 'validation set (to choose hyperparams): MAE: %f, MSE: %f, SPEARMAN: %f\n', pr_mae_validation, pr_err_validation, pr_spear_validation);
    
end

end