% load POLYADIC_RESULTS;
clear all; close all;

%load('/Users/joachim/Dropbox/GeorgiaTech/papers/polyadic_regression/exps_3way/results_server_log/genexpr_coldstart/2/POLYADIC_RESULTS_GENEXPR.mat');
%load('/Users/joachim/Dropbox/GeorgiaTech/papers/polyadic_regression/exps_3way/results_server_log/genexpr/2/POLYADIC_RESULTS_GENEXPR.mat');

%load('/Users/joachim/Dropbox/GeorgiaTech/papers/polyadic_regression/exps_3way/results_server_log/genexpr_coldstart/2_NN_30iters/POLYADIC_RESULTS_GENEXPR.mat');
load('/Users/joachim/Dropbox/GeorgiaTech/papers/polyadic_regression/exps_3way/results_server_log/genexpr_coldstart/2_NN_30iters/POLYADIC_RESULTS_GENEXPR.mat');

grid_vars = 5;

possvals1 = [0 logspace(-5, 3, 9)];
possvals2 = [0 logspace(-5, 3, 9)];

spearmat = zeros(length(possvals1), length(possvals2));
errmat = zeros(length(possvals1), length(possvals2));
s_core_magn = zeros(length(possvals1), length(possvals2));
s12_magn = zeros(length(possvals1), length(possvals2));
s13_magn = zeros(length(possvals1), length(possvals2));
s23_magn = zeros(length(possvals1), length(possvals2));
w_magn = zeros(length(possvals1), length(possvals2));

absum_Score_mat = cell2mat(absum_Score);

for i=1: length(polyadic_pr_spear)
    
    %if (data(i)<threshold || isnan(data(i)))
    %if (polyadic_pr_spear(i)>threshold)
        aa = cell(1, grid_vars); [aa{:}] = ind2sub(outer_iters, i); aa = cell2mat(aa);
        fprintf('position %d: %1.2f -> ', i, polyadic_pr_spear(i));
        for j=1: length(aa)
            fprintf('%d ', aa(j));
        end
        fprintf('\n');
        
        spearmat(aa(end-1), aa(end)) = polyadic_pr_spear(i);
        errmat(aa(end-1), aa(end)) = polyadic_err_ten(i);

        w_magn(aa(end-1), aa(end)) = sum([absum_w{i}{:}]);

        s_core_magn(aa(end-1), aa(end)) = absum_Score_mat(i);
        
        s12_magn(aa(end-1), aa(end)) = absum_S{i}{1,2};
        s13_magn(aa(end-1), aa(end)) = absum_S{i}{1,3};
        s23_magn(aa(end-1), aa(end)) = absum_S{i}{2,3};


    %end
end


figure;imagesc(w_magn);colorbar;title('w magnitude');

figure;imagesc(s_core_magn);colorbar;title('core S magnitude');

figure;imagesc(s12_magn);colorbar;title('S{1,2} magnitude');
figure;imagesc(s13_magn);colorbar;title('S{1,3} magnitude');
figure;imagesc(s23_magn);colorbar;title('S{2,3} magnitude');

figure;imagesc(errmat);colorbar;title('mse');
figure;imagesc(spearmat);colorbar;title('spearman rho');


