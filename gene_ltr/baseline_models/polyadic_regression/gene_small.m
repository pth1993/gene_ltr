ROOTPATH = '/fs/project/PAS1536/hoang/codes';
addpath('/fs/project/PAS1536/hoang/codes/apg');
addpath('/fs/project/PAS1536/hoang/codes/tensor_toolbox');
addpath('/fs/project/PAS1536/hoang/codes/armadillo-7.200.2');
% ROOTPATH = '/Users/hoangpham/Downloads/SDM2017/codes';
% addpath('/Users/hoangpham/Downloads/SDM2017/codes/apg');
% addpath('/Users/hoangpham/Downloads/SDM2017/codes/tensor_toolbox');
% addpath('/Users/hoangpham/Downloads/SDM2017/codes/armadillo-7.200.2');
fprintf('Load data from file\n')
load('data_500.mat');

X = {};
X{1}= drug;
X{2}= cell;
X{3} = gene;

d = [603 7 128];
n = [500 7 978]; 
m = [5 5 5];

prox_param = [];
for i= 1: length(d)
    prox_param.w{i}.method = 'L1';
    prox_param.w{i}.param = 10;
end

for i= 1: length(d)
    prox_param.F{i}.method = 'L2';
    prox_param.F{i}.param = 0.5;
end

CREG = [];
prox_param.C{1,2}.method = CREG; prox_param.C{1,3}.method = CREG; prox_param.C{2,3}.method = CREG; prox_param.C{1,2,3}.method = CREG; 

opts = [];
opts.flag_opt_C_GD = true;   % if flag_opt_C_GD == true, in each iteration, use gradient descent to optimize C12,C13,C23,C123. Other wise, in each iteration, compute closed form solutions of C12,C13,C23,C123 using least square.
opts.use_c_sub = true;   % if use_c_sub == true, use C++ version of sub-function for accelaration. Otherwise, only use matlab version of sub-function, which might be about 50 times slower. The results of different version of sub-funcion are the same.

opts.m = m;

opts.SUBSETS = 0;

opts.init_hosvd = 0;
opts.initial_scale = 1;      % initial_scale for the paramter for optimization, i.e., (b,w1,w2,w3,F1,F3,C12,C13,C23,C123) will be initialized using [0,1] Uniform random numbers multiplied by the initial_scale
opts.initrandn = 0;
opts.init_zero = 0;
opts.evalFlag = 1; % to check how the error is dropping every 100 iters
opts.validationset = data_dev;

%optimization settings of the original APG matlab function
opts.MAX_ITERS = 2000;          % the maximum number of iteration of the APG optimization.
opts.EPS = 1e-6;                % used for the stopping critierion, which is based on the relative error of two successive parameter vectors.
opts.GEN_PLOTS = false;

% additional parameter configs
opts.IND_MODE = [];
assert(isempty(opts.IND_MODE));
opts.CPP_IND_MODE = -1;
    
opts.POSITIVE_F = 1;

opts.sparseY = true;
opts.myfileid = 1;
fprintf('Initialize parameters\n')
b = 0;  % zero 
w = {};
for i = 1: length(d)
w{i}= zeros(d(i),1);
end

F = {};
for i = 1: length(d)
F{i}= rand(d(i),m(i));
end

C={};
for i = 1:2
    for j = i+1:3
        C{i,j} = randn(m(i), m(j));  % randn PAIRWISE DOMAIN INTERACTIONS
    end
end
C{1,2,3} = randn(m(1),m(2),m(3)); % randn TENSOR CORE
% Y = estimate_Y(X,b,w,F,C, opts.sparseY, [], opts.IND_MODE);
fprintf('Train\n')
[b,w,F,C] = apg_threeway_opt(X,data_train,prox_param,opts);
fprintf('Predict\n')
Y_dev_out = estimate_Y(X,b,w,F,C, opts.sparseY, data_dev(:, 1:end-1), opts.IND_MODE);
Y_test_out = estimate_Y(X,b,w,F,C, opts.sparseY, data_test(:, 1:end-1), opts.IND_MODE);
fprintf('Save file\n')
save('out_500_1000.mat','Y_dev_out', 'Y_test_out');
