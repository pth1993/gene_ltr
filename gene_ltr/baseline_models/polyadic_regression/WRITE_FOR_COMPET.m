function WRITE_FOR_COMPET(MAT, myset, path_for_compet, is_training, i, subset_data, subset_feature)
if (~isempty(subset_data))
    assert(isempty(subset_feature));
elseif (~isempty(subset_feature))
    assert(isempty(subset_data));
end

assert(size(myset, 1)==size(MAT, 1));

if (is_training==1) %1 for training set, 0 for test set, 2 for validation set
    if (~isempty(subset_data))
        fileID = fopen(strcat(path_for_compet,sprintf('train%d_%d.txt',i,subset_data)),'w');
    elseif (~isempty(subset_feature))
        fileID = fopen(strcat(path_for_compet,sprintf('feat_train%d_%d.txt',i,subset_feature)),'w');
    else
        fileID = fopen(strcat(path_for_compet,sprintf('train%d.txt',i)),'w');
    end
    
    % center responses
    meanY = mean(myset(:, end));
    myset(:, end) = myset(:, end) - meanY;
    
    save(strcat(path_for_compet, sprintf('meanY_%d.mat', i)), 'meanY');
else
    if (is_training==0)
        fileID = fopen(strcat(path_for_compet,sprintf('test%d.txt',i)),'w');
    else
        assert(is_training==2);
        fileID = fopen(strcat(path_for_compet,sprintf('validation%d.txt',i)),'w');
    end
end

for r=1: size(MAT, 1)
    fprintf(fileID, '%f', myset(r, end));
    for p=1: length(MAT(r, :))
        if (MAT(r, p)~=0)
            if (MAT(r, p)==1)
                fprintf(fileID, ' %d:1', p-1);
            else
                fprintf(fileID, ' %d:%.3f', p-1, MAT(r, p));
            end
        end
    end
    fprintf(fileID, '\n');
end
fclose(fileID);