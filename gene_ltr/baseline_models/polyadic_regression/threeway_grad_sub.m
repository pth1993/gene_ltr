function dx = threeway_grad_sub(X,b,w,F,C,Y,dim_x, sparseY, idx, IND_MODE, flag_opt_C_GD)

%% init
n = zeros(3,1);
d = zeros(3,1);
m = zeros(3,1);
for i = 1:3
    [d(i),n(i)] = size(X{i});
    m(i) = size(F{i},2);
end

dx = zeros(dim_x,1);

%% compute grad
if (sparseY == true)
    
    assert(size(idx, 2)==3);
    
    for i=1: size(idx, 1)
        xtmp = {};
        xtmp{1} = X{1}(:,idx(i,1));
        xtmp{2} = X{2}(:,idx(i,2));
        xtmp{3} = X{3}(:,idx(i,3));
        
        dx = inner_grad(dx, xtmp, b, w, Y(i), F, C, IND_MODE, flag_opt_C_GD);
    end
    
else
    
    for i = 1:n(1)
        x1 = X{1}(:,i);
        for j = 1:n(2)
            x2 = X{2}(:,j);
            for k = 1:n(3)
                x3 = X{3}(:,k);
                
                xtmp = {};
                xtmp{1} = x1;
                xtmp{2} = x2;
                xtmp{3} = x3;
                
                dx = inner_grad(dx, xtmp, b, w, Y(i,j,k), F, C, IND_MODE, flag_opt_C_GD);
            end
        end
    end
    
end

end