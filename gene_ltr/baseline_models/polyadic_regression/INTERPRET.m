clear all;close all;
regw_idx = 6;
regf_idx = 1;
%path = '/Users/joachim/Dropbox/GeorgiaTech/papers/polyadic_regression/exps_3way/results_server_log/genexpr_coldstart/2_NN_4_1/5/';
path = '/Users/joachim/Dropbox/GeorgiaTech/papers/polyadic_regression/exps_3way/results_server_log/genexpr_coldstart/2_NN/';
%path = '/Users/joachim/Dropbox/GeorgiaTech/papers/polyadic_regression/exps_3way/results_server_log/genexpr_coldstart/5_5_NN/';

data_path = '/Users/joachim/Dropbox/GeorgiaTech/health_data/LINCS_drug_ind_gene_exp/LINCS_coldstart.mat';
load(data_path);
load('/Users/joachim/Dropbox/GeorgiaTech/papers/polyadic_regression/exps_drug_gene_tissue/drug_feature_names.mat'); %load feature names for drugs
drug_feature_mynames = drug_feature_names(drugFeatIndices); % adjust the names of drug features to align to the ones available right now
load(strcat(path, 'POLYADIC_RESULTS_GENEXPR_',num2str(regw_idx),'_',num2str(regf_idx),'.mat'));
mypath = '/Users/joachim/Dropbox/GeorgiaTech/papers/polyadic_regression/results/';


clusters = 2;
clust_idx = kmeans(F{1}, clusters);
new_F1 = []; new_drug_feature_mynames = {};
for i=1: clusters
    temp = F{1}(clust_idx == i, :);
    temp_names = drug_feature_mynames(clust_idx == i);
    new_F1 = [new_F1; temp];
    new_drug_feature_mynames = [new_drug_feature_mynames; temp_names];
end

PERCENTILE = 99;
% toplot_drug_idx = logical(zeros(size(new_drug_feature_mynames)));
% for j=1: size(F{1}, 2)
%    temp = F{1}(:, j);
%    toplot_drug_idx = toplot_drug_idx | (temp > prctile(F{1}(:), PERCENTILE));
% end
for j=1: size(F{1}, 2)
   temp = F{1}(:, j);
   new_drug_feature_mynames(temp > prctile(F{1}(:), PERCENTILE))
end
%% plot
% new_drug_feature_mynames2 = new_drug_feature_mynames;
% toempty = find(~toplot_drug_idx);
% for i=1: length(toempty)
%     new_drug_feature_mynames2{toempty(i)} = '';
% end

%figure; imagesc(F{1}); colorbar; title('Drug features basis'); ax = gca; ax.YTick = 1:size(F{1}, 1);  ax.YTickLabel=drug_feature_mynames'; %ax.FontSize = 6;
figure; imagesc(new_F1);  colorbar; title('Low-rank representation of drug features'); ax = gca;  ax.XTick = 1:size(F{1}, 2);%ax.YTick = 1:size(new_F1, 1); ax.YTickLabel=new_drug_feature_mynames2'; ax.FontSize = 10;
print('-dpdf', '-r300', strcat(mypath,'interpret_F1.pdf')); close;
figure; imagesc(F{2});  colorbar; title('Gene features basis');
figure; imagesc(C{1,2}); colorbar; title('Drug feature groups to gene feature groups avg effect');
figure; imagesc(C{1,3});  colorbar; title('Average effect of each group of drug features for each tissue'); ax = gca;  ax.YTick = 1:size(new_F1, 2); ax.XTickLabel=cellIds;
print('-dpdf', '-r300', strcat(mypath,'interpret_C13.pdf')); close;
figure; imagesc(C{2,3});  colorbar; title('Gene feature groups to each one of the tissues avg effect'); ax = gca; ax.XTickLabel=cellIds;

core_C = C{1,2,3};
% slice w.r.t. gene groups
% for i=1: size(core_C, 2)
%     figure; imagesc(squeeze(core_C(:,i,:)));  colorbar; title(strcat('Gene group ',num2str(i),' : drug feature groups to each one of the tissues avg effect')); ax = gca; ax.XTickLabel=cellIds;
% end
for i=1: size(core_C, 3)
    figure; imagesc(squeeze(core_C(:,:,i)));  colorbar; title(strcat('Tissue ',num2str(i),' : drug feature groups to gene feature groups'));
end

%% others
for j=1:2
    for k=j+1:3
        Shat{j,k} =F{j}*C{j,k}*F{k}';
    end
end
Shat_core = double ( ttm( tensor(C{1,2,3}), F ) );


% PERCENTILE = 90; percent = 3;
% [feat_drg_group, drg_group] = INTERPRET_top_feat_and_entities(F{1}, X{1}, PERCENTILE, percent, 0);
% [feat_gene_group, gene_group] = INTERPRET_top_feat_and_entities(F{2}, X{2}, PERCENTILE, percent, 1);
% TISSUES = [3];
% D_GROUP = 1; G_GROUP = 1;
% indices1 = ismember(data(:, 1), drg_group{D_GROUP});
% indices2 = ismember(data(:, 2), gene_group{G_GROUP});
% indices3 = ismember(data(:, 3), TISSUES);
% mean(data(indices1 & indices2 & indices3, end))
