function dx = threeway_grad(x, opts)

dim_x = size(x,1); %dimension of all parameters
d = zeros(3,1);     % dimension of Xi, i = 1,2,3;
n = zeros(3,1);     % sample size of Xi, i = 1,2,3;
for i = 1:3
    [d(i),n(i)] = size(opts.X{i});
end
m = opts.m;          % number of columns of Fi, i = 1,2,3;
Y = opts.Y;

if opts.sparseY==false
    numel_loss = numel(Y);  % number of all targets
else
    numel_loss = size(Y, 1); % non-zero number
end

% re-organize the parameters into the forms of (b,w,F,C) from the vector x.
b = x(1); x(1) = [];
w = {};
for i=1: 3
    if (i==opts.IND_MODE)
        continue;
    end
    w{i} = x(1:d(i)); x(1:d(i)) = [];
end

F = {};
for i=1: 3
    if (i==opts.IND_MODE)
        F{i} = eye(d(i));
        continue;
    end
    F{i} = reshape(x(1:d(i)*m(i)),[d(i) m(i)]); x(1:d(i)*m(i)) = [];
end

if (opts.flag_opt_C_GD)
    C = {};
    for i = 1:2
        for j = i+1:3
            C{i,j} = reshape(x(1:m(i)*m(j)),[m(i) m(j)]); x(1:m(i)*m(j)) = [];
        end
    end
    C{1,2,3} = reshape(x(1:m(1)*m(2)*m(3)),[m(1) m(2) m(3)]); x(1:m(1)*m(2)*m(3)) = [];
else
    C = opts.C;
end

X = opts.X;

% sub-function, which contains 3-layer loop, so C++ version will give a good acceleration
if opts.use_c_sub
    if (~isempty(opts.IND_MODE))
        w{opts.IND_MODE} = [];
    end
    % C++ version
    if (opts.sparseY)
        dx = threeway_grad_sub_c(opts.CPP_IND_MODE,opts.flag_opt_C_GD,X{1},X{2},X{3},b,w{1},w{2},w{3},F{1},F{2},F{3},C{1,2},C{1,3},C{2,3},C{1,2,3}, dim_x, opts.sparseY, Y(:,end), Y(:,1:end-1));
    else
        dx = threeway_grad_sub_c(opts.CPP_IND_MODE,opts.flag_opt_C_GD,X{1},X{2},X{3},b,w{1},w{2},w{3},F{1},F{2},F{3},C{1,2},C{1,3},C{2,3},C{1,2,3}, dim_x, opts.sparseY, Y);
    end
else
    C{1,2,3} = tensor(C{1,2,3}, m);
    % matlab version
    if (opts.sparseY)
        dx = threeway_grad_sub(X,b,w,F,C,Y(:,end),dim_x, 1, Y(:,1:end-1), opts.IND_MODE, opts.flag_opt_C_GD);
    else
        dx = threeway_grad_sub(X,b,w,F,C,Y,dim_x, 0, [], opts.IND_MODE, opts.flag_opt_C_GD);
    end
end
dx = dx / numel_loss;

end