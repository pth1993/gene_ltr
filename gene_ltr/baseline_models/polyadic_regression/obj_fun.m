function f = obj_fun(x,b,w,C,xF,IND_MODE)

f = b;
for k=1:3
    if (k==IND_MODE)
        continue;
    end
    f = f + w{k}'*x{k};
end

for i=1:2
    for j=i+1:3
        f = f + xF{i}*C{i,j}*xF{j}';
    end
end

% f = f + double ( ttm( C{1,2,3}, xF ) ); % assuming C{1,2,3} is a tensor

C123 = double(C{1,2,3});
f2 = zeros(size(C123, 3), 1);
for i=1: size(C123, 3)
    f2(i) = xF{1} * C123(:,:,i) * xF{2}' * xF{3}(i); 
end
f = f + sum(f2);

end