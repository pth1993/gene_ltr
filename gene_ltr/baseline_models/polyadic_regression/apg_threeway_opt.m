function [b,w,F,C] = apg_threeway_opt(X,Y,prox_param,opts)
% Input:
% X: should be a cell, length(X) should be 3
% Y: should be a cube, i.e., length(size(Y)) should be 3
% prox_param: coefficient of regularization. If it is empty, L2 norm
% regularization will be applied as the default setting.
% opts: the following fields of opts should be given:
%       1. opts.m:  the number of columns of Fi, i =1,2,3. For instance,
%       opts.m = [2 3 4];
%       2. opts.initial_scale: initial_scale for the paramter for
%       optimization, i.e., (b,w1,w2,w3,F1,F2,F3,C12,C13,C23,C123) will be
%       initialized using [0,1] Uniform random numbers multiplied by the
%       initial_scale. For instance, opts.initial_scale = 1e-5;
%       3. opts.use_c_sub: if use_c_sub == true, use C++ version of
%       sub-function for accelaration. Otherwise, only use matlab version
%       of sub-function, which might be about 50 times slower. The results
%       of different version of sub-funcion are the same.

% store data
opts.X = X;
opts.Y = Y;

% super parameters
d = zeros(3,1);     % dimension of Xi, i = 1,2,3;
n = zeros(3,1);     % sample size of Xi, i = 1,2,3;
for i = 1:3
    [d(i),n(i)] = size(opts.X{i});
end
m = opts.m;         % number of columns of Fi, i = 1,2,3;

% the default setting of regularization
if isempty(prox_param)
    for i=1: 3
        if (i==opts.IND_MODE)
            continue;
        end
        prox_param.w{i}.method = 'L2';  % regularization method, options are : 'L2' for L2 norm; 'L1' for L1 norm;  'EN' for Elastic Net;  'GS' for Group Sparsity, which is only valid for matrix;
        prox_param.w{i}.param = 1/d(i); % coefficient of regularization, e.g., in ( lambda ||x||_2^2 ), lambda is the coefficient of regularization.
        prox_param.F{i}.method = 'L2';  % regularization method for F1
        prox_param.F{i}.param = 1/(d(i)*m(i)); % coefficient of regularization for F1, which can be the same with the coefficient of regularization for non-partial-induction modes
    end
end
opts.prox_param = prox_param;

%dimension of all parameters
dim_x = 1;
for i=1: 3
    if (i==opts.IND_MODE)
        continue;
    end
    dim_x = dim_x + d(i) + d(i)*m(i);
end

% include C as parameters only if opts.flag_opt_C_GD == true
if (opts.flag_opt_C_GD)
    dim_C = 0;
    for i = 1:2
        for j = i+1:3
            dim_C = dim_C + m(i)*m(j);
        end
    end
    dim_C = dim_C + prod(m);
    dim_x = dim_x + dim_C;
end

%initialization of all parameters
if (~opts.init_zero) % default is all zeros
    if (opts.initrandn)
        opts.X_INIT = opts.initial_scale * randn(dim_x,1);
    else
        opts.X_INIT = opts.initial_scale * rand(dim_x,1);
    end
end

if (opts.init_hosvd) % overwrite F matrices with HOSVD matrices
    opts = INIT_WITH_HOSVD(opts, X, Y, d, m);
end


%APG
if (opts.flag_opt_C_GD)
    [x] = apg(@threeway_grad, @threeway_soft_thresh, dim_x, opts); % x is a vector, containing all the parameters
else
    %APG, the only modification is that in each iteration, compute the closed form solutions for (C12,C13,C23,C123) using least square
    [x,C] = apg_opt_C_close_form(@threeway_grad, @threeway_soft_thresh, dim_x, opts);
end

% re-organize the parameters into the forms of (b,w,F,C) from the vector x.
b = x(1); x(1) = [];

w = {};
for i=1: 3
    if (i==opts.IND_MODE)
        continue;
    end
    w{i} = x(1:d(i)); x(1:d(i)) = [];
end

F = {};
for i=1: 3
    if (i==opts.IND_MODE)
        F{i} = eye(d(i));
        continue;
    end
    F{i} = reshape(x(1:d(i)*m(i)),[d(i) m(i)]); x(1:d(i)*m(i)) = [];
end

% include C as parameters only if opts.flag_opt_C_GD == true
if (opts.flag_opt_C_GD)
    C = {};
    for i = 1:2
        for j = i+1:3
            C{i,j} = reshape(x(1:m(i)*m(j)),[m(i) m(j)]); x(1:m(i)*m(j)) = [];
        end
    end
    C{1,2,3} = reshape(x(1:m(1)*m(2)*m(3)),[m(1) m(2) m(3)]); x(1:m(1)*m(2)*m(3)) = [];
end

end