function [data,Y_C] = opt_C_close_form_sub(X,b,w,F,Y, numel_loss, numel_C, sparseY, IND_MODE)

data = zeros(numel_loss,numel_C);
Y_C = zeros(numel_loss,1);

n = zeros(3,1);
d = zeros(3,1);
m = zeros(3,1);
for i = 1:3
    [d(i),n(i)] = size(X{i});
    m(i) = size(F{i},2);
end

cont = 0;
if (sparseY == true)
    
    for i=1: size(Y, 1)                
        cont = cont + 1;

        xtmp = {}; xtmp{1} = X{1}(:,Y(i,1)); xtmp{2} = X{2}(:,Y(i,2));  xtmp{3} = X{3}(:,Y(i,3));
        [Y_C(cont), data(cont, :)] = inner_lsq(xtmp,b,w,F,Y(i, end),IND_MODE);
    end
    
else
    
    for i = 1:n(1)
        x1 = X{1}(:,i);
        for j = 1:n(2)
            x2 = X{2}(:,j);
            for k = 1:n(3)
                x3 = X{3}(:,k);
                
                cont = cont + 1;
                
                xtmp = {}; xtmp{1} = x1; xtmp{2} = x2; xtmp{3} = x3;
                [Y_C(cont), data(cont, :)] = inner_lsq(xtmp,b,w,F,Y(i,j,k),IND_MODE);
                
            end
        end
    end
    
end




end