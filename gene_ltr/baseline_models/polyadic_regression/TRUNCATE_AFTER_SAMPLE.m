function [X, data, C] = TRUNCATE_AFTER_SAMPLE(X, data, EHR_DATA)

if (EHR_DATA)
    dims = 3;
else
    dims = 2;
    assert(length(unique(data(:, 3)))==2);
end

for i=1:dims
    [C{i}, ~, ic{i}] = unique(data(:,i));
    X{i} = X{i}(:,C{i});
    
    data(:, i) = ic{i};

    if (~EHR_DATA && i==2)
        % similarity mat of genes will have only existing genes' similarity
        assert(size(X{2}, 1)==850);
        X{i} = X{i}(C{i}, :);
        assert(issymmetric(X{2}));
    end
end