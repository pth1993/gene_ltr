function [X] = TRUNCATE_FEATURES(X, EHR_DATA)

if (EHR_DATA)
    dims = 3;
else
    dims = 2;    
end

for i=1: dims
   stdev = std(X{i}, 0, 2);
   nullfeat = stdev == 0;
   X{i}(nullfeat, :) = [];
   
   %cc = corr(X{i}');
   %meancc = mean(abs(cc));
   
end