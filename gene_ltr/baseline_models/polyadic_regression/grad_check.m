function grad_check(xtmp, b, w, F, dF, C, dC)

EPSILON = 10^-4;

%% perform a gradient check for F and C low-rank matrices/tensors
% F check
for idx=1: 3
    
    for i=1: size(F{idx}, 1)
        for j=1: size(F{idx}, 2)
            
            templus = F; tempminus = F;
            
            templus{idx}(i,j) = F{idx}(i,j) + EPSILON;
            tempS_plus = eval_S(templus, C);
            
            tempminus{idx}(i,j) = F{idx}(i,j) - EPSILON;
            tempS_minus = eval_S(tempminus, C);
            
            analyt = ( obj_fun(xtmp, b, w, tempS_plus) - obj_fun(xtmp, b, w, tempS_minus) ) / (2*EPSILON);
            
            assert(abs( dF{idx}(i,j) - analyt ) < EPSILON);
        end
    end
end

% C check
for ii = 1:2
    for jj = ii+1:3
        
        for i=1: size(C{ii,jj}, 1)
            for j=1: size(C{ii,jj}, 2)
                
                templus = C; tempminus = C;
                
                templus{ii,jj}(i,j) = C{ii,jj}(i,j) + EPSILON;
                tempS_plus = eval_S(F, templus);
                
                tempminus{ii,jj}(i,j) = C{ii,jj}(i,j) - EPSILON;
                tempS_minus = eval_S(F, tempminus);
                
                analyt = ( obj_fun(xtmp, b, w, tempS_plus) - obj_fun(xtmp, b, w, tempS_minus) ) / (2*EPSILON);
                
                assert(abs( dC{ii,jj}(i,j) - analyt ) < EPSILON);
            end
        end
    end
end

% C tensor check

for i=1: size(C{1,2,3}, 1)
    for j=1: size(C{1,2,3}, 2)
        for k=1:  size(C{1,2,3}, 3)
            
            templus = C; tempminus = C;
            
            templus{1,2,3}(i,j,k) = C{1,2,3}(i,j,k) + EPSILON;
            tempS_plus = eval_S(F, templus);
            
            tempminus{1,2,3}(i,j,k) = C{1,2,3}(i,j,k) - EPSILON;
            tempS_minus = eval_S(F, tempminus);
            
            analyt = ( obj_fun(xtmp, b, w, tempS_plus) - obj_fun(xtmp, b, w, tempS_minus) ) / (2*EPSILON);
            
            assert(abs( dC{1,2,3}(i,j,k) - analyt ) < EPSILON);
        end
    end
end

