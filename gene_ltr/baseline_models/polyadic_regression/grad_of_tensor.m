function dF = grad_of_tensor(x,C,idx,xF)

% % DEBUG - IGNORE
% FTx1 = F{1}'*x{1};
% FTx3 = F{3}'*x{3};
% sum_mat = zeros(1, size(F{2}, 2));
% for i=1: size(C, 3)
%     sum_mat = sum_mat + FTx3(i)*FTx1'*C(:,:,i);
% end
% dF_2 = x{2}*sum_mat;

%% computation of gradient the core tensor w.r.t. the shared low-rank matrices F
%C_tilde = tensor(C); % tensor toolbox usage
C_tilde = C;
id = setdiff(1:3, idx);
assert(length(id)==2);
mykr = kron(xF{id(2)}, xF{id(1)});
dF = x{idx} * mykr * double(tenmat(C_tilde, idx))';
end