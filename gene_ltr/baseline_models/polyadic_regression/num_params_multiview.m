function ret = num_params_multiview(d, m)
assert(length(d) == 3); num_of_views = 3;
ret = m*(num_of_views + sum(d)); % sum((d+1).*m)