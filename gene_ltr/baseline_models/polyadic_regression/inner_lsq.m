function [val, vec] = inner_lsq(xtmp,b,w,F,y,IND_MODE)

%% val computation
f = b;
for k=1:3
    if (k==IND_MODE)
        continue;
    end
    f = f + w{k}'*xtmp{k};
end
val = y - f;

%% vec computation
for i=1: 3
    if (i==IND_MODE)
        xF{i} = xtmp{i}';
        continue;
    end
    xF{i} = xtmp{i}'*F{i};
end

dC12 = xF{1}'*xF{2};
dC13 = xF{1}'*xF{3};
dC23 = xF{2}'*xF{3};
%dC123 = bsxfun(@times,repmat(F{1}' * x1 * x2' * F{2},[1 1 m(3)]), reshape(F{3}' * x3,[1 1 m(3)]));
dC123 = double(full(ktensor(1, xF{1}', xF{2}', xF{3}')));

vec = [dC12(:);dC13(:);dC23(:);dC123(:)]';