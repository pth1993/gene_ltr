function opts = INIT_WITH_HOSVD(opts, X, Y, d, m)

% init
F = {};
for i=1: 3
    if (i==opts.IND_MODE)
        continue;
    end
    F{i} = zeros(d(i), m(i));
end

% compute tensor sum
% tensorsum = tensor(zeros(d(1), d(2), d(3)));
if (~opts.sparseY)
    lambda = [];
    for i=1: 3
        A{i} = [];
    end
    
    for i=1: size(Y, 1)
        for j=1: size(Y, 2)
            for k=1: size(Y, 3)
                lambda = [lambda;Y(i,j,k)];
                A{1} = [A{1} X{1}(:, i)];
                A{2} = [A{2} X{2}(:, j)];
                A{3} = [A{3} X{3}(:, k)];
                
                % tensorsum = tensorsum + tensor(ktensor(Y(i,j,k), X{1}(:, i), X{2}(:, j), X{3}(:, k)));
            end
        end
    end
    normalizer = (1/numel(Y));
else
    lambda = zeros(size(Y, 1), 1);
    for i=1: 3
        A{i} = zeros(size(X{i}, 1), size(Y, 1));
    end
    
    for i=1: size(Y, 1)
        lambda(i) = Y(i, end);
        A{1}(:, i) = X{1}(:, Y(i, 1));
        A{2}(:, i) = X{2}(:, Y(i, 2));
        A{3}(:, i) = X{3}(:, Y(i, 3));
        
        % tensorsum = tensorsum + tensor(ktensor(Y(i, end), X{1}(:, Y(i, 1)),  X{2}(:, Y(i, 2)), X{3}(:, Y(i, 3))));
    end
    normalizer = (1/size(Y, 1));
end
% tensorsum = full(ktensor(lambda, A));
tensorsum = ktensor(lambda, A);
tensorsum = normalizer*tensorsum;

% compute nvecs and place them to the correct place in the vector of params
initidx = 2; %first pos belongs to scalar b
for i=1: 3
    if (i==opts.IND_MODE)
        continue;
    end
    initidx = initidx + d(i);
end
for i=1: 3
    if (i==opts.IND_MODE)
        continue;
    end
    
    F{i} = nvecs(tensorsum, i, m(i));
    opts.X_INIT(initidx:initidx+d(i)*m(i)-1) = F{i}(:);
    initidx = initidx + d(i)*m(i);
end