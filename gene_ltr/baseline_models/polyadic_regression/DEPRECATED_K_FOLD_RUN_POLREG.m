function [saved_pr_err, saved_pr_spear, absum_w, absum_S, absum_Score] = DEPRECATED_K_FOLD_RUN_POLREG(myopts, aa, outer_iter_idx, ROOTPATH, FOLDS, EHR_DATA, myfileid)

if (EHR_DATA)
    if (isempty(myfileid))
        myfileid = fopen(sprintf('notes_ehr/notes_%d.txt',outer_iter_idx), 'w');
    end
    path = strcat(ROOTPATH, 'health_data/mimic3_preprocessed/out/');
    results_path = strcat(ROOTPATH, 'papers/polyadic_regression/exps_3way/results_EHR/');
    IND_MODE = [];
    load(strcat(path,'tensor_data_EHR.mat'));
else
    if (isempty(myfileid))
        myfileid = fopen(sprintf('notes_genexpr/notes_%d.txt',outer_iter_idx), 'w');
    end
    path = strcat(ROOTPATH, 'health_data/LINCS_drug_ind_gene_exp/');
    results_path = strcat(ROOTPATH, 'papers/polyadic_regression/exps_3way/results_GENEXPR/');
    IND_MODE = 3; assert(length(IND_MODE) == 1);
    load(strcat(path,'tensor_data_GENEXPR.mat'));
end

for i=1:3
    d(i) = size(X{i}, 1);
    n(i) = size(X{i}, 2);
end

[FEATURE_RANK, opts, prox_param] = DEFINE_PARAMS(myopts, aa, myfileid, IND_MODE, EHR_DATA);
opts.IND_MODE = IND_MODE;
opts.myfileid = myfileid;

m = [FEATURE_RANK(1) FEATURE_RANK(2) FEATURE_RANK(3)]; % test with small m
opts.m = m;
if (~isempty(opts.IND_MODE))
    assert(m(IND_MODE) == d(IND_MODE));
    assert(d(IND_MODE) == n(IND_MODE) && isequal(eye(d(IND_MODE)), X{IND_MODE}));
end
opts.IND_MODE = IND_MODE;
if (isempty(IND_MODE))
    opts.CPP_IND_MODE = -1;
else
    assert(IND_MODE > 0 && IND_MODE < 4);
    opts.CPP_IND_MODE = IND_MODE;
end

if (EHR_DATA)
    opts.flag_opt_C_GD = true;   % if flag_opt_C_GD == true, in each iteration, use gradient descent to optimize C12,C13,C23,C123. Other wise, in each iteration, compute closed form solutions of C12,C13,C23,C123 using least square.
    fprintf(myfileid, 'GD for C (no regularization on C)\n');
else
    opts.flag_opt_C_GD = false;   % if flag_opt_C_GD == true, in each iteration, use gradient descent to optimize C12,C13,C23,C123. Other wise, in each iteration, compute closed form solutions of C12,C13,C23,C123 using least square.
    fprintf(myfileid, 'No GD for C, simple least-squares (no regularization on C as well)\n');
end

opts.use_c_sub = true;   % if use_c_sub == true, use C++ version of sub-function for accelaration. Otherwise, only use matlab version of sub-function, which might be about 50 times slower. The results of different version of sub-funcion are the same.

opts.EPS = 1e-6; % used for the stopping critierion, which is based on the relative error of two successive parameter vectors.
%opts.ALPHA = 1; % reduce forward tracking for stability
opts.MAX_ITERS = 5000;
opts.GEN_PLOTS = false;

opts.sparseY = true; % indicate that we will only evaluate a fraction of the combinations between cross-domain data points

% STANDARDIZE = false;
CENTER_Y = true;

for i=1: FOLDS
    
    % standardize
    %     if (STANDARDIZE)
    %         for j=1: 3
    %             if (j~=IND_MODE)
    %                 [Z{j}, mu{j}, sigma{j}] = zscore(X{j}');
    %
    %                 zerovar = find(sigma{j}==0);
    %                 if (~isempty(zerovar))
    %                     mu{j}(zerovar) = []; sigma{j}(zerovar) = []; Z{j}(:, zerovar) = [];
    %                 end
    %
    %                 X{j} = Z{j}';
    %                 d(j) = size(X{j}, 1);
    %             end
    %         end
    %     end
    
    if (CENTER_Y)
        meanY = mean(trainset{i}(:, end));
        trainset{i}(:, end) = trainset{i}(:, end) - meanY; % center responses
    end
    
    %% train and test
    %[b,w,F,C] = apg_threeway_opt(X, trainset{i}, prox_param, opts);
    fprintf(myfileid, 'training set size: %d \n',size(trainset{i}, 1));
    [b,w,F,C] = apg_threeway_opt(X, trainset{i}, prox_param, opts);
    
    fprintf(myfileid, 'test set size: %d \n',size(testset{i}, 1));
    if (CENTER_Y)
        Y_out = meanY + estimate_Y(X, b, w, F, C, opts.sparseY, testset{i}(:,1:end-1), IND_MODE);
    else
        Y_out = estimate_Y(X, b, w, F, C, opts.sparseY, testset{i}(:,1:end-1), IND_MODE);
    end
    
    pr_err(i) = eval_fun(Y_out, testset{i}(:,end));
    pr_spear(i) = corr(Y_out, testset{i}(:,end), 'type', 'Spearman');
    fprintf(myfileid, 'pr_err: %f, pr_spear: %f\n', pr_err(i), pr_spear(i));
    
    % run only for 1-fold
    saved_pr_err = pr_err(i);
    saved_pr_spear = pr_spear(i);
    
    absum_w = cell(1, 3);
    if (isempty(IND_MODE))
        for j=1:3
            absum_w{j} = sum(abs(w{j}));
        end
    else
        for j=1:3
            if (j~=IND_MODE)
                absum_w{j} = sum(abs(w{j}));
            end
        end
    end
    absum_S = cell(3);
    for j=1:2
        for k=j+1:3
            absum_S{j,k} = sum(sum(abs(F{j}*C{j,k}*F{k}')));
        end
    end
    absum_Score = sum(sum(sum(abs( double ( ttm( tensor(C{1,2,3}), F ) ) ) ) ) );
    return;
    
    
end

%% save
saved_pr_err = mean(pr_err);
saved_pr_spear = mean(pr_spear);
fprintf(myfileid, 'saved_pr_err: %f, saved_pr_spear: %f\n', saved_pr_err, saved_pr_spear);
clear pr_err pr_spear

save(strcat(results_path,'polyadic_',sprintf('%d',outer_iter_idx),'.mat'),'saved_pr_err','saved_pr_spear');


%% plot
% if (PLOTME==1)
%     load(strcat(results_path, '/linear.mat'));
%     load(strcat(results_path, '/fm.mat'));
%     load(strcat(results_path, '/polyadic_8.mat'));
%     pol_err = rank_pr_err;
%     pol_spear = rank_pr_spear;
%     load(strcat(results_path, '/polyadic_10.mat'));
%     pol_err(10) = rank_pr_err(10);
%     pol_spear(10) = rank_pr_spear(10);
%
%     % error
%     figure;
%     hold on;
%     ranks = 2:2:10;
%     plot(ranks, pol_err(ranks), 'b.--', 'MarkerSize', 20);
%     plot(ranks(1), linear_err, 'r*', 'MarkerSize', 10);
%     plot(ranks, rank_factmach_err(ranks), 'ko-', 'MarkerSize', 10);
%
%     %plot(2, linear_interact_err, 'k*', 'MarkerSize', 20);
%     %set(gca,'XTickLabel',{'Linear Model + Pairwise Interactions', 'Linear Model', 'Polyadic Regression (m=2)', ...
%     %   'Polyadic Regression (m=4)', 'Polyadic Regression (m=6)'});
%     ylabel('Mean Squared Error','FontSize',15);
%     xlabel('Low-rank parameter','FontSize',15);
%     leg = legend('Polyadic Regression','Linear Model','Factorization Machines',...
%         'Location','northwest');
%     leg.FontSize = 15;
%     set(gca,'FontSize',15);
%     set(gcf, 'Color', 'w');
%     hold off;
%     export_fig(strcat(results_path, 'EHR_mse.pdf'),'-q101');
%     close all;
%
%     % spearman rank
%     figure;
%     hold on;
%     ranks = 2:2:10;
%     plot(ranks, pol_spear(ranks), 'b.--', 'MarkerSize', 20);
%     plot(ranks(1), linear_spear, 'r*', 'MarkerSize', 10);
%     plot(ranks, rank_factmach_spear(ranks), 'ko-', 'MarkerSize', 10);
%
%     %plot(2, linear_interact_err, 'k*', 'MarkerSize', 20);
%     %set(gca,'XTickLabel',{'Linear Model + Pairwise Interactions', 'Linear Model', 'Polyadic Regression (m=2)', ...
%     %   'Polyadic Regression (m=4)', 'Polyadic Regression (m=6)'});
%     ylabel('Spearman rank correlation coefficient','FontSize',15);
%     xlabel('Low-rank parameter','FontSize',15);
%     leg = legend('Polyadic Regression','Linear Model','Factorization Machines',...
%         'Location','southwest');
%     leg.FontSize = 15;
%     set(gca,'FontSize',15);
%     set(gcf, 'Color', 'w');
%     hold off;
%     export_fig(strcat(results_path, 'EHR_spear.pdf'),'-q101');
%     close all;
% end


fclose(myfileid);