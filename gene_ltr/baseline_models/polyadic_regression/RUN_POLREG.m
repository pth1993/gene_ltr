function [pr_err_validation, pr_spear_validation, absum_w, absum_S, absum_Score, w, F, C] =...
    RUN_POLREG(myopts, aa, outer_iter_idx, ROOTPATH, FOLDS, EHR_DATA, myfileid, COLD_START, SUBSETS_DATA, SINGLE, SUBSETS_FEATURES)

if (EHR_DATA)
    if (isempty(myfileid))
        myfileid = fopen(sprintf('notes_ehr/notes_%d.txt',outer_iter_idx), 'w');
    end
    path = strcat(ROOTPATH, 'health_data/mimic3_preprocessed/out/');
    results_path = strcat(ROOTPATH, 'papers/polyadic_regression/exps_3way/results_EHR/');
    IND_MODE = [];
    load(strcat(path,'tensor_data_EHR.mat'));
else
    if (isempty(myfileid))
        if (SINGLE)
            myfileid = fopen(strcat('notes_genexpr/notes_',int2str(myopts.REG_VALUE_L2_w),'_',int2str(myopts.REG_VALUE_L2_F),'.txt'), 'w');
        else
            myfileid = fopen(sprintf('notes_genexpr/notes_%d.txt',outer_iter_idx), 'w');
        end
    end
    path = strcat(ROOTPATH, 'health_data/LINCS_drug_ind_gene_exp/');
    results_path = strcat(ROOTPATH, 'papers/polyadic_regression/exps_3way/results_GENEXPR/');
    IND_MODE = 3; assert(length(IND_MODE) == 1);
    
    if (COLD_START)
        if (SUBSETS_DATA)
            load(strcat(path,'subsets_tensor_data_GENEXPR.mat'));
            fprintf(myfileid, 'WARNING! RUNNING FOR SUBSETS OF DATA\n');
            subset = input('1/8, 1/4, 1/2 or the whole (1) dataset?')
            assert(subset == 1 || subset==2 || subset == 4 || subset == 8);
            trainset = trainset_subsets{subset};
            
            if (subset == 1)
                load(strcat(path,'tensor_data_coldstart_GENEXPR.mat'));
            end
            
        elseif (SUBSETS_FEATURES)
            load(strcat(path,'feature_subsets_tensor_data_GENEXPR.mat'));
            fprintf(myfileid, 'WARNING! RUNNING FOR FEATURE SUBSETS OF DATA\n');
            subset = input('1/8, 1/4, 1/2 of dataset in terms of gene features?')
            assert(subset==2 || subset == 4 || subset == 8);
            X = X_subsets{subset};
            
        else
            load(strcat(path,'tensor_data_coldstart_GENEXPR.mat'));
            fprintf(myfileid, 'loading COLD START on drugs setting!\n');
        end
    else
        load(strcat(path,'tensor_data_GENEXPR.mat'));
    end
    
end

for i=1:3
    d(i) = size(X{i}, 1);
    n(i) = size(X{i}, 2);
end

[FEATURE_RANK, opts, prox_param] = DEFINE_PARAMS(myopts, aa, myfileid, IND_MODE, EHR_DATA, COLD_START);
if (SUBSETS_DATA || SUBSETS_FEATURES)
    opts.SUBSETS = 1;
else
    opts.SUBSETS = 0;
end

opts.IND_MODE = IND_MODE;
opts.myfileid = myfileid;

m = [FEATURE_RANK(1) FEATURE_RANK(2) FEATURE_RANK(3)]; % test with small m
opts.m = m;
if (~isempty(opts.IND_MODE))
    assert(m(IND_MODE) == d(IND_MODE));
    assert(d(IND_MODE) == n(IND_MODE) && isequal(eye(d(IND_MODE)), X{IND_MODE}));
end
opts.IND_MODE = IND_MODE;
if (isempty(IND_MODE))
    opts.CPP_IND_MODE = -1;
else
    assert(IND_MODE > 0 && IND_MODE < 4);
    opts.CPP_IND_MODE = IND_MODE;
end

if (EHR_DATA)
    opts.flag_opt_C_GD = true;   % if flag_opt_C_GD == true, in each iteration, use gradient descent to optimize C12,C13,C23,C123. Other wise, in each iteration, compute closed form solutions of C12,C13,C23,C123 using least square.
    fprintf(myfileid, 'GD for C (no regularization on C)\n');
else
    opts.flag_opt_C_GD = false;   % if flag_opt_C_GD == true, in each iteration, use gradient descent to optimize C12,C13,C23,C123. Other wise, in each iteration, compute closed form solutions of C12,C13,C23,C123 using least square.
    fprintf(myfileid, 'No GD for C, simple least-squares (no regularization on C as well)\n');
end

opts.use_c_sub = true;   % if use_c_sub == true, use C++ version of sub-function for accelaration. Otherwise, only use matlab version of sub-function, which might be about 50 times slower. The results of different version of sub-funcion are the same.

opts.EPS = 1e-6; % used for the stopping critierion, which is based on the relative error of two successive parameter vectors.
%opts.ALPHA = 1; % reduce forward tracking for stability
opts.MAX_ITERS = 2000;
opts.GEN_PLOTS = false;

opts.sparseY = true; % indicate that we will only evaluate a fraction of the combinations between cross-domain data points

opts.CENTER_Y = true;

if (opts.CENTER_Y)
    opts.meanY = mean(trainset(:, end));
    trainset(:, end) = trainset(:, end) - opts.meanY; % center responses
end

%% train
opts.validationset = validationset; % to check how the error is dropping every 100 iters
opts.evalFlag = 1; % to check how the error is dropping every 100 iters

fprintf(myfileid, 'training set size: %d \n',size(trainset, 1));
[b,w,F,C] = apg_threeway_opt(X, trainset, prox_param, opts);

%% validation set
fprintf(myfileid, 'validation set size: %d \n',size(validationset, 1));
if (opts.CENTER_Y)
    Y_out = opts.meanY + estimate_Y(X, b, w, F, C, opts.sparseY, validationset(:,1:end-1), IND_MODE);
else
    Y_out = estimate_Y(X, b, w, F, C, opts.sparseY, validationset(:,1:end-1), IND_MODE);
end
pr_mae_validation = mean(abs(Y_out - validationset(:,end)));
pr_err_validation = eval_fun(Y_out, validationset(:,end));
pr_spear_validation = corr(Y_out, validationset(:,end), 'type', 'Spearman');
fprintf(myfileid, 'validation set (to choose hyperparams): MAE: %f, MSE: %f, SPEARMAN: %f\n', pr_mae_validation, pr_err_validation, pr_spear_validation);

%% test set
fprintf(myfileid, 'test set size: %d \n',size(testset, 1));
if (opts.CENTER_Y)
    Y_out = opts.meanY + estimate_Y(X, b, w, F, C, opts.sparseY, testset(:,1:end-1), IND_MODE);
else
    Y_out = estimate_Y(X, b, w, F, C, opts.sparseY, testset(:,1:end-1), IND_MODE);
end
pr_mae_test = mean(abs(Y_out - testset(:,end)));
pr_err_test = eval_fun(Y_out, testset(:,end));
pr_spear_test = corr(Y_out, testset(:,end), 'type', 'Spearman');
fprintf(myfileid, 'test set (reported stats): MAE: %f, MSE: %f, SPEARMAN: %f\n', pr_mae_test, pr_err_test, pr_spear_test);

%% record magnitude of parameters
absum_w = cell(1, 3);
if (isempty(IND_MODE))
    for j=1:3
        absum_w{j} = sum(abs(w{j}));
    end
else
    for j=1:3
        if (j~=IND_MODE)
            absum_w{j} = sum(abs(w{j}));
        end
    end
end
absum_S = cell(3);
for j=1:2
    for k=j+1:3
        absum_S{j,k} = sum(sum(abs(F{j}*C{j,k}*F{k}')));
    end
end
absum_Score = sum(sum(sum(abs( double ( ttm( tensor(C{1,2,3}), F ) ) ) ) ) );

fclose(myfileid);