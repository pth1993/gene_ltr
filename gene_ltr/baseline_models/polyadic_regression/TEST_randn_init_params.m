function [b,w,F,C] = TEST_randn_init_params(n, d, m)

b = randn;  % Gaussian random number
w = {};
for i = 1: length(d)
    w{i}= randn(d(i),1); % Gaussian random number
end

F = {};
for i = 1: length(d)
    F{i}= randn(d(i),m(i)); % Gaussian random number
end

C={};
for i = 1:2
    for j = i+1:3
        C{i,j} = randn(m(i),m(j));  % Gaussian random number
    end
end
C{1,2,3} = randn(m(1),m(2),m(3)); % Gaussian random number