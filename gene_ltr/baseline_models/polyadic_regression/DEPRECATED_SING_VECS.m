% DEPREC_STANDARDIZE


%% LINEAR

k = [0 .001 .01 .05];
for i=1: FOLDS
    
    %% train
    %[Z, mu, sigma, meanY, DESIGNMAT, TESTMAT] =...
     %   STANDARDIZE_DESIGNMAT(DESIGNMAT, TESTMAT, trainset, i);
    meanY = mean(trainset{i}(:, end));
 
    [coeff, score, latent] = pca(DESIGNMAT{i});
    numfactors = truncSingVals(latent, .9);
    Z = score(:, 1:numfactors);
    W = coeff(:, 1:numfactors);
    
    %LM = fitglm(DESIGNMAT{i}, trainset{i}(:, end), 'linear','Distribution','poisson');
    %LM = fitglm(DESIGNMAT{i}, trainset{i}(:, end), 'interactions');
    LM2 = ridge(trainset{i}(:, end) - meanY, Z, k);
    %[LM2, fitinfo] = lasso(Z, trainset{i}(:, end) - meanY, 'alpha', .5);
    LM = LM2(:, end);
    
    %LM = fitglm(Z, trainset{i}(:, end) - meanY, 'linear');
    
    %% test
    % testmat = STANDARDIZE_TESTMAT(TESTMAT{i}, mu, sigma);
    
    % LM_PRED = meanY + predict(LM, testmat);
    % LM_PRED = meanY + testmat*LM;
    meanMat = repmat(mean(DESIGNMAT{i}), size(TESTMAT{i}, 1), 1);
    LM_PRED = meanY + (TESTMAT{i} - meanMat) * W * LM;
    
    LM_ERR(i) = eval_fun(LM_PRED, testset{i}(:,end))
    LM_SPEAR(i) = corr(LM_PRED, testset{i}(:,end), 'type', 'Spearman')
end