function i = truncSingVals(svs, thresh)

    currsum = 0;
    totalsum = sum(svs);
    
    if totalsum == 0
        i = length(svs);
        return;
    end
    
    for i = 1:length(svs)
        currsum = currsum + svs(i);
        if (currsum/totalsum > thresh)
            break;
        end
    end
    
    i = max(1, i);
end