clc ; clear all; close all;
currpath = '/Users/joachim/Dropbox/GeorgiaTech/papers/polyadic_regression/exps_3way/three-way/';
oldpath = '/Users/joachim/Dropbox/GeorgiaTech/papers/polyadic_regression/exps_2way_custom/three-way/';

rng('default');
%% super param
IND_MODE = 2;

m = [2 3 5];
d = [9 3 7];
n = [23 3 31];
if (~isempty(IND_MODE))
    d(IND_MODE) = m(IND_MODE);
    n(IND_MODE) = d(IND_MODE);
end

for i=1: 3
    if (i==IND_MODE)
        continue;
    end
    prox_param.w{i}.method = 'L2';  % regularization method, options are : 'L2' for L2 norm; 'L1' for L1 norm;  'EN' for Elastic Net;  'GS' for Group Sparsity, which is only valid for matrix;
    prox_param.w{i}.param = 1/d(i); % coefficient of regularization, e.g., in ( lambda ||x||_2^2 ), lambda is the coefficient of regularization.
    prox_param.F{i}.method = 'L2';  % regularization method for F1
    prox_param.F{i}.param = 1/(d(i)*m(i)); % coefficient of regularization for F1, which can be the same with the coefficient of regularization for non-partial-induction modes
end

opts = [];
opts.m = m;
opts.initial_scale = 1e-0;
opts.MAX_ITERS = 200;
% opts.EPS = 1e-6;
% for example:
% opts.QUIET = true;
% opts.GEN_PLOTS = false;
% opts.USE_RESTART = false;
% opts.ALPHA = 1;

%% param
b = randn;
w = {};
for i = 1:3
    if (i==IND_MODE)
        continue;
    end
    
    w{i}= randn(d(i),1);
end

F = {};
for i = 1:3
    if (i==IND_MODE)
        F{IND_MODE} = eye(d(IND_MODE));
        continue;
    end
    F{i}= rand(d(i),m(i));
end

C={};
for i = 1:2
    for j = i+1:3
        C{i,j} = randn(m(i),m(j));
    end
end
C{1,2,3} = randn(m(1),m(2),m(3));
%% data
%train
X = {};
for i = 1:3
    if (i==IND_MODE)
        X{IND_MODE} = eye(d(IND_MODE));
        continue;
    end
    X{i}= randn(d(i),n(i));
end



%% obj_fun
x = {};
for i = 1:3
    x{i} = X{i}(:,1);
end

disp('matlab1')
tic
xF{1} = x{1}'*F{1};
xF{2} = x{2}'*F{2};
xF{3} = x{3}'*F{3};
matlabC = C;
matlabC{1,2,3} = tensor(C{1,2,3});
val = obj_fun(x,b,w,matlabC,xF,IND_MODE);
toc

cd(oldpath);
disp('matlab2')
tic
S = {};
S{1,2} = F{1} * C{1,2};
S{1,3} = F{1} * C{1,3} * F{3}';
S{2,3} = C{2,3} * F{3}';
S{1,2,3} = threeway_prod_S(C{1,2,3},F); 
val_c = obj_fun(x,b,w,S);
toc
err = val-val_c;
err = norm(err(:));disp(err)
cd(currpath);
%% grad_of_tensor
% for gradord=1: 3
%     
%     sparseY = 1;
%     if (sparseY == 1)
%         load vars.mat;
%     else
%         co_occur_tensor = [];
%     end
%     
%     for i = 1:3
%         [d(i),n(i)] = size(X{i});
%     end
%     [b,w,F,C] = TEST_randn_init_params(n, d, m);
%     
%     disp('***************************************************')
%     disp('test grad_of_tensor_c.cpp')
%     x = {};
%     for i = 1:3
%         x{i} = X{i}(:,1);
%     end
%     disp('matlab')
%     tic
%     matlabC = C;
%     matlabC{1,2,3} = tensor(C{1,2,3});
%     xF{1} = x{1}'*F{1};
%     xF{2} = x{2}'*F{2};
%     xF{3} = x{3}'*F{3};
%     dF = grad_of_tensor(x,matlabC{1,2,3},gradord,xF);
%     toc
%     
%     cd(oldpath);
%     tic
%     dF_c = grad_of_tensor(x,C{1,2,3},F,gradord);
%     toc
%     disp('difference between matlab and matlab2')
%     err = dF-dF_c;
%     err = norm(err(:));disp(err)
%     cd(currpath);
% 
% end

%% threeway_grad_with_C_sub
sparseY = 1;
nonzeros = 1000;
MAXCOUNT = 500;
for i=1: 3
   data(:, i) = randi(n(i), nonzeros, 1);
end
data(:, 4) = randi(MAXCOUNT, nonzeros, 1);
Y = estimate_Y(X,b,w,F,C, sparseY, data(:, 1:end-1), IND_MODE);

opts.sparseX = 0;
if (opts.sparseX)
    density = .01;
    disp('TEST WITH SPARSE X');
    for jj=1:3
        X{jj} = sprand(size(X{jj},1), size(X{jj},2), density);
    end
end

disp('***************************************************')
disp('test threeway_grad_with_C_sub_c.cpp')
dim_x = length([b;w{1};w{3};F{1}(:);F{3}(:);C{1,2}(:);C{1,3}(:);C{2,3}(:);C{1,2,3}(:)]);
disp('matlab')
matlabC = C;
matlabC{1,2,3} = tensor(C{1,2,3});
tic
dx = threeway_grad_with_C_sub(X,b,w,F,matlabC,data(:,end),dim_x,sparseY, data(:,1:end-1),IND_MODE);
toc

cd(oldpath);
tic
dx2 = threeway_grad_with_C_sub(X,b,w,F,C,data(:,end),dim_x, sparseY, data(:, 1:end-1));
toc
disp('difference between matlab and matlab2')
err = dx-dx2;
err = norm(err(:));disp(err)
cd(currpath);