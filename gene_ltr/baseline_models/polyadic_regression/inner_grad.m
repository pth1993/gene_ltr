function dx = inner_grad(dx, xtmp, b, w, Y, F, C, IND_MODE, flag_opt_C_GD)

for i=1: 3
    if (i==IND_MODE)
        xF{i} = xtmp{i}';
        continue;
    end
    xF{i} = xtmp{i}'*F{i};
end

obj_val = obj_fun(xtmp,b,w,C,xF,IND_MODE);

err = 2 * ( obj_val - Y ); % square loss

db = 1;
dw = [];
for i=1: 3
    if (i==IND_MODE)
        continue;
    end
    dw = [dw; xtmp{i}];
end

grdten = cell(3, 1);
for i=1:3
    if (i==IND_MODE)
        continue;
    end
    
    grdten{i} =  grad_of_tensor(xtmp, C{1,2,3}, i, xF);
    
    if (i==1)
        dF{1}= xtmp{1} * xF{2} * C{1,2}' + xtmp{1} * xF{3} * C{1,3}' + grdten{1};
    elseif (i==2)
        dF{2} = xtmp{2} * xF{1} * C{1,2} + xtmp{2} * xF{3} * C{2,3}' + grdten{2};
    elseif (i==3)
        dF{3} = xtmp{3} * xF{2} * C{2,3}  + xtmp{3} * xF{1} * C{1,3}  + grdten{3};
    else
        assert(1>2);
    end
end

if (flag_opt_C_GD)
    dC{1,2} = xF{1}'*xF{2};
    dC{1,3} = xF{1}'*xF{3};
    dC{2,3} = xF{2}'*xF{3};
    ten_as_mat = xF{1}' * kron(xF{3}, xF{2});
    dC{1,2,3} = reshape(ten_as_mat, size(C{1,2,3}));
end

% grad_check(xtmp, b, w, F, dF, C, dC); % DEBUG: gradient checking
final = [db; dw];
for i=1: 3
    if (i==IND_MODE)
        continue;
    end    
    final = [final; dF{i}(:)];
end

if (flag_opt_C_GD)
    final = [final;dC{1,2}(:);dC{1,3}(:);dC{2,3}(:);dC{1,2,3}(:)];
end

dx = dx + err * final;
