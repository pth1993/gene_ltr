clc;clear;close all;
addpath('/fs/project/PAS1536/hoang/codes/apg');
addpath('/fs/project/PAS1536/hoang/codes/tensor_toolbox');
addpath('/fs/project/PAS1536/hoang/codes/armadillo-7.200.2/include');
ARMA_INCLUDE = '-I/fs/project/PAS1536/hoang/codes/armadillo-7.200.2/include';

mex -I/fs/project/PAS1536/hoang/codes/armadillo-7.200.2/include obj_fun_c.cpp CXXFLAGS='\$CXXFLAGS -fopenmp -O2' LDFLAGS='\$LDFLAGS -fopenmp' -largeArrayDims
mex -I/fs/project/PAS1536/hoang/codes/armadillo-7.200.2/include grad_of_tensor_c.cpp CXXFLAGS='\$CXXFLAGS -fopenmp -O2' LDFLAGS='\$LDFLAGS -fopenmp' -largeArrayDims
mex -I/fs/project/PAS1536/hoang/codes/armadillo-7.200.2/include threeway_grad_sub_c.cpp CXXFLAGS='\$CXXFLAGS -fopenmp -O2' LDFLAGS='\$LDFLAGS -fopenmp' -largeArrayDims
mex -I/fs/project/PAS1536/hoang/codes/armadillo-7.200.2/include opt_C_close_form_sub_c.cpp CXXFLAGS='\$CXXFLAGS -fopenmp -O2' LDFLAGS='\$LDFLAGS -fopenmp' -largeArrayDims

% mex -I/fs/project/PAS1536/hoang/codes/armadillo-7.200.2/include obj_fun_c.cpp CFLAGS='$CFLAGS -fopenmp' -LDFLAGS='$LDFLAGS -fopenmp' -largeArrayDims
% mex -I/fs/project/PAS1536/hoang/codes/armadillo-7.200.2/include grad_of_tensor_c.cpp CFLAGS='$CFLAGS -fopenmp' -LDFLAGS='$LDFLAGS -fopenmp' -largeArrayDims
% mex -I/fs/project/PAS1536/hoang/codes/armadillo-7.200.2/include threeway_grad_sub_c.cpp CFLAGS='$CFLAGS -fopenmp' -LDFLAGS='$LDFLAGS -fopenmp' -largeArrayDims
% mex -I/fs/project/PAS1536/hoang/codes/armadillo-7.200.2/include opt_C_close_form_sub_c.cpp CFLAGS='$CFLAGS -fopenmp' -LDFLAGS='$LDFLAGS -fopenmp' -largeArrayDims
% mex(ARMA_INCLUDE, 'obj_fun_c.cpp', -COMPFLAGS "$COMPFLAGS /openmp", '-largeArrayDims');
% mex(ARMA_INCLUDE, 'grad_of_tensor_c.cpp', '-largeArrayDims');
% mex(ARMA_INCLUDE, 'threeway_grad_sub_c.cpp', '-largeArrayDims');
% mex(ARMA_INCLUDE, 'opt_C_close_form_sub_c.cpp', '-largeArrayDims');

%% super param
opts = [];

IND_MODE = 3;
opts.IND_MODE = IND_MODE;
if (isempty(IND_MODE))
    CPP_IND_MODE = -1;
else
    assert(IND_MODE > 0 && IND_MODE < 4);
    CPP_IND_MODE = IND_MODE;
end
opts.CPP_IND_MODE = CPP_IND_MODE;
opts.flag_opt_C_GD = 0;

m = [2 3 10];
d = [5 6 10];
n = [8 9 10];

prox_param = [];
for i = 1:3
    if (i==IND_MODE)
        continue;
    end
    prox_param.w{i}.method = 'L2'; prox_param.w{i}.param = 1/d(i);
end
for i = 1:3
    if (i==IND_MODE)
        continue;
    end
    prox_param.F{i}.method = 'L2'; prox_param.F{i}.param = 1/(d(i)*m(i));
end

opts.m = m;
opts.initial_scale = 1e-0;
opts.MAX_ITERS = 200;
% opts.EPS = 1e-6;
% for example:
% opts.QUIET = true;
% opts.GEN_PLOTS = false;
% opts.USE_RESTART = false;
% opts.ALPHA = 1;

%% param
b = randn;
w = cell(1,3);
for i = 1:3
    if (i==IND_MODE)
        continue;
    end
    w{i}= randn(d(i),1);
end

F = {};
for i = 1:3
    if (i==IND_MODE)
        F{i} = eye(d(i));
        continue;
    end
    F{i}= rand(d(i),m(i));
end

C={};
for i = 1:2
    for j = i+1:3
        C{i,j} = randn(m(i),m(j));
    end
end
C{1,2,3} = randn(m(1),m(2),m(3));

%% data

%train
X = {};
for i = 1:3
    if (i==IND_MODE)
        X{i} = eye(d(i));
        continue;
    end
    X{i}= randn(d(i),n(i));
end

matlabC = C;
matlabC{1,2,3} = tensor(C{1,2,3}, m);
Y = estimate_Y(X,b,w,F,matlabC,false,0,IND_MODE);
Y = Y + 0.01 * randn(size(Y));

%% obj_fun
disp('***************************************************')
disp('test obj_fun_c.cpp')
x = {};
for i = 1:3
    x{i} = X{i}(:,1);
end

disp('matlab')
tic
xF{1} = x{1}'*F{1};
xF{2} = x{2}'*F{2};
xF{3} = x{3}'*F{3};
matlabC = C;
matlabC{1,2,3} = tensor(C{1,2,3}, m);
val = obj_fun(x,b,w,matlabC,xF,IND_MODE);
toc
disp('C++')
tic
val_c = obj_fun_c(CPP_IND_MODE,x{1},x{2},x{3},b,w{1},w{2},w{3},C{1,2},C{1,3},C{2,3},C{1,2,3},xF{1},xF{2},xF{3});
toc
disp('difference between matlab and C++:')
err = val-val_c;
err = norm(err(:));disp(err)

%% grad_of_tensor
disp('***************************************************')
disp('test grad_of_tensor_c.cpp')
x = {};
for i = 1:3
    x{i} = X{i}(:,1);
end
disp('matlab')
tic
matlabC = C;
matlabC{1,2,3} = tensor(C{1,2,3}, m);
xF{1} = x{1}'*F{1};
xF{2} = x{2}'*F{2};
xF{3} = x{3}'*F{3};
dF = grad_of_tensor(x,matlabC{1,2,3},3,xF);
toc
disp('C++')
tic
dF_c = grad_of_tensor_c(x{1},x{2},x{3},3,xF{1},xF{2},xF{3},C{1,2,3});
toc
disp('difference between matlab and C++:')
err = dF-dF_c;
err = norm(err(:));disp(err)

%% threeway_grad_with_C_sub
sparseY = 0;
opts.sparseY = sparseY;

if (sparseY)
    nonzeros = 1000;
    MAXCOUNT = 500;
    for i=1: 3
        data(:, i) = randi(n(i), nonzeros, 1);
    end
    data(:, 4) = randi(MAXCOUNT, nonzeros, 1);
else
    data = Y;
end
clear Y


disp('***************************************************')
disp('test threeway_grad_with_C_sub_c.cpp')
templen = b;
for i=1: 3
    if (IND_MODE == i)
        continue;
    end
    templen = [templen; w{i}];
end
for i=1: 3
    if (IND_MODE == i)
        continue;
    end
    templen = [templen; F{i}(:)];
end
if (opts.flag_opt_C_GD)
    templen = [templen; C{1,2}(:);C{1,3}(:);C{2,3}(:);C{1,2,3}(:)];
end
dim_x = length(templen);

disp('matlab')
tic
matlabC = C;
matlabC{1,2,3} = tensor(C{1,2,3}, m);
if (sparseY)
    dx = threeway_grad_sub(X,b,w,F,matlabC,data(:,end),dim_x, sparseY, data(:,1:end-1), opts.IND_MODE, opts.flag_opt_C_GD);
else
    dx = threeway_grad_sub(X,b,w,F,matlabC,data,dim_x, 0, [], opts.IND_MODE, opts.flag_opt_C_GD);
end
toc

disp('C++')
tic
if (sparseY)
    dx_c = threeway_grad_sub_c(CPP_IND_MODE,opts.flag_opt_C_GD,X{1},X{2},X{3},b,w{1},w{2},w{3},F{1},F{2},F{3},C{1,2},C{1,3},C{2,3},C{1,2,3},dim_x, sparseY, data(:,end), data(:, 1:end-1));
else
    dx_c = threeway_grad_sub_c(CPP_IND_MODE,opts.flag_opt_C_GD,X{1},X{2},X{3},b,w{1},w{2},w{3},F{1},F{2},F{3},C{1,2},C{1,3},C{2,3},C{1,2,3},dim_x, sparseY, data);
end
toc
disp('difference between matlab and C++:')
err = dx-dx_c;
err = norm(err(:));disp(err)

%% opt_C_close_form_sub
disp('***************************************************')
disp('test opt_C_close_form_sub_c.cpp')
if opts.sparseY==false
    numel_loss = numel(data);  % number of all targets
else
    numel_loss = size(data, 1); % non-zero number
end
numel_C = 0;
for i = 1:2
    for j = i+1:3
        numel_C = numel_C + m(i)*m(j);
    end
end
numel_C = numel_C + prod(m);
disp('matlab')
tic
[result,Y_C] = opt_C_close_form_sub(X,b,w,F,data, numel_loss, numel_C, opts.sparseY, opts.IND_MODE);
toc

disp('C++')
tic
if (opts.sparseY)
    [result_c,Y_C_c] = opt_C_close_form_sub_c(opts.CPP_IND_MODE,X{1},X{2},X{3},b,w{1},w{2},w{3},F{1},F{2},F{3}, numel_loss, numel_C, opts.sparseY, data(:,end), data(:,1:end-1));
else
    [result_c,Y_C_c] = opt_C_close_form_sub_c(opts.CPP_IND_MODE,X{1},X{2},X{3},b,w{1},w{2},w{3},F{1},F{2},F{3}, numel_loss, numel_C, opts.sparseY, data);
end
toc
disp('difference between matlab and C++:')
err = result-result_c;
err = norm(err(:));disp(err)
err = Y_C-Y_C_c;
err = norm(err(:));disp(err)