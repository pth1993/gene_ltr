function [Z, mu, sigma, meanY, DESIGNMAT, TESTMAT, zerovar] = STANDARDIZE_DESIGNMAT(DESIGNMAT, TESTMAT, trainset, i)

[Z, mu, sigma] = zscore(DESIGNMAT{i});

% if (~isempty(IND_MODE))
%    assert(1>2); 
% end

% drop features with 0 variation
zerovar = find(sigma==0);
if (~isempty(zerovar))
    mu(zerovar) = []; sigma(zerovar) = [];
    Z(:, zerovar) = []; DESIGNMAT{i}(:, zerovar) = []; TESTMAT{i}(:, zerovar) = [];
end
meanY = mean(trainset{i}(:, end));