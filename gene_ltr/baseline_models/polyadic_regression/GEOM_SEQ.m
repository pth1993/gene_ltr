function seq = GEOM_SEQ(num_samples, l0, base)

for i=1: num_samples
    seq(i) = l0*(base^i);
end