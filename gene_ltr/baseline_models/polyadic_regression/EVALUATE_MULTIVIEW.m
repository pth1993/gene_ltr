clear all; close all;
ROOTPATH = '/Users/joachim/Dropbox/GeorgiaTech/';
FOLDS = 5;

EHR_DATA = 0;
COLD_START = 1;
VALID_OR_TEST = 0; % 1 FOR VALIDATION SET

if (EHR_DATA)
    
else
    path = strcat(ROOTPATH, 'health_data/LINCS_drug_ind_gene_exp/');
    if (COLD_START)
        %path_for_MultiView = strcat(ROOTPATH, 'papers/polyadic_regression/exps_3way/competitors/MultiView/COLDSTART_output'); %output_3
        path_for_MultiView = strcat(ROOTPATH, 'papers/polyadic_regression/exps_3way/competitors/MultiView/COLDSTART_output_5times/5');
        load(strcat(path,'tensor_data_coldstart_GENEXPR.mat'));
    else
        path_for_MultiView = strcat(ROOTPATH, 'papers/polyadic_regression/exps_3way/competitors/MultiView/output_3'); %output_3
        load(strcat(path,'tensor_data_GENEXPR.mat'));
    end
end

ranks = [3];
%regs = {'0.0' '0.001' '0.01' '0.1'};
regs = {'0.0'};
%iters = 50:50:500;
iters = 50:50:150;

load(strcat(path_for_MultiView, sprintf('/meanY_%d.mat', 1)));

for j=1:length(ranks)
    for k=1: length(regs)
        for l=1: length(iters)
            
            if (VALID_OR_TEST)
                filename = strcat(path_for_MultiView, sprintf('/val_%d_%s_%d/part-00000',ranks(j),regs{k},iters(l)));
            else
                filename = strcat(path_for_MultiView, sprintf('/test_%d_%s_%d/part-00000',ranks(j),regs{k},iters(l)));
            end
            
            fileID = fopen(filename);
            C = textscan(fileID,'(%d,%f)');
            MV_PRED = C{2};
            fclose(fileID);
            
            % de-center responses based on the mean of the y_i's of the training
            MV_PRED = meanY + MV_PRED;
            
            if (VALID_OR_TEST)
                MV_ERR(j,k,l) = eval_fun(MV_PRED, validationset(:,end));
                MV_SPEAR(j,k,l) = corr(MV_PRED, validationset(:,end), 'type', 'Spearman');
            else
                MV_ERR(j,k,l) = eval_fun(MV_PRED, testset(:,end));
                MV_SPEAR(j,k,l) = corr(MV_PRED, testset(:,end), 'type', 'Spearman');
            end
        end
    end
end
MV_SPEAR
