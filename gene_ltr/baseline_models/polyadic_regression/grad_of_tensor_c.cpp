#include "armaMex.hpp"
#include "math.h"
//#ifdef OMPINCL
//#include <omp.h>
//#endif

mat grad_of_tensor(mat x1, mat x2, mat x3,cube C ,double idx,mat xF1,mat xF2,mat xF3)
{   
    mat dF;

    if(abs(idx-1)<1e-6)
    { 
        mat sum_cols = zeros(xF1.n_cols, C.n_slices);

        //#pragma omp parallel for
        for(int i = 0;i<C.n_slices;i++)
        {             
           sum_cols.col(i) = xF3(i) * C.slice(i) * xF2.t();
        }
        
        dF = x1 * sum(sum_cols, 1).t();
  
    }
    else if(abs(idx-2)<1e-6)
    { 
        mat sum_cols = zeros(xF2.n_cols, C.n_slices);

        //#pragma omp parallel for
        for(int i = 0;i<C.n_slices;i++)
        {
            sum_cols.col(i) = xF3(i) * C.slice(i).t() * xF1.t() ;
        } 
        
        dF = x2 * sum(sum_cols, 1).t();
  
    }
    else if(abs(idx-3)<1e-6)
    { 
        vec v = zeros(C.n_slices,1);

        //#pragma omp parallel for
        for(int i = 0;i<C.n_slices;i++)
        {
            mat tmp = xF1 * C.slice(i) * xF2.t(); 
            v(i) = tmp(0,0);
        }
         dF = x3 * v.t();
    }
     
    return dF; 
}




void
mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
  {
     int argctr = 0;
     mat x1 = armaGetPr(prhs[argctr++]);
     mat x2 = armaGetPr(prhs[argctr++]);
     mat x3 = armaGetPr(prhs[argctr++]);   
     double idx = armaGetDouble(prhs[argctr++]);
     mat xF1 = armaGetPr(prhs[argctr++]);
     mat xF2 = armaGetPr(prhs[argctr++]);
     mat xF3 = armaGetPr(prhs[argctr++]);

    cube C123(xF1.n_cols, xF2.n_cols, xF3.n_cols);
    int ro = C123.n_rows, co = C123.n_cols, sl = C123.n_slices;
    if (ro==1 && co==1 && sl==1)
        C123 = armaGetDouble(prhs[argctr++]); 
    else if (sl==1){
        mat tempten = armaGetPr(prhs[argctr++]);
        C123.slice(0) = tempten;
    }
    else
        C123 = armaGetCubePr(prhs[argctr++]); 
   
     mat dF = grad_of_tensor(x1,x2,x3,C123,idx,xF1,xF2,xF3);
 
     mat mat_out = dF;
 
     plhs[0] = armaCreateMxMatrix(mat_out.n_rows, mat_out.n_cols);


     armaSetPr(plhs[0], mat_out);
  
  return;
  }
