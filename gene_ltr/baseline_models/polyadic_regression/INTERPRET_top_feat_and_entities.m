function [feat_group, group] = INTERPRET_top_feat_and_entities(F_mat, X_mat, PERCENTILE, percent, GENES)

if (GENES) % handle the gene case differently, since the features are not categorical (we need a thresholding criterion, based on which we consider that a gene will be included in the corresponding group of genes)
    GENE_THRESHOLD = prctile(X_mat(:), PERCENTILE);
end

for i=1: size(F_mat, 2)
    temp = F_mat(:, i);
    temp(temp < prctile(temp, PERCENTILE)) = 0;
    feat_group{i} = find(temp);
end

if (~GENES)
    for i=1: size(F_mat, 2)
        group{i} = [];
        for j=1: size(X_mat, 2)
            if (length(intersect(find(X_mat(:, j)), feat_group{i}))>=(length(feat_group{i})/percent)) %1/3 of the important drug features have to be present in a drug for it to be considered
                group{i} = [group{i}; j];
            end
        end
    end
else
    for i=1: size(F_mat, 2)
        group{i} = [];
        for j=1: size(X_mat, 2)
            temp = find(X_mat(:, j) >= GENE_THRESHOLD);
            if (length(intersect(temp, feat_group{i}))>=(length(feat_group{i})/percent)) %1/3 of the important drug features have to be present in a drug for it to be considered
                group{i} = [group{i}; j];
            end
        end
    end
end

