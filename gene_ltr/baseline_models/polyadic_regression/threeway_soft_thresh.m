function v = threeway_soft_thresh(x, t, opts)

d = zeros(3,1);     % dimension of Xi, i = 1,2,3;
n = zeros(3,1);     % sample size of Xi, i = 1,2,3;
for i = 1:3
    [d(i),n(i)] = size(opts.X{i});
end
m = opts.m;          % number of columns of Fi, i = 1,2,3;

% re-organize the parameters into the forms of (b,w,F) from the vector x.
b = x(1); x(1) = [];
w = {};
for i=1: 3
    if (i==opts.IND_MODE)
        continue;
    end
    w{i} = x(1:d(i)); x(1:d(i)) = [];
end
F = {};
for i=1: 3
    if (i==opts.IND_MODE)
        continue;
    end
    F{i} = reshape(x(1:d(i)*m(i)),[d(i) m(i)]); x(1:d(i)*m(i)) = [];
end
%------------------------------------------------------------------------------------
% regularization of the C parameters as well!
if (~isempty(opts.prox_param.C{1,2}.method))
    assert(opts.flag_opt_C_GD);
    C = {};
    for i = 1:2
        for j = i+1:3
            C{i,j} = reshape(x(1:m(i)*m(j)),[m(i) m(j)]); x(1:m(i)*m(j)) = [];
        end
    end
    C{1,2,3} = reshape(x(1:m(1)*m(2)*m(3)),[m(1) m(2) m(3)]); x(1:m(1)*m(2)*m(3)) = [];
end
%------------------------------------------------------------------------------------


%% soft_thresh for each parameter
for i=1: 3
    if (i==opts.IND_MODE)
        continue;
    end
    w{i} = sub_prox_fun(w{i},t,opts.prox_param.w{i}.method,opts.prox_param.w{i}.param);
    F{i} = sub_prox_fun(F{i},t,opts.prox_param.F{i}.method,opts.prox_param.F{i}.param);
    
    if (opts.POSITIVE_F)
        F{i} = max(0,F{i});
    end
end

%------------------------------------------------------------------------------------
% regularization of the C parameters as well!
if (~isempty(opts.prox_param.C{1,2}.method))
    for i = 1:2
        for j = i+1:3
            C{i,j} = sub_prox_fun(C{i,j},t,opts.prox_param.C{i,j}.method,opts.prox_param.C{i,j}.param);
        end
    end
    C{1,2,3} = sub_prox_fun(C{1,2,3},t,opts.prox_param.C{1,2,3}.method,opts.prox_param.C{1,2,3}.param);
end
%------------------------------------------------------------------------------------


%% re-organize the parameters into the parameter vector x.
v = b;
for i=1: 3
    if (i==opts.IND_MODE)
        continue;
    end
    v = [v; w{i}];
end
for i=1: 3
    if (i==opts.IND_MODE)
        continue;
    end
    v = [v; F{i}(:)];
end

%------------------------------------------------------------------------------------
% regularization of the C parameters as well!
if (~isempty(opts.prox_param.C{1,2}.method))
    v = [v;C{1,2}(:);C{1,3}(:);C{2,3}(:);C{1,2,3}(:)];
%------------------------------------------------------------------------------------
else
    v = [v; x];
end

end