#include "armaMex.hpp"
#include "math.h"
#include <assert.h>
#ifdef OMPINCL
#include <omp.h>
#endif


struct linsys {
	mat data;
	mat Y_C;
};

struct singlesys {
	mat myvec;
	double val;
};

singlesys inner_lsq(double CPP_IND_MODE, mat x1, mat x2, mat x3, double b, mat w1, mat w2,mat w3,
	double Y, mat F1,mat F2,mat F3){

	// val computation
    mat val;
    val << b << endr;
    if (CPP_IND_MODE == -1)
    	val = val + w1.t()*x1 + w2.t()*x2 + w3.t()*x3;
    else if (CPP_IND_MODE == 1)
    	val = val + w2.t()*x2 + w3.t()*x3;
    else if (CPP_IND_MODE == 2)
    	val = val + w1.t()*x1 + w3.t()*x3;
    else //CPP_IND_MODE = 3
    	val = val + w1.t()*x1 + w2.t()*x2;
    val(0, 0) =  Y - val(0, 0);

    // vec computation
    mat xF1, xF2, xF3;
    if (CPP_IND_MODE != 1)
    	xF1 = x1.t()*F1;
    else
    	xF1 = x1.t();

    if (CPP_IND_MODE != 2)
		xF2 = x2.t()*F2;
    else
    	xF2 = x2.t();

    if (CPP_IND_MODE != 3)
		xF3 = x3.t()*F3;
    else
    	xF3 = x3.t();

	mat dC12 = xF1.t()*xF2;
	mat dC13 = xF1.t()*xF3;
	mat dC23 = xF2.t()*xF3;

	cube dC123 = zeros(xF1.n_cols,xF2.n_cols,xF3.n_cols);
	for(int i3 = 0;i3<xF3.n_cols;i3++)
	{
		dC123.slice(i3) = xF1.t() * xF2 * xF3(i3);
	}

    mat v5 = join_vert( vectorise(dC12), vectorise(dC13) );
    mat v6 = join_vert( v5, vectorise(dC23) );
    mat v7 = join_vert( v6, vectorise(dC123) );

    singlesys mysinglesys;
    mysinglesys.val = val(0, 0);
    mysinglesys.myvec = v7.t();

    return mysinglesys;

}

linsys opt_C_close_form_sub(double CPP_IND_MODE,mat X1, mat X2, mat X3, double b,mat w1, mat w2, mat w3,mat F1,mat F2,mat F3,cube Y, int numel_loss, int numel_C)
{	
	int cont = 0;
	linsys mylinsys;
	mylinsys.data.zeros(numel_loss,numel_C);
	mylinsys.Y_C.zeros(numel_loss,1);

	for(int i = 0;i<X1.n_cols;i++)
	{
		mat x1 = X1.col(i);    
		for(int j = 0;j<X2.n_cols;j++)
		{
			mat x2 = X2.col(j); 
			for(int k = 0;k<X3.n_cols;k++)
			{
				mat x3 = X3.col(k);

				singlesys temp = inner_lsq(CPP_IND_MODE, x1,x2, x3, b,w1, w2, w3, Y(i,j,k),F1, F2,F3);
				mylinsys.Y_C(cont) = temp.val;
				mylinsys.data.row(cont) = temp.myvec;

				cont++;
			}
		}
	} 

	return mylinsys;

}

linsys opt_C_close_form_sub(double CPP_IND_MODE,mat X1, mat X2, mat X3, double b,mat w1, mat w2, mat w3,mat F1,mat F2,mat F3, mat idx, vec Y, int numel_loss, int numel_C)
{
	mat local_data = zeros(numel_loss,numel_C);
	mat local_Y_C = zeros(numel_loss,1);

	#pragma omp parallel for
	for(int i = 0;i<idx.n_rows;i++){

	    mat x1 = X1.col(idx(i, 0)-1); //CAUTION: C++ starts indexing from Zero, in contrast to Matlab
	    mat x2 = X2.col(idx(i, 1)-1);
	    mat x3 = X3.col(idx(i, 2)-1);

	    singlesys temp = inner_lsq(CPP_IND_MODE, x1,x2, x3,b,w1, w2, w3, Y(i),F1, F2,F3);
		local_data.row(i) = temp.myvec;
		local_Y_C(i) = temp.val;

	}

	linsys mylinsys;
	mylinsys.data = local_data;
	mylinsys.Y_C = local_Y_C;
	return mylinsys;
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	int argctr = 0;
	double CPP_IND_MODE = armaGetDouble(prhs[argctr++]);
	assert(CPP_IND_MODE == -1 || (CPP_IND_MODE > 0 && CPP_IND_MODE < 4));
	
	mat X1 = armaGetPr(prhs[argctr++]);   
	mat X2 = armaGetPr(prhs[argctr++]); 
	mat X3 = armaGetPr(prhs[argctr++]); 
	double b = armaGetDouble(prhs[argctr++]);

	mat w1,w2,w3;
	if (CPP_IND_MODE==1)
		argctr++;
	else
		w1 = armaGetPr(prhs[argctr++]);
	if (CPP_IND_MODE==2)
		argctr++;
	else
		w2 = armaGetPr(prhs[argctr++]);
	if (CPP_IND_MODE==3)
		argctr++;
	else
		w3 = armaGetPr(prhs[argctr++]);

	mat F1, F2, F3;
	if (CPP_IND_MODE==1)
		argctr++;
	else
		F1 = armaGetPr(prhs[argctr++]); 
	if (CPP_IND_MODE==2)
		argctr++;
	else
		F2 = armaGetPr(prhs[argctr++]); 
	if (CPP_IND_MODE==3)
		argctr++;
	else
		F3 = armaGetPr(prhs[argctr++]); 

    int numel_loss = armaGetDouble(prhs[argctr++]);
	int numel_C = armaGetDouble(prhs[argctr++]);

	double sparseY = armaGetDouble(prhs[argctr++]); 

	linsys returnsys;
	if (sparseY == 0){ 
		cube Y = armaGetCubePr(prhs[argctr++]); 
		returnsys = opt_C_close_form_sub(CPP_IND_MODE,X1,X2,X3,b,w1,w2,w3,F1,F2,F3, Y,numel_loss, numel_C);
	} 
	else{
		vec Y = armaGetPr(prhs[argctr++]); 
		mat idx = armaGetPr(prhs[argctr++]); 
		returnsys = opt_C_close_form_sub(CPP_IND_MODE,X1,X2,X3,b,w1,w2,w3,F1,F2,F3,idx, Y,numel_loss, numel_C); 
	} 

	mat mat_out1 = returnsys.data;	
	mat mat_out2 = returnsys.Y_C;
	plhs[0] = armaCreateMxMatrix(mat_out1.n_rows, mat_out1.n_cols); 
	armaSetPr(plhs[0], mat_out1);


	plhs[1] = armaCreateMxMatrix(mat_out2.n_rows, mat_out2.n_cols); 
	armaSetPr(plhs[1], mat_out2);

	return; 
}
