clear all; close all;
ROOTPATH = '/Users/joachim/Dropbox/GeorgiaTech/';
FM_OUT_DIR = 'output/';
FOLDS = 5;

EHR_DATA = 0;
COLD_START = 1;

if (EHR_DATA)
    path = strcat(ROOTPATH, 'health_data/mimic3_preprocessed/out/');
    path_for_FM = strcat(ROOTPATH, 'papers/polyadic_regression/exps_3way/competitors/FM/EHR');
    load(strcat(path,'tensor_data_EHR.mat'));
else
    path = strcat(ROOTPATH, 'health_data/LINCS_drug_ind_gene_exp/');
    if (COLD_START)
        path_for_FM = strcat(ROOTPATH, 'papers/polyadic_regression/exps_3way/competitors/FM/GENEXPR_COLDSTART/');
        load(strcat(path,'tensor_data_coldstart_GENEXPR.mat'));
    else
        path_for_FM = strcat(ROOTPATH, 'papers/polyadic_regression/exps_3way/competitors/FM/GENEXPR/');
        load(strcat(path,'tensor_data_GENEXPR.mat'));
    end
end

rank = 2;
stdrange = 0:3;
stds = [0.0001 0.001 0.01 0.1];


for j=1:length(stdrange)
    FM_PRED = load(strcat(path_for_FM, strcat(FM_OUT_DIR, num2str(rank), sprintf('/output_%d_%d.txt',1,stdrange(j)))));
    % de-center responses based on the mean of the y_i's of the training
    load(strcat(path_for_FM, sprintf('meanY_%d.mat', 1)));
    FM_PRED = meanY + FM_PRED;
    
    FM_ERR(j) = eval_fun(FM_PRED, validationset(:,end))
    FM_SPEAR(j) = corr(FM_PRED, validationset(:,end), 'type', 'Spearman')
    
end
figure;
stem(stds, FM_SPEAR)