function ret = num_params_polreg(d, m)
disp('Assuming partial induction on mode 3!')
ret = 1+sum(d)+sum(d.*m)+m(1)*m(2)+m(1)*m(3)+m(2)*m(3)+prod(m);
ret = ret - d(3) - d(3)*m(3);