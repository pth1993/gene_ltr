

def get_pearson_score(input_file):
    score = dict()
    with open(input_file) as f:
        for line in f:
            line = line.strip().split('\t')
            sc = float(line[1])
            score[line[0]] = float(sc)
    return score


def generate_data(input_file, exp_score):
    with open(input_file, 'r') as f, open('data/signature__0_5.csv', 'w') as \
            f1, open('data/signature__0_1.csv', 'w') as f2, open('data/signature_0_1.csv', 'w') as \
            f3, open('data/signature_0_3.csv', 'w') as f4, open('data/signature_0_5.csv', 'w') as f5:
        header = f.readline()  # skip header
        f1.write(header)
        f2.write(header)
        f3.write(header)
        f4.write(header)
        f5.write(header)
        for line in f:
            data = line.strip().split(',')
            assert len(data) == 983, "Wrong format"
            sc = exp_score[data[0]]
            if sc > 0.5:
                f5.write(line)
            if sc > 0.3:
                f4.write(line)
            if sc > 0.1:
                f3.write(line)
            if sc > -0.1:
                f2.write(line)
            if sc > -0.5:
                f1.write(line)


if __name__ == '__main__':
    exp_score = get_pearson_score('data/pearson_score.txt')
    generate_data('data/signature_all.csv', exp_score)