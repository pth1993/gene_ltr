import os
os.environ["CUDA_VISIBLE_DEVICES"] = "0"
import sys
from datetime import datetime
import torch
import numpy as np
import argparse
sys.path.append(os.path.dirname(os.path.realpath(__file__)).replace('/cv_script', '') + '/models')
sys.path.append(os.path.dirname(os.path.realpath(__file__)).replace('/cv_script', '') + '/utils')
import base_model_regression
import datareader
import metric

start_time = datetime.now()

parser = argparse.ArgumentParser(description='Base Model Regression')
parser.add_argument('--drug_file')
parser.add_argument('--drug_target_file', default=None)
parser.add_argument('--fp_type')
parser.add_argument('--data_file')
parser.add_argument('--fold')


args = parser.parse_args()

drug_file = args.drug_file
drug_target_file = args.drug_target_file
fp_type = args.fp_type
gene_expression_file = args.data_file
fold = int(args.fold)

# parameters initialization
drug_embed_dim = 128
drug_target_embed_dim = 128
gene_embed_dim = 128
pert_type_emb_dim = 4
cell_id_emb_dim = 128
pert_idose_emb_dim = 4
hid_dim = 128
dropout = 0.5
batch_size = 16
num_gene = 978
stop_threshold = 50
num_epoch = 50
precision_degree = [10, 20, 50, 100]
loss_type = 'point_wise_mse'
intitializer = torch.nn.init.xavier_uniform_
gene_file = 'data/gene_vector.csv'
pert_id_file = 'data/drug_cv.json'
filter = {"time": "24H", "pert_id": ['BRD-U41416256', 'BRD-U60236422'], "pert_type": ["trt_cp"],
          "cell_id": ['A375', 'HA1E', 'HELA', 'HT29', 'MCF7', 'PC3', 'YAPC'],
          "pert_idose": ["0.04 um", "0.12 um", "0.37 um", "1.11 um", "3.33 um", "10.0 um"]}

# check cuda
if torch.cuda.is_available():
    device = torch.device("cuda")
else:
    device = torch.device("cpu")
print("Use GPU: %s" % torch.cuda.is_available())

data = datareader.DataReaderCV(drug_file, pert_id_file, gene_file, gene_expression_file, filter, fp_type, device, fold)
print('#Train: %d' % len(data.train_feature['drug']))
print('#Test: %d' % len(data.test_feature['drug']))

# model creation
model = base_model_regression.BaseModelRegression(drug_input_dim=data.drug_dim, drug_emb_dim=drug_embed_dim,
                                                  gene_input_dim=np.shape(data.gene)[1], gene_emb_dim=gene_embed_dim,
                                                  num_gene=np.shape(data.gene)[0], hid_dim=hid_dim,
                                                  dropout=dropout, loss_type=loss_type, device=device,
                                                  initializer=intitializer,
                                                  pert_type_input_dim=len(filter['pert_type']),
                                                  cell_id_input_dim=len(filter['cell_id']),
                                                  pert_idose_input_dim=len(filter['pert_idose']),
                                                  pert_type_emb_dim=pert_type_emb_dim,
                                                  cell_id_emb_dim=cell_id_emb_dim,
                                                  pert_idose_emb_dim=pert_idose_emb_dim,
                                                  use_pert_type=data.use_pert_type, use_cell_id=data.use_cell_id,
                                                  use_pert_idose=data.use_pert_idose,
                                                  drug_target_input_dim=data.drug_target_dim,
                                                  drug_target_emb_dim=drug_target_embed_dim)
model.to(device)
model = model.double()

# training
optimizer = torch.optim.Adam(model.parameters())
pearson_list_test = []
spearman_list_test = []
rmse_list_test = []
precisionk_list_test = []
for epoch in range(num_epoch):
    print("Iteration %d:" % (epoch+1))
    model.train()
    epoch_loss = 0
    for i, batch in enumerate(data.get_batch_data(dataset='train', batch_size=batch_size, shuffle=True)):
        ft, lb = batch
        drug = ft['drug']
        if data.drug_target is not None:
            drug_target = ft['drug_target']
        else:
            drug_target = None
        if data.use_pert_type:
            pert_type = ft['pert_type']
        else:
            pert_type = None
        if data.use_cell_id:
            cell_id = ft['cell_id']
        else:
            cell_id = None
        if data.use_pert_idose:
            pert_idose = ft['pert_idose']
        else:
            pert_idose = None
        optimizer.zero_grad()
        predict = model(drug, data.gene, drug_target, pert_type, cell_id, pert_idose)
        loss = model.loss(lb, predict)
        loss.backward()
        optimizer.step()
        epoch_loss += loss.item()
    print('Train loss:')
    print(epoch_loss/(i+1))

    model.eval()

    epoch_loss = 0
    lb_np = np.empty([0, num_gene])
    predict_np = np.empty([0, num_gene])
    with torch.no_grad():
        for i, batch in enumerate(data.get_batch_data(dataset='test', batch_size=batch_size, shuffle=False)):
            ft, lb = batch
            drug = ft['drug']
            if data.drug_target is not None:
                drug_target = ft['drug_target']
            else:
                drug_target = None
            if data.use_pert_type:
                pert_type = ft['pert_type']
            else:
                pert_type = None
            if data.use_cell_id:
                cell_id = ft['cell_id']
            else:
                cell_id = None
            if data.use_pert_idose:
                pert_idose = ft['pert_idose']
            else:
                pert_idose = None
            predict = model(drug, data.gene, drug_target, pert_type, cell_id, pert_idose)
            loss = model.loss(lb, predict)
            epoch_loss += loss.item()
            lb_np = np.concatenate((lb_np, lb.cpu().numpy()), axis=0)
            predict_np = np.concatenate((predict_np, predict.cpu().numpy()), axis=0)
        print('Test loss:')
        print(epoch_loss / (i + 1))
        rmse = metric.rmse(lb_np, predict_np)
        rmse_list_test.append(rmse)
        print('RMSE: %.4f' % rmse)
        pearson, pearson_raw = metric.correlation(lb_np, predict_np, 'pearson')
        pearson_list_test.append(pearson)
        print('Pearson\'s correlation: %.4f' % pearson)
        spearman, _ = metric.correlation(lb_np, predict_np, 'spearman')
        spearman_list_test.append(spearman)
        print('Spearman\'s correlation: %.4f' % spearman)
        precision = []
        for k in precision_degree:
            precision_neg, precision_pos = metric.precision_k(lb_np, predict_np, k)
            print("Precision@%d Positive: %.4f" % (k, precision_pos))
            print("Precision@%d Negative: %.4f" % (k, precision_neg))
            precision.append([precision_pos, precision_neg])
        precisionk_list_test.append(precision)

best_test_epoch = np.argmax(pearson_list_test)
print("Epoch %d got best Pearson's correlation on test set: %.4f" % (best_test_epoch + 1, pearson_list_test[best_test_epoch]))
print("Epoch %d got Spearman's correlation on test set: %.4f" % (best_test_epoch + 1, spearman_list_test[best_test_epoch]))
print("Epoch %d got RMSE on test set: %.4f" % (best_test_epoch + 1, rmse_list_test[best_test_epoch]))
print("Epoch %d got P@100 POS and NEG on test set: %.4f, %.4f" % (best_test_epoch + 1,
                                                                  precisionk_list_test[best_test_epoch][-1][0],
                                                                  precisionk_list_test[best_test_epoch][-1][1]))
end_time = datetime.now()
print(end_time - start_time)
