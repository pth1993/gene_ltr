import numpy as np
from scipy.spatial.distance import cdist
import seaborn as sns; sns.set()
import matplotlib.pyplot as plt
import pandas as pd


def get_score_main(input_file):
    with open(input_file) as f:
        data = []
        for line in f:
            data.append(line)
        score = data[-2]
        score = [float(i) for i in score.strip().split(',')]
    return score


def get_score_knn(input_file):
    with open(input_file) as f:
        data = []
        for line in f:
            data.append(line)
        score = data[-8]
        score = [float(i) for i in score.strip().split(',')]
    return score


def read_drug(input_file, num_feature):
    drug = []
    drug_vec = []
    with open(input_file, 'r') as f:
        for line in f:
            line = line.strip().split(',')
            assert len(line) == num_feature + 1, "Wrong format"
            bin_vec = [float(i) for i in line[1:]]
            drug.append(line[0])
            drug_vec.append(bin_vec)
    drug_vec = np.asarray(drug_vec, dtype=np.float64)
    index = []
    for i in range(np.shape(drug_vec)[1]):
        if len(set(drug_vec[:, i])) > 1:
            index.append(i)
    drug_vec = drug_vec[:, index]
    drug = dict(zip(drug, drug_vec))
    return drug, len(index)


def choose_mean_example(examples):
    num_example = len(examples)
    mean_value = (num_example - 1) / 2
    indexes = np.argsort(examples, axis=0)
    indexes = np.argsort(indexes, axis=0)
    indexes = np.mean(indexes, axis=1)
    distance = (indexes - mean_value)**2
    index = np.argmin(distance)
    return examples[index]


def split_data_by_pert_id(input_file):
    with open(input_file) as f:
        test_pert_id = f.readline().strip().split(',')[1:]
        dev_pert_id = f.readline().strip().split(',')[1:]
        return dev_pert_id, test_pert_id


def read_data(input_file_train, input_file_dev, input_file_test, filter, drug, dev_pert_id, test_pert_id):
    data_train = dict()
    data_dev = dict()
    data_test = dict()
    pert_id = []
    filter_pert_id = dev_pert_id + test_pert_id + filter["pert_id"]
    with open(input_file_train, 'r') as f:
        f.readline()
        for line in f:
            line = line.strip().split(',')
            assert len(line) == 983, "Wrong format"
            if filter["time"] in line[0] and line[1] not in filter_pert_id and line[2] in filter["pert_type"] \
                    and line[3] in filter["cell_id"] and line[4] in filter["pert_idose"]:
                pert_id.append(line[1])
                ft = ','.join(line[1:5])
                lb = [float(i) for i in line[5:]]
                try:
                    data_train[ft].append(lb)
                except KeyError:
                    data_train[ft] = [lb]
    train_pert_id = sorted(list(set(pert_id)))

    pert_id = []
    with open(input_file_dev, 'r') as f:
        f.readline()
        for line in f:
            line = line.strip().split(',')
            assert len(line) == 983, "Wrong format"
            if filter["time"] in line[0] and line[1] in dev_pert_id and line[2] in filter["pert_type"] \
                    and line[3] in filter["cell_id"] and line[4] in filter["pert_idose"]:
                pert_id.append(line[1])
                ft = ','.join(line[1:5])
                lb = [float(i) for i in line[5:]]
                try:
                    data_dev[ft].append(lb)
                except KeyError:
                    data_dev[ft] = [lb]
    dev_pert_id = sorted(list(set(pert_id)))

    pert_id = []
    with open(input_file_test, 'r') as f:
        f.readline()
        for line in f:
            line = line.strip().split(',')
            assert len(line) == 983, "Wrong format"
            if filter["time"] in line[0] and line[1] in test_pert_id and line[2] in filter["pert_type"] \
                    and line[3] in filter["cell_id"] and line[4] in filter["pert_idose"]:
                pert_id.append(line[1])
                ft = ','.join(line[1:5])
                lb = [float(i) for i in line[5:]]
                try:
                    data_test[ft].append(lb)
                except KeyError:
                    data_test[ft] = [lb]
    test_pert_id = sorted(list(set(pert_id)))

    drug_ft_train = []
    drug_ft_dev = []
    drug_ft_test = []
    exp_train = []
    exp_dev = []
    exp_test = []
    for id in train_pert_id:
        drug_ft_train.append(drug[id])
    drug_ft_train = np.asarray(drug_ft_train, dtype=np.float64)
    for id in dev_pert_id:
        drug_ft_dev.append(drug[id])
    drug_ft_dev = np.asarray(drug_ft_dev, dtype=np.float64)
    for id in test_pert_id:
        drug_ft_test.append(drug[id])
    drug_ft_test = np.asarray(drug_ft_test, dtype=np.float64)

    for key, value in sorted(data_train.items()):
        exp_train.append(key)
    for key, value in sorted(data_dev.items()):
        exp_dev.append(key)
    for key, value in sorted(data_test.items()):
        exp_test.append(key)

    train_pert_dict = dict(zip(list(range(len(train_pert_id))), train_pert_id))
    dev_pert_dict = dict(zip(dev_pert_id, list(range(len(dev_pert_id)))))
    test_pert_dict = dict(zip(test_pert_id, list(range(len(test_pert_id)))))
    return drug_ft_train, drug_ft_dev, drug_ft_test, train_pert_dict, dev_pert_dict, test_pert_dict, exp_train, \
           exp_dev, exp_test


def calculate_distance(input_a, input_b, distance):
    output = cdist(input_a, input_b, distance)
    return output


def get_neighbor(distance):
    return np.argsort(distance)


def kNN(exp_train, exp_test, train_pert_dict, test_pert_dict, neighbor, k_list, distance):
    distance_list = []
    exp_train = [i[:-8] for i in exp_train]
    for ft in exp_test:
        ft = ft.split(',')
        drug_idx = test_pert_dict[ft[0]]
        nb_list = neighbor[drug_idx]
        cnt = 0
        for nb_idx in nb_list:
            nb = train_pert_dict[nb_idx]
            ft_train = ','.join([nb] + ft[1:3])
            if ft_train in exp_train:
                cnt += 1
                if cnt in k_list:
                    distance_list.append(distance[drug_idx, nb_idx])
                    break
    return distance_list


def create_graph(score_nfp, score_base, score_knn, distance_list):
    distance_total = []
    score_total = []
    for k, score in enumerate([score_nfp, score_base, score_knn]):
        for j, distance in enumerate(distance_list):
            output = dict()
            for s, d in zip(score, distance):
                if d not in output:
                    output[d] = [s]
                else:
                    output[d].append(s)
            distance = [np.percentile(distance, 25), np.percentile(distance, 50),
                        np.percentile(distance, 75), np.percentile(distance, 100)]
            # distance = [np.percentile(distance, 50), np.percentile(distance, 100)]
            print(distance)
            score1 = [[] for i in range(len(distance))]
            for d in output:
                for i, d1 in enumerate(distance):
                    if d1 >= d:
                        score1[i] += output[d]
                        break
            freq = [len(i) for i in score1]
            distance_new = []
            if k == 0 or k == 1:
                for i, d in enumerate(['d < Q1', 'Q1 < d < Q2', 'Q2 < d < Q3', 'Q3 < d']):
                    distance_new += [d] * freq[i]
            else:
                for i, d in enumerate(['Q1 < d < Q2', 'd < Q1', 'Q3 < d', 'Q2 < d < Q3']):
                    distance_new += [d] * freq[i]
            # if k == 0:
            #     for i, d in enumerate(['d < Q1', 'Q1 < d < Q2', 'Q2 < d < Q3', 'Q3 < d']):
            #         distance_new += [d] * freq[i]
            # else:
            #     for i, d in enumerate(['d < Q1', 'Q2 < d < Q3', 'Q1 < d < Q2', 'Q3 < d']):
            #         distance_new += [d] * freq[i]
            score1 = [item for sublist in score1 for item in sublist]
            distance_total += distance_new
            score_total += score1
    data = {'Distance (d)': distance_total, 'score': score_total, 'model':
        np.repeat(['DeepCE', 'Vanilla Neural Network', 'kNN'], 496), 'Fingerprint': np.repeat(['PubChem'], 496 * 3)}
    df = pd.DataFrame(data)
    sns.set(font_scale=1)
    sns.set_style('whitegrid')
    fig, ax = plt.subplots(figsize=(7, 4))
    sns.barplot("model", y="score", hue="Distance (d)", data=df)
    sns.despine(left=True)
    ax.set_ylabel("Pearson correlation")
    ax.set_xlabel("Model")
    plt.show()
    fig.savefig('fig/fig_3_drug_distance.pdf')


def get_all_drug(input_file, drug_fp):
    fp = []
    with open(input_file, 'r') as f:
        drug = f.readline().strip().split(',')
    for d in drug:
        fp.append(drug_fp[d])
    return drug, np.array(fp)


if __name__ == '__main__':
    gene_expression_file_train = 'data/signature_0_7.csv'
    filter = {"time": "24H", "pert_id": ['BRD-U41416256', 'BRD-U60236422'], "pert_type": ["trt_cp"],
              "cell_id": ["A375", "HT29", "MCF7", "PC3", "HA1E", "YAPC", "HELA"],
              "pert_idose": ["0.04 um", "0.12 um", "0.37 um", "1.11 um", "3.33 um", "10.0 um"]}
    # dist_list = ['cosine', 'euclidean', 'correlation', 'rogerstanimoto', 'jaccard']
    dist_list = ['jaccard']
    score_nfp = get_score_main('output/183907_nfp_drug_gene_attention_model_regression_0_1.txt')
    score_base = get_score_main('output/775083_base_model_regression_pubchem.txt')
    score_knn = get_score_knn('output/knn_out.txt')
    # score_knn = None
    drug_pubchem = read_drug('data/drugs_pubchem.csv', 881)[0]
    drug_circular = read_drug('data/drugs_circular.csv', 1024)[0]
    drug_drug_target = read_drug('data/drugs_target_diy.csv', 978)[0]
    drug_ltip = read_drug('data/drugs_target_ltip.csv', 978)[0]

    # drug, fp = get_all_drug('data/pert_id_0_7.txt', drug_pubchem)
    # distance = calculate_distance(fp, fp, dist_list[0])

    k_list = [9]
    dev_pert_id, test_pert_id = split_data_by_pert_id('data/pert_id_eval.txt')
    distance_list = []
    for i, drug in enumerate([drug_pubchem]):
        if i == 0:
            print("Drug feature: pubchem")
        elif i == 1:
            print("Drug feature: circular")
        for dist in dist_list:
            print(dist)
            drug_ft_train, drug_ft_dev, drug_ft_test, train_pert_dict, dev_pert_dict, \
            test_pert_dict, exp_train, exp_dev, exp_test = \
                read_data(gene_expression_file_train, 'data/signature_dev.csv', 'data/signature_test.csv',
                          filter, drug, dev_pert_id, test_pert_id)
            distance = calculate_distance(drug_ft_test, drug_ft_train, dist)
            neighbor = get_neighbor(distance)
            distance = kNN(exp_train, exp_test, train_pert_dict, test_pert_dict, neighbor, k_list, distance)
            distance_list.append(distance)
    create_graph(score_nfp, score_base, score_knn, distance_list)



