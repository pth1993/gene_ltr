import numpy as np


def extract_result(text):
    pearson = float(text[0].split()[9])
    spearman = float(text[1].split()[8])
    rmse = float(text[2].split()[7])
    p_pos, p_neg = [float(i.replace(',', '')) for i in text[3].split()[10:]]
    return pearson, spearman, rmse, p_pos, p_neg


def calculate_average(i0, i1, i2, i3, i4):
    i_ave = np.mean([i0, i1, i2, i3, i4], axis=0)
    return np.around(i_ave, decimals=4)


def convert_2_string(input):
    return [str(i) for i in input]
    

fold_list = ['fold_0/', 'fold_1/', 'fold_2/', 'fold_3/', 'fold_4/']
eid = '314397'
# file_name_list = ['base_model_regression_pubchem', 'base_model_regression_circular', 'base_model_regression_random',
#                   'base_model_regression_drug_target', 'base_model_regression_ltip',
#                   'base_model_regression_pubchem_drug_target', 'base_model_regression_pubchem_ltip',
#                   'base_model_regression_circular_drug_target', 'base_model_regression_circular_ltip',
#                   'nfp_model_regression', 'nfp_model_regression_drug_target', 'nfp_model_regression_ltip',
#                   'linear_regression_none_pubchem', 'linear_regression_none_circular',
#                   'linear_regression_none_drug_target', 'linear_regression_none_ltip', 'linear_regression_l1_pubchem',
#                   'linear_regression_l1_circular', 'linear_regression_l1_drug_target', 'linear_regression_l1_ltip',
#                   'linear_regression_l2_pubchem', 'linear_regression_l2_circular', 'linear_regression_l2_drug_target',
#                   'linear_regression_l2_ltip']
file_name_list =['nfp_model_regression', 'nfp_model_regression_drug_target', 'nfp_model_regression_ltip']
output_dev = {'fold_0/': [], 'fold_1/': [], 'fold_2/': [], 'fold_3/': [], 'fold_4/': [], 'fold_ave/': []}
output_test = {'fold_0/': [], 'fold_1/': [], 'fold_2/': [], 'fold_3/': [], 'fold_4/': [], 'fold_ave/': []}
for fold in fold_list:
    for file_name in file_name_list:
        with open(fold + eid + '_' + file_name + '.txt') as f:
            text = []
            for line in f:
                text.append(line.strip())
            dev = text[-9:-5]
            test = text[-5:-1]
            pearson, spearman, rmse, p_pos, p_neg = extract_result(dev)
            output_dev[fold].append([pearson, spearman, rmse, p_pos, p_neg])
            pearson, spearman, rmse, p_pos, p_neg = extract_result(test)
            output_test[fold].append([pearson, spearman, rmse, p_pos, p_neg])
with open('dev_result.csv', 'w') as f:
    for file_name, (i0, i1, i2, i3, i4) in zip(file_name_list, zip(output_dev['fold_0/'], output_dev['fold_1/'], output_dev['fold_2/'],
                                  output_dev['fold_3/'], output_dev['fold_4/'])):
        i_ave = calculate_average(i0, i1, i2, i3, i4)
        output_dev['fold_ave/'].append(i_ave)
        f.write(','.join([eid, '0', file_name] + convert_2_string(i0)) + '\n')
        f.write(','.join([eid, '1', file_name] + convert_2_string(i1)) + '\n')
        f.write(','.join([eid, '2', file_name] + convert_2_string(i2)) + '\n')
        f.write(','.join([eid, '3', file_name] + convert_2_string(i3)) + '\n')
        f.write(','.join([eid, '4', file_name] + convert_2_string(i4)) + '\n')
        f.write(','.join([eid, 'ave.', file_name] + convert_2_string(i_ave)) + '\n')
with open('test_result.csv', 'w') as f:
    for file_name, (i0, i1, i2, i3, i4) in zip(file_name_list, zip(output_test['fold_0/'], output_test['fold_1/'], output_test['fold_2/'],
                                  output_test['fold_3/'], output_test['fold_4/'])):
        i_ave = calculate_average(i0, i1, i2, i3, i4)
        output_test['fold_ave/'].append(i_ave)
        f.write(','.join([eid, '0', file_name] + convert_2_string(i0)) + '\n')
        f.write(','.join([eid, '1', file_name] + convert_2_string(i1)) + '\n')
        f.write(','.join([eid, '2', file_name] + convert_2_string(i2)) + '\n')
        f.write(','.join([eid, '3', file_name] + convert_2_string(i3)) + '\n')
        f.write(','.join([eid, '4', file_name] + convert_2_string(i4)) + '\n')
        f.write(','.join([eid, 'ave.', file_name] + convert_2_string(i_ave)) + '\n')






