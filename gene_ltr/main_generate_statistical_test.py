from scipy.stats import ttest_rel
import numpy as np


def get_score_main(input_file):
    with open(input_file) as f:
        data = []
        for line in f:
            data.append(line)
        score = data[-2]
        score = [float(i) for i in score.strip().split(',')]
    return score


def get_score_down_stream(input_file):
    data = []
    with open(input_file, 'r') as f:
        for line in f:
            line = line.strip().split(',')
            data.append([float(i) for i in line])
    data = np.array(data)
    score1 = data[:7]
    # score1 = np.reshape(np.mean(np.reshape(score1, (7, 10, 5, 3)), axis=2), 210)
    score1 = np.mean(np.reshape(score1, (7, 14, 5, 4)), axis=2)
    score2 = data[7:]
    # score2 = np.reshape(np.mean(np.reshape(score2, (7, 10, 5, 3)), axis=2), 210)
    score2 = np.mean(np.reshape(score2, (7, 14, 5, 4)), axis=2)
    return score1, score2


def get_score_down_stream_specifice(score1, score2):
    print('Per cell line:')
    print('Original')
    print(np.mean(score1, (1, 2)))
    print('Predicted')
    print(np.mean(score2, (1, 2)))
    print('Per label:')
    print('Original')
    print(np.mean(score1, (0, 2)))
    print('Predicted')
    print(np.mean(score2, (0, 2)))
    print('Per model:')
    print('Original')
    print(np.mean(score1, (0, 1)))
    print('Predicted')
    print(np.mean(score2, (0, 1)))


if __name__ == '__main__':
    # file1 = 'output/592249_nfp_drug_gene_attention_model_regression_0_1.txt'
    # file2 = 'output/789729_nfp_drug_gene_attention_model_regression_0_1.txt'
    # score1 = get_score_main(file1)
    # score2 = get_score_main(file2)
    # p_value = ttest_rel(score1, score2)[1]
    # print(p_value)
    input_file = 'output/down_stream_output_new_1.csv'
    score1, score2 = get_score_down_stream(input_file)
    shape = np.shape(score1)
    print(shape)
    for i in range(shape[0]):
        tmp1 = np.reshape(score1[i], -1)
        tmp2 = np.reshape(score2[i], -1)
        p_value = ttest_rel(tmp1, tmp2)[1]
        print(p_value)
    for i in range(shape[1]):
        tmp1 = np.reshape(score1[:, i], -1)
        tmp2 = np.reshape(score2[:, i], -1)
        p_value = ttest_rel(tmp1, tmp2)[1]
        if i == 11:
            print(tmp1)
            print(tmp2)
        print(p_value)
    for i in range(shape[2]):
        tmp1 = np.reshape(score1[:, :, i], -1)
        tmp2 = np.reshape(score2[:, :, i], -1)
        p_value = ttest_rel(tmp1, tmp2)[1]
        print(p_value)
    # get_score_down_stream_specifice(score1, score2)
