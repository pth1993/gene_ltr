import numpy as np
import random
import torch
import data_utils
import json

seed = 123126
# seed = 343
# seed = 4
# seed = 5791
np.random.seed(seed=seed)
random.seed(a=seed)
torch.manual_seed(seed)


class DataReader(object):
    def __init__(self, drug_file, pert_id_file, cell_id_file, gene_file, data_file_train, data_file_dev, data_file_test,
                 filter, fp_type, id_type, device, drug_target_file=None):
        assert id_type == 'drug', 'Wrong id type'
        self.device = device
        self.fp_type = fp_type
        if fp_type == 'pubchem':
            self.drug, self.drug_dim = data_utils.read_drug_number(drug_file, 881)
        elif fp_type == 'circular':
            self.drug, self.drug_dim = data_utils.read_drug_number(drug_file, 1024)
        elif fp_type == 'drug-target':
            self.drug, self.drug_dim = data_utils.read_drug_number(drug_file, 978)
        elif fp_type == 'neural':
            self.drug, self.drug_dim = data_utils.read_drug_string(drug_file)
        elif fp_type == 'random':
            self.drug, self.drug_dim = data_utils.read_drug_number(drug_file, 500)
        else:
            raise ValueError("Unknown fingerprint: %s" % fp_type)
        if drug_target_file is not None:
            self.drug_target, self.drug_target_dim = data_utils.read_drug_number(drug_target_file, 978)
        else:
            self.drug_target = None
            self.drug_target_dim = None
        self.gene = data_utils.read_gene(gene_file, self.device)
        dev_pert_id, test_pert_id = data_utils.get_id_test(pert_id_file)
        dev_cell_id, test_cell_id = data_utils.get_id_test(cell_id_file)
        self.cell_dim = len(filter['cell_id']) - len(dev_cell_id) - len(test_cell_id)
        # feature_train, label_train = data_utils.read_data_train(data_file_train, filter, dev_pert_id, test_pert_id)
        # feature_dev, label_dev = data_utils.read_data_test(data_file_dev, filter)
        # feature_test, label_test = data_utils.read_data_test(data_file_test, filter)
        feature_train, label_train = data_utils.read_data_train(data_file_train, filter, dev_pert_id, test_pert_id,
                                                                dev_cell_id, test_cell_id)
        feature_dev, label_dev = data_utils.read_data_test(data_file_dev, filter,  dev_pert_id, test_pert_id,
                                                           dev_cell_id, test_cell_id, id_type)
        feature_test, label_test = data_utils.read_data_test(data_file_test, filter, dev_pert_id, test_pert_id,
                                                             dev_cell_id, test_cell_id, id_type)
        self.train_feature, self.dev_feature, self.test_feature, self.train_label, \
        self.dev_label, self.test_label, self.use_pert_type, self.use_cell_id, self.use_pert_idose = \
            data_utils.transfrom_to_tensor(feature_train, label_train, feature_dev, label_dev,
                                           feature_test, label_test, self.drug, fp_type, self.device, self.drug_target)

    def get_batch_data(self, dataset, batch_size, shuffle):
        if dataset == 'train':
            feature = self.train_feature
            label = self.train_label
        elif dataset == 'dev':
            feature = self.dev_feature
            label = self.dev_label
        elif dataset == 'test':
            feature = self.test_feature
            label = self.test_label
        if shuffle:
            index = torch.randperm(len(feature['drug'])).long()
            if self.fp_type == 'neural':
                index = index.numpy()
            else:
                index = index.to(self.device)
        for start_idx in range(0, len(feature['drug']), batch_size):
            if shuffle:
                excerpt = index[start_idx:start_idx + batch_size]
            else:
                excerpt = slice(start_idx, start_idx + batch_size)
            output = dict()
            if self.fp_type == 'neural':
                output['drug'] = data_utils.convert_smile_to_feature(feature['drug'][excerpt], self.device)
                output['mask'] = data_utils.create_mask_feature(output['drug'], self.device)
            else:
                output['drug'] = feature['drug'][excerpt]
            if self.drug_target is not None:
                output['drug_target'] = feature['drug_target'][excerpt]
            if self.use_pert_type:
                output['pert_type'] = feature['pert_type'][excerpt]
            if self.use_cell_id:
                output['cell_id'] = feature['cell_id'][excerpt]
            if self.use_pert_idose:
                output['pert_idose'] = feature['pert_idose'][excerpt]
            yield output, label[excerpt]


class DataReader1(object):
    def __init__(self, drug_file, pert_id_file, cell_id_file, gene_file, cell_file, data_file_train, data_file_dev,
                 data_file_test, filter, fp_type, id_type, device, drug_target_file=None):
        self.device = device
        self.fp_type = fp_type
        if fp_type == 'pubchem':
            self.drug, self.drug_dim = data_utils.read_drug_number(drug_file, 881)
        elif fp_type == 'circular':
            self.drug, self.drug_dim = data_utils.read_drug_number(drug_file, 1024)
        elif fp_type == 'drug-target':
            self.drug, self.drug_dim = data_utils.read_drug_number(drug_file, 978)
        elif fp_type == 'neural':
            self.drug, self.drug_dim = data_utils.read_drug_string(drug_file)
        elif fp_type == 'random':
            self.drug, self.drug_dim = data_utils.read_drug_number(drug_file, 500)
        else:
            raise ValueError("Unknown fingerprint: %s" % fp_type)
        if drug_target_file is not None:
            self.drug_target, self.drug_target_dim = data_utils.read_drug_number(drug_target_file, 978)
        else:
            self.drug_target = None
            self.drug_target_dim = None
        self.gene = data_utils.read_gene(gene_file, self.device)
        self.cell, self.cell_dim = data_utils.read_cell(cell_file, 54271)
        dev_pert_id, test_pert_id = data_utils.get_id_test(pert_id_file)
        dev_cell_id, test_cell_id = data_utils.get_id_test(cell_id_file)
        feature_train, label_train = data_utils.read_data_train(data_file_train, filter, dev_pert_id, test_pert_id,
                                                                dev_cell_id, test_cell_id)
        feature_dev, label_dev = data_utils.read_data_test(data_file_dev, filter,  dev_pert_id, test_pert_id,
                                                             dev_cell_id, test_cell_id, id_type)
        feature_test, label_test = data_utils.read_data_test(data_file_test, filter, dev_pert_id, test_pert_id,
                                                             dev_cell_id, test_cell_id, id_type)
        self.train_feature, self.dev_feature, self.test_feature, self.train_label, \
        self.dev_label, self.test_label, self.use_pert_type, self.use_cell_id, self.use_pert_idose = \
            data_utils.transfrom_to_tensor_1(feature_train, label_train, feature_dev, label_dev, feature_test,
                                             label_test, self.drug, fp_type, self.device, self.drug_target, self.cell)

    def get_batch_data(self, dataset, batch_size, shuffle):
        if dataset == 'train':
            feature = self.train_feature
            label = self.train_label
        elif dataset == 'dev':
            feature = self.dev_feature
            label = self.dev_label
        elif dataset == 'test':
            feature = self.test_feature
            label = self.test_label
        if shuffle:
            index = torch.randperm(len(feature['drug'])).long()
            if self.fp_type == 'neural':
                index = index.numpy()
            else:
                index = index.to(self.device)
        for start_idx in range(0, len(feature['drug']), batch_size):
            if shuffle:
                excerpt = index[start_idx:start_idx + batch_size]
            else:
                excerpt = slice(start_idx, start_idx + batch_size)
            output = dict()
            if self.fp_type == 'neural':
                output['drug'] = data_utils.convert_smile_to_feature(feature['drug'][excerpt], self.device)
            else:
                output['drug'] = feature['drug'][excerpt]
            if self.drug_target is not None:
                output['drug_target'] = feature['drug_target'][excerpt]
            if self.use_pert_type:
                output['pert_type'] = feature['pert_type'][excerpt]
            if self.use_cell_id:
                output['cell_id'] = feature['cell_id'][excerpt]
            if self.use_pert_idose:
                output['pert_idose'] = feature['pert_idose'][excerpt]
            yield output, label[excerpt]


class DataReaderTradition(object):
    def __init__(self, drug_file, pert_id_file, cell_id_file, gene_file, data_file_train, data_file_dev, data_file_test,
                 filter, fp_type, id_type, device, drug_target_file=None):
        assert id_type == 'drug', 'Wrong id type'
        self.device = device
        self.fp_type = fp_type
        if fp_type == 'pubchem':
            self.drug, self.drug_dim = data_utils.read_drug_number(drug_file, 881)
        elif fp_type == 'circular':
            self.drug, self.drug_dim = data_utils.read_drug_number(drug_file, 1024)
        elif fp_type == 'drug-target':
            self.drug, self.drug_dim = data_utils.read_drug_number(drug_file, 978)
        elif fp_type == 'neural':
            self.drug, self.drug_dim = data_utils.read_drug_string(drug_file)
        elif fp_type == 'random':
            self.drug, self.drug_dim = data_utils.read_drug_number(drug_file, 500)
        else:
            raise ValueError("Unknown fingerprint: %s" % fp_type)
        if drug_target_file is not None:
            self.drug_target, self.drug_target_dim = data_utils.read_drug_number(drug_target_file, 978)
        else:
            self.drug_target = None
            self.drug_target_dim = None
        self.gene = data_utils.read_gene(gene_file, self.device)
        dev_cell_id, test_cell_id = data_utils.get_id_test(cell_id_file)
        self.cell_dim = len(filter['cell_id']) - len(dev_cell_id) - len(test_cell_id)
        # feature_train, label_train = data_utils.read_data_train(data_file_train, filter, dev_pert_id, test_pert_id)
        # feature_dev, label_dev = data_utils.read_data_test(data_file_dev, filter)
        # feature_test, label_test = data_utils.read_data_test(data_file_test, filter)
        feature_train, label_train, feature_test, label_test = data_utils.read_data_tradition(data_file_train, filter,
                                                                                              dev_cell_id, test_cell_id)
        self.train_feature, self.dev_feature, self.test_feature, self.train_label, \
        self.dev_label, self.test_label, self.use_pert_type, self.use_cell_id, self.use_pert_idose = \
            data_utils.transfrom_to_tensor(feature_train, label_train, feature_test, label_test,
                                           feature_test, label_test, self.drug, fp_type, self.device, self.drug_target)

    def get_batch_data(self, dataset, batch_size, shuffle):
        if dataset == 'train':
            feature = self.train_feature
            label = self.train_label
        elif dataset == 'dev':
            feature = self.dev_feature
            label = self.dev_label
        elif dataset == 'test':
            feature = self.test_feature
            label = self.test_label
        if shuffle:
            index = torch.randperm(len(feature['drug'])).long()
            if self.fp_type == 'neural':
                index = index.numpy()
            else:
                index = index.to(self.device)
        for start_idx in range(0, len(feature['drug']), batch_size):
            if shuffle:
                excerpt = index[start_idx:start_idx + batch_size]
            else:
                excerpt = slice(start_idx, start_idx + batch_size)
            output = dict()
            if self.fp_type == 'neural':
                output['drug'] = data_utils.convert_smile_to_feature(feature['drug'][excerpt], self.device)
                output['mask'] = data_utils.create_mask_feature(output['drug'], self.device)
            else:
                output['drug'] = feature['drug'][excerpt]
            if self.drug_target is not None:
                output['drug_target'] = feature['drug_target'][excerpt]
            if self.use_pert_type:
                output['pert_type'] = feature['pert_type'][excerpt]
            if self.use_cell_id:
                output['cell_id'] = feature['cell_id'][excerpt]
            if self.use_pert_idose:
                output['pert_idose'] = feature['pert_idose'][excerpt]
            yield output, label[excerpt]


class DataReaderTradition1(object):
    def __init__(self, drug_file, pert_id_file, cell_id_file, gene_file, data_file_train, data_file_dev, data_file_test,
                 filter, fp_type, id_type, device, drug_target_file=None):
        assert id_type == 'drug', 'Wrong id type'
        self.device = device
        self.fp_type = fp_type
        if fp_type == 'pubchem':
            self.drug, self.drug_dim = data_utils.read_drug_number(drug_file, 881)
        elif fp_type == 'circular':
            self.drug, self.drug_dim = data_utils.read_drug_number(drug_file, 1024)
        elif fp_type == 'drug-target':
            self.drug, self.drug_dim = data_utils.read_drug_number(drug_file, 978)
        elif fp_type == 'neural':
            self.drug, self.drug_dim = data_utils.read_drug_string(drug_file)
        elif fp_type == 'random':
            self.drug, self.drug_dim = data_utils.read_drug_number(drug_file, 500)
        else:
            raise ValueError("Unknown fingerprint: %s" % fp_type)
        if drug_target_file is not None:
            self.drug_target, self.drug_target_dim = data_utils.read_drug_number(drug_target_file, 978)
        else:
            self.drug_target = None
            self.drug_target_dim = None
        self.gene = data_utils.read_gene(gene_file, self.device)
        dev_cell_id, test_cell_id = data_utils.get_id_test(cell_id_file)
        self.cell_dim = len(filter['cell_id']) - len(dev_cell_id) - len(test_cell_id)
        # feature_train, label_train = data_utils.read_data_train(data_file_train, filter, dev_pert_id, test_pert_id)
        # feature_dev, label_dev = data_utils.read_data_test(data_file_dev, filter)
        # feature_test, label_test = data_utils.read_data_test(data_file_test, filter)
        feature_train, label_train, feature_dev, label_dev, feature_test, label_test = data_utils.read_data_tradition_1\
            (data_file_train, filter, dev_cell_id, test_cell_id)
        self.train_feature, self.dev_feature, self.test_feature, self.train_label, \
        self.dev_label, self.test_label, self.use_pert_type, self.use_cell_id, self.use_pert_idose = \
            data_utils.transfrom_to_tensor(feature_train, label_train, feature_dev, label_dev,
                                           feature_test, label_test, self.drug, fp_type, self.device, self.drug_target)

    def get_batch_data(self, dataset, batch_size, shuffle):
        if dataset == 'train':
            feature = self.train_feature
            label = self.train_label
        elif dataset == 'dev':
            feature = self.dev_feature
            label = self.dev_label
        elif dataset == 'test':
            feature = self.test_feature
            label = self.test_label
        if shuffle:
            index = torch.randperm(len(feature['drug'])).long()
            if self.fp_type == 'neural':
                index = index.numpy()
            else:
                index = index.to(self.device)
        for start_idx in range(0, len(feature['drug']), batch_size):
            if shuffle:
                excerpt = index[start_idx:start_idx + batch_size]
            else:
                excerpt = slice(start_idx, start_idx + batch_size)
            output = dict()
            if self.fp_type == 'neural':
                output['drug'] = data_utils.convert_smile_to_feature(feature['drug'][excerpt], self.device)
                output['mask'] = data_utils.create_mask_feature(output['drug'], self.device)
            else:
                output['drug'] = feature['drug'][excerpt]
            if self.drug_target is not None:
                output['drug_target'] = feature['drug_target'][excerpt]
            if self.use_pert_type:
                output['pert_type'] = feature['pert_type'][excerpt]
            if self.use_cell_id:
                output['cell_id'] = feature['cell_id'][excerpt]
            if self.use_pert_idose:
                output['pert_idose'] = feature['pert_idose'][excerpt]
            yield output, label[excerpt]


class DataReaderCV(object):
    def __init__(self, drug_file, pert_id_file, gene_file, data_file, filter, fp_type, device, fold,
                 drug_target_file=None):
        self.device = device
        self.fp_type = fp_type
        if fp_type == 'pubchem':
            self.drug, self.drug_dim = data_utils.read_drug_number(drug_file, 881)
        elif fp_type == 'circular':
            self.drug, self.drug_dim = data_utils.read_drug_number(drug_file, 1024)
        elif fp_type == 'drug-target':
            self.drug, self.drug_dim = data_utils.read_drug_number(drug_file, 978)
        elif fp_type == 'neural':
            self.drug, self.drug_dim = data_utils.read_drug_string(drug_file)
        elif fp_type == 'random':
            self.drug, self.drug_dim = data_utils.read_drug_number(drug_file, 500)
        else:
            raise ValueError("Unknown fingerprint: %s" % fp_type)
        if drug_target_file is not None:
            self.drug_target, self.drug_target_dim = data_utils.read_drug_number(drug_target_file, 978)
        else:
            self.drug_target = None
            self.drug_target_dim = None
        self.gene = data_utils.read_gene(gene_file, self.device)
        train_pert_id, test_pert_id = self.get_id_cv(pert_id_file, fold)
        feature_train, label_train, feature_test, label_test = \
            data_utils.read_data_cv(data_file, filter, train_pert_id, test_pert_id)
        self.train_feature, self.test_feature, self.train_label, self.test_label, self.use_pert_type, self.use_cell_id, \
        self.use_pert_idose = data_utils.transfrom_to_tensor_cv(feature_train, label_train, feature_test, label_test,
                                                                self.drug, fp_type, self.device, self.drug_target)

    def get_id_cv(self, input_file, fold):
        train_pert_id = []
        test_pert_id = []
        with open(input_file, 'r') as f:
            drug = json.load(f)
        for k, v in drug.items():
            pert_id = v.split(',')
            if 'fold_%d' % fold == k:
                test_pert_id += pert_id
            else:
                train_pert_id += pert_id
        return train_pert_id, test_pert_id

    def get_batch_data(self, dataset, batch_size, shuffle):
        if dataset == 'train':
            feature = self.train_feature
            label = self.train_label
        elif dataset == 'test':
            feature = self.test_feature
            label = self.test_label
        if shuffle:
            index = torch.randperm(len(feature['drug'])).long()
            if self.fp_type == 'neural':
                index = index.numpy()
            else:
                index = index.to(self.device)
        for start_idx in range(0, len(feature['drug']), batch_size):
            if shuffle:
                excerpt = index[start_idx:start_idx + batch_size]
            else:
                excerpt = slice(start_idx, start_idx + batch_size)
            output = dict()
            if self.fp_type == 'neural':
                output['drug'] = data_utils.convert_smile_to_feature(feature['drug'][excerpt], self.device)
                output['mask'] = data_utils.create_mask_feature(output['drug'], self.device)
            else:
                output['drug'] = feature['drug'][excerpt]
            if self.drug_target is not None:
                output['drug_target'] = feature['drug_target'][excerpt]
            if self.use_pert_type:
                output['pert_type'] = feature['pert_type'][excerpt]
            if self.use_cell_id:
                output['cell_id'] = feature['cell_id'][excerpt]
            if self.use_pert_idose:
                output['pert_idose'] = feature['pert_idose'][excerpt]
            yield output, label[excerpt]


if __name__ == '__main__':
    filter = {"time": "24H", "pert_id": ['BRD-U41416256', 'BRD-U60236422'], "pert_type": ["trt_cp"],
              "cell_id": ["A375", "HT29", "MCF7", "PC3", "HA1E", "YAPC", "HELA"],
              "pert_idose": ["0.04 um", "0.12 um", "0.37 um", "1.11 um", "3.33 um", "10.0 um"]}
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    fp_type = 'pubchem'
    data = DataReaderCV('../data/drugs_pubchem.csv', '../data/drug_cv.json', '../data/gene_vector.csv',
                        '../data/signature_0_7.csv', filter, fp_type, device, 2)
    for i, batch in enumerate(data.get_batch_data(dataset='train', batch_size=2, shuffle=True)):
        print(batch)
        break


