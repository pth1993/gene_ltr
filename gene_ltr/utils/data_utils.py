import numpy as np
import random
import torch
from sklearn.model_selection import train_test_split
from molecules import Molecules


def convert_bin_2_int(input_vec):
    output_vec = []
    for idx, val in enumerate(input_vec):
        if val == 1:
            output_vec.append(idx + 1)
    return output_vec


def convert_int_2_bin(input_vec, length):
    output_vec = [0] * length
    for i in input_vec:
        output_vec[i-1] = 1
    return output_vec


def convert_regression_2_classification(input_vec):
    output_vec = [0] * len(input_vec)
    sort_idx = np.argsort(input_vec)
    pos_idx = sort_idx[-100:]
    neg_idx = sort_idx[:100]
    for id in pos_idx:
        output_vec[id] = 1
    for id in neg_idx:
        output_vec[id] = 2
    return output_vec


# def read_drug_pubchem(input_file):
#     with open(input_file, 'r') as f:
#         max_length = 0
#         max_index = 0
#         drug = dict()
#         for line in f:
#             line = line.strip().split(',')
#             assert len(line) == 883, "Wrong format"
#             bin_vec = [int(i) for i in line[2:]]
#             int_vec = convert_bin_2_int(bin_vec)
#             drug[line[0]] = int_vec
#             max_length = max(max_length, len(int_vec))
#             max_index = max(max_index, max(int_vec))
#     return drug, max_length, max_index


def read_drug_number(input_file, num_feature):
    drug = []
    drug_vec = []
    with open(input_file, 'r') as f:
        for line in f:
            line = line.strip().split(',')
            assert len(line) == num_feature + 1, "Wrong format"
            bin_vec = [float(i) for i in line[1:]]
            drug.append(line[0])
            drug_vec.append(bin_vec)
    drug_vec = np.asarray(drug_vec, dtype=np.float64)
    index = []
    for i in range(np.shape(drug_vec)[1]):
        if len(set(drug_vec[:, i])) > 1:
            index.append(i)
    drug_vec = drug_vec[:, index]
    drug = dict(zip(drug, drug_vec))
    return drug, len(index)


def read_drug_string(input_file):
    with open(input_file, 'r') as f:
        drug = dict()
        for line in f:
            line = line.strip().split(',')
            assert len(line) == 2, "Wrong format"
            drug[line[0]] = line[1]
    return drug, None


def read_cell(input_file, num_feature):
    cell = dict()
    with open(input_file) as f:
        for line in f:
            line = line.strip().split(',')
            assert len(line) == num_feature + 1, 'Wrong format'
            cell[line[0]] = [float(i) for i in line[1:]]
    return cell, num_feature


def read_gene(input_file, device):
    with open(input_file, 'r') as f:
        gene = []
        for line in f:
            line = line.strip().split(',')
            assert len(line) == 129, "Wrong format"
            gene.append([float(i) for i in line[1:]])
    return torch.from_numpy(np.asarray(gene, dtype=np.float64)).to(device)


def convert_smile_to_feature(smiles, device):
    molecules = Molecules(smiles)
    node_repr = torch.FloatTensor([node.data for node in molecules.get_node_list('atom')]).to(device).double()
    edge_repr = torch.FloatTensor([node.data for node in molecules.get_node_list('bond')]).to(device).double()
    return {'molecules': molecules, 'atom': node_repr, 'bond': edge_repr}


def create_mask_feature(data, device):
    batch_idx = data['molecules'].get_neighbor_idx_by_batch('atom')
    molecule_length = [len(idx) for idx in batch_idx]
    mask = torch.zeros(len(batch_idx), max(molecule_length)).to(device).double()
    for idx, length in enumerate(molecule_length):
        mask[idx][:length] = 1
    return mask


def choose_mean_example(examples):
    num_example = len(examples)
    mean_value = (num_example - 1) / 2
    indexes = np.argsort(examples, axis=0)
    indexes = np.argsort(indexes, axis=0)
    indexes = np.mean(indexes, axis=1)
    distance = (indexes - mean_value)**2
    index = np.argmin(distance)
    return examples[index]


def split_data_by_pert_id(pert_id):
    random.shuffle(pert_id)
    num_pert_id = len(pert_id)
    fold_size = int(num_pert_id/10)
    train_pert_id = pert_id[:fold_size*6]
    dev_pert_id = pert_id[fold_size*6: fold_size*8]
    test_pert_id = pert_id[fold_size*8:]
    return train_pert_id, dev_pert_id, test_pert_id


def split_data_by_pert_id_cv(input_file, fold):
    with open(input_file) as f:
        pert_id = f.readline().strip().split(',')
    num_pert_id = len(pert_id)
    fold_size = int(num_pert_id / 10)
    fold_0 = pert_id[:fold_size * 2]
    fold_1 = pert_id[fold_size * 2:fold_size * 4]
    fold_2 = pert_id[fold_size * 4:fold_size * 6]
    fold_3 = pert_id[fold_size * 6:fold_size * 8]
    fold_4 = pert_id[fold_size * 8:]
    if fold == 0:
        test_pert_id = fold_0
        dev_pert_id = fold_1
        train_pert_id = fold_2 + fold_3 + fold_4
    elif fold == 1:
        test_pert_id = fold_1
        dev_pert_id = fold_2
        train_pert_id = fold_3 + fold_4 + fold_0
    elif fold == 2:
        test_pert_id = fold_2
        dev_pert_id = fold_3
        train_pert_id = fold_4 + fold_0 + fold_1
    elif fold == 3:
        test_pert_id = fold_3
        dev_pert_id = fold_4
        train_pert_id = fold_0 + fold_1 + fold_2
    elif fold == 4:
        test_pert_id = fold_4
        dev_pert_id = fold_0
        train_pert_id = fold_1 + fold_2 + fold_3
    else:
        raise ValueError("Unknown fold number: %d" % fold)
    return train_pert_id, dev_pert_id, test_pert_id


def get_id_test(input_file):
    with open(input_file) as f:
        for line in f:
            line = line.strip().split(',')
            if line[0] == 'test':
                test_id = line[1:]
            elif line[0] == 'dev':
                dev_id = line[1:]
            else:
                raise ValueError("Unknown dataset: %s" % line[0])
    return dev_id, test_id


def read_data_old(input_file, filter):
    feature = []
    label_regression = []
    label_classification = []
    data = dict()
    pert_id = []
    with open(input_file, 'r') as f:
        f.readline()  # skip header
        for line in f:
            line = line.strip().split(',')
            assert len(line) == 983, "Wrong format"
            if filter["time"] in line[0] and line[1] not in filter["pert_id"] and line[2] in filter["pert_type"] \
                    and line[3] in filter["cell_id"] and line[4] in filter["pert_idose"]:
                ft = ','.join(line[1:5])
                lb = [float(i) for i in line[5:]]
                if ft in data.keys():
                    data[ft].append(lb)
                else:
                    data[ft] = [lb]
    for ft, lb in sorted(data.items()):
        ft = ft.split(',')
        feature.append(ft)
        pert_id.append(ft[0])
        if len(lb) == 1:
            label_regression.append(lb[0])
            label_classification.append(convert_regression_2_classification(lb[0]))
        else:
            lb = choose_mean_example(lb)
            label_regression.append(lb)
            label_classification.append(convert_regression_2_classification(lb))
    return np.asarray(feature), np.asarray(label_regression, dtype=np.float64), \
           np.asarray(label_classification, dtype=np.float64), sorted(list(set(pert_id)))


def read_data_train(input_file, filter, dev_pert_id, test_pert_id, dev_cell_id, test_cell_id):
    feature = []
    label = []
    data = dict()
    pert_id = []
    filter_drug = filter["pert_id"] + dev_pert_id + test_pert_id
    filter_cell = list(set(filter["cell_id"]) - set(dev_cell_id + test_cell_id))
    with open(input_file, 'r') as f:
        f.readline()  # skip header
        for line in f:
            line = line.strip().split(',')
            assert len(line) == 983, "Wrong format"
            if filter["time"] in line[0] and line[1] not in filter_drug and line[2] in filter["pert_type"] \
                    and line[3] in filter_cell and line[4] in filter["pert_idose"]:
                ft = ','.join(line[1:5])
                lb = [float(i) for i in line[5:]]
                if ft in data.keys():
                    data[ft].append(lb)
                else:
                    data[ft] = [lb]
    for ft, lb in sorted(data.items()):
        ft = ft.split(',')
        feature.append(ft)
        pert_id.append(ft[0])
        if len(lb) == 1:
            label.append(lb[0])
        else:
            lb = choose_mean_example(lb)
            label.append(lb)
    return np.asarray(feature), np.asarray(label, dtype=np.float64)


def read_data_test(input_file, filter, dev_pert_id, test_pert_id, dev_cell_id, test_cell_id, id_type):
    feature = []
    label = []
    data = dict()
    pert_id = []
    if id_type == 'drug':
        filter_drug = filter["pert_id"]
        filter_cell = list(set(filter["cell_id"]) - set(dev_cell_id + test_cell_id))
    elif id_type == 'cell':
        filter_drug = filter["pert_id"] + dev_pert_id + test_pert_id
        filter_cell = filter["cell_id"]
    else:
        raise ValueError("Unknown id_type: %s" % id_type)
    with open(input_file, 'r') as f:
        f.readline()  # skip header
        for line in f:
            line = line.strip().split(',')
            assert len(line) == 983, "Wrong format"
            if filter["time"] in line[0] and line[1] not in filter_drug and line[2] in filter["pert_type"] \
                    and line[3] in filter_cell and line[4] in filter["pert_idose"]:
                ft = ','.join(line[1:5])
                lb = [float(i) for i in line[5:]]
                if ft in data.keys():
                    data[ft].append(lb)
                else:
                    data[ft] = [lb]
    for ft, lb in sorted(data.items()):
        ft = ft.split(',')
        feature.append(ft)
        pert_id.append(ft[0])
        if len(lb) == 1:
            label.append(lb[0])
        else:
            lb = choose_mean_example(lb)
            label.append(lb)
    return np.asarray(feature), np.asarray(label, dtype=np.float64)


def read_data_tradition(input_file, filter, dev_cell_id, test_cell_id):
    feature_train = []
    feature_test = []
    label_train = []
    label_test = []
    data = dict()
    filter_drug = filter["pert_id"]
    filter_cell = list(set(filter["cell_id"]) - set(dev_cell_id + test_cell_id))
    with open(input_file, 'r') as f:
        f.readline()  # skip header
        for line in f:
            line = line.strip().split(',')
            assert len(line) == 983, "Wrong format"
            if filter["time"] in line[0] and line[1] not in filter_drug and line[2] in filter["pert_type"] \
                    and line[3] in filter_cell and line[4] in filter["pert_idose"]:
                ft = ','.join(line[1:5])
                lb = [float(i) for i in line[5:]]
                if ft in data.keys():
                    data[ft].append(lb)
                else:
                    data[ft] = [lb]
    keys = sorted(data.keys())
    train_key, test_key = train_test_split(keys, test_size=0.2, random_state=75)
    is_test = False
    for ft, lb in sorted(data.items()):
        if ft in test_key:
            is_test = True
        ft = ft.split(',')
        if is_test:
            feature_test.append(ft)
            if len(lb) == 1:
                label_test.append(lb[0])
            else:
                lb = choose_mean_example(lb)
                label_test.append(lb)
        else:
            feature_train.append(ft)
            if len(lb) == 1:
                label_train.append(lb[0])
            else:
                lb = choose_mean_example(lb)
                label_train.append(lb)
        is_test = False
    return np.asarray(feature_train), np.asarray(label_train, dtype=np.float64), np.asarray(feature_test), \
           np.asarray(label_test, dtype=np.float64)


def read_data_tradition_1(input_file, filter, dev_cell_id, test_cell_id):
    feature_train = []
    feature_dev = []
    feature_test = []
    label_train = []
    label_dev = []
    label_test = []
    data = dict()
    filter_drug = filter["pert_id"]
    filter_cell = list(set(filter["cell_id"]) - set(dev_cell_id + test_cell_id))
    with open(input_file, 'r') as f:
        f.readline()  # skip header
        for line in f:
            line = line.strip().split(',')
            assert len(line) == 983, "Wrong format"
            if filter["time"] in line[0] and line[1] not in filter_drug and line[2] in filter["pert_type"] \
                    and line[3] in filter_cell and line[4] in filter["pert_idose"]:
                ft = ','.join(line[1:5])
                lb = [float(i) for i in line[5:]]
                if ft in data.keys():
                    data[ft].append(lb)
                else:
                    data[ft] = [lb]
    keys = sorted(data.keys())
    # train_key, test_key = train_test_split(keys, test_size=0.2, random_state=75)
    # train_key, dev_key = train_test_split(train_key, test_size=0.25, random_state=75)
    train_key, test_key = train_test_split(keys, test_size=0.1, random_state=1)
    train_key, dev_key = train_test_split(train_key, test_size=0.1, random_state=1)
    for ft, lb in sorted(data.items()):
        if ft in train_key:
            check = 0
        elif ft in dev_key:
            check = 1
        elif ft in test_key:
            check = 2
        else:
            raise ValueError('Wrong feature')
        ft = ft.split(',')
        if check == 0:
            feature_train.append(ft)
            if len(lb) == 1:
                label_train.append(lb[0])
            else:
                lb = choose_mean_example(lb)
                label_train.append(lb)
        elif check == 1:
            feature_dev.append(ft)
            if len(lb) == 1:
                label_dev.append(lb[0])
            else:
                lb = choose_mean_example(lb)
                label_dev.append(lb)
        elif check == 2:
            feature_test.append(ft)
            if len(lb) == 1:
                label_test.append(lb[0])
            else:
                lb = choose_mean_example(lb)
                label_test.append(lb)
        else:
            raise ValueError('Wrong feature')
    return np.asarray(feature_train), np.asarray(label_train, dtype=np.float64), np.asarray(feature_dev), \
           np.asarray(label_dev, dtype=np.float64), np.asarray(feature_test), np.asarray(label_test, dtype=np.float64)


def read_data_cv(input_file, filter, train_pert_id, test_pert_id):
    feature_train = []
    label_train = []
    feature_test = []
    label_test = []
    data = dict()
    pert_id = []
    with open(input_file, 'r') as f:
        f.readline()  # skip header
        for line in f:
            line = line.strip().split(',')
            assert len(line) == 983, "Wrong format"
            if filter["time"] in line[0] and line[1] not in filter["pert_id"] and line[2] in filter["pert_type"] \
                    and line[3] in filter["cell_id"] and line[4] in filter["pert_idose"]:
                ft = ','.join(line[1:5])
                lb = [float(i) for i in line[5:]]
                if ft in data.keys():
                    data[ft].append(lb)
                else:
                    data[ft] = [lb]
    for ft, lb in sorted(data.items()):
        ft = ft.split(',')
        if ft[0] in train_pert_id:
            feature_train.append(ft)
            if len(lb) == 1:
                label_train.append(lb[0])
            else:
                lb = choose_mean_example(lb)
                label_train.append(lb)
        elif ft[0] in test_pert_id:
            feature_test.append(ft)
            if len(lb) == 1:
                label_test.append(lb[0])
            else:
                lb = choose_mean_example(lb)
                label_test.append(lb)
        else:
            raise ValueError('Unknown drug')
        pert_id.append(ft[0])
    return np.asarray(feature_train), np.asarray(label_train, dtype=np.float64), np.asarray(feature_test), \
           np.asarray(label_test, dtype=np.float64)


def transfrom_to_tensor(feature_train, label_train, feature_dev, label_dev, feature_test, label_test, drug, fp_type,
                        device, drug_target):
    train_drug_feature = []
    dev_drug_feature = []
    test_drug_feature = []
    train_drug_target_feature = []
    dev_drug_target_feature = []
    test_drug_target_feature = []
    # pert_type_set = sorted(list(set(feature_train[:, 1])))
    # cell_id_set = sorted(list(set(feature_train[:, 2])))
    # pert_idose_set = sorted(list(set(feature_train[:, 3])))
    pert_type_set = ['trt_cp']
    cell_id_set = ['HA1E', 'HT29', 'MCF7', 'YAPC', 'HELA', 'PC3', 'A375']
    pert_idose_set = ['1.11 um', '0.37 um', '10.0 um', '0.04 um', '3.33 um', '0.12 um']
    use_pert_type = False
    use_cell_id = False
    use_pert_idose = False
    if len(pert_type_set) > 1:
        pert_type_dict = dict(zip(pert_type_set, list(range(len(pert_type_set)))))
        train_pert_type_feature = []
        dev_pert_type_feature = []
        test_pert_type_feature = []
        use_pert_type = True
    if len(cell_id_set) > 1:
        cell_id_dict = dict(zip(cell_id_set, list(range(len(cell_id_set)))))
        train_cell_id_feature = []
        dev_cell_id_feature = []
        test_cell_id_feature = []
        use_cell_id = True
    if len(pert_idose_set) > 1:
        pert_idose_dict = dict(zip(pert_idose_set, list(range(len(pert_idose_set)))))
        train_pert_idose_feature = []
        dev_pert_idose_feature = []
        test_pert_idose_feature = []
        use_pert_idose = True
    print('Feature Summary:')
    print(pert_type_set)
    print(cell_id_set)
    print(pert_idose_set)

    for i, ft in enumerate(feature_train):
        if fp_type in ['pubchem', 'circular', 'random', 'drug-target']:
            drug_fp = drug[ft[0]]
            drug_fp = np.asarray(drug_fp, dtype=np.float64)
        elif fp_type == 'neural':
            drug_fp = drug[ft[0]]
        else:
            raise ValueError("Unknown fingerprint: %s" % fp_type)
        train_drug_feature.append(drug_fp)
        if drug_target is not None:
            train_drug_target_feature.append(drug_target[ft[0]])
        if use_pert_type:
            pert_type_feature = np.zeros(len(pert_type_set))
            pert_type_feature[pert_type_dict[ft[1]]] = 1
            train_pert_type_feature.append(np.array(pert_type_feature, dtype=np.float64))
        if use_cell_id:
            cell_id_feature = np.zeros(len(cell_id_set))
            cell_id_feature[cell_id_dict[ft[2]]] = 1
            train_cell_id_feature.append(np.array(cell_id_feature, dtype=np.float64))
        if use_pert_idose:
            pert_idose_feature = np.zeros(len(pert_idose_set))
            pert_idose_feature[pert_idose_dict[ft[3]]] = 1
            train_pert_idose_feature.append(np.array(pert_idose_feature, dtype=np.float64))

    for i, ft in enumerate(feature_dev):
        if fp_type in ['pubchem', 'circular', 'random', 'drug-target']:
            drug_fp = drug[ft[0]]
            drug_fp = np.asarray(drug_fp, dtype=np.float64)
        elif fp_type == 'neural':
            drug_fp = drug[ft[0]]
        else:
            raise ValueError("Unknown fingerprint: %s" % fp_type)
        dev_drug_feature.append(drug_fp)
        if drug_target is not None:
            dev_drug_target_feature.append(drug_target[ft[0]])
        if use_pert_type:
            pert_type_feature = np.zeros(len(pert_type_set))
            pert_type_feature[pert_type_dict[ft[1]]] = 1
            dev_pert_type_feature.append(np.array(pert_type_feature, dtype=np.float64))
        if use_cell_id:
            cell_id_feature = np.zeros(len(cell_id_set))
            cell_id_feature[cell_id_dict[ft[2]]] = 1
            dev_cell_id_feature.append(np.array(cell_id_feature, dtype=np.float64))
        if use_pert_idose:
            pert_idose_feature = np.zeros(len(pert_idose_set))
            pert_idose_feature[pert_idose_dict[ft[3]]] = 1
            dev_pert_idose_feature.append(np.array(pert_idose_feature, dtype=np.float64))

    for i, ft in enumerate(feature_test):
        if fp_type in ['pubchem', 'circular', 'random', 'drug-target']:
            drug_fp = drug[ft[0]]
            drug_fp = np.asarray(drug_fp, dtype=np.float64)
        elif fp_type == 'neural':
            drug_fp = drug[ft[0]]
        else:
            raise ValueError("Unknown fingerprint: %s" % fp_type)
        test_drug_feature.append(drug_fp)
        if drug_target is not None:
            test_drug_target_feature.append(drug_target[ft[0]])
        if use_pert_type:
            pert_type_feature = np.zeros(len(pert_type_set))
            pert_type_feature[pert_type_dict[ft[1]]] = 1
            test_pert_type_feature.append(np.array(pert_type_feature, dtype=np.float64))
        if use_cell_id:
            cell_id_feature = np.zeros(len(cell_id_set))
            cell_id_feature[cell_id_dict[ft[2]]] = 1
            test_cell_id_feature.append(np.array(cell_id_feature, dtype=np.float64))
        if use_pert_idose:
            pert_idose_feature = np.zeros(len(pert_idose_set))
            pert_idose_feature[pert_idose_dict[ft[3]]] = 1
            test_pert_idose_feature.append(np.array(pert_idose_feature, dtype=np.float64))

    train_feature = dict()
    dev_feature = dict()
    test_feature = dict()
    if fp_type in ['pubchem', 'circular', 'random', 'drug-target']:
        train_feature['drug'] = torch.from_numpy(np.asarray(train_drug_feature, dtype=np.float64)).to(device)
        dev_feature['drug'] = torch.from_numpy(np.asarray(dev_drug_feature, dtype=np.float64)).to(device)
        test_feature['drug'] = torch.from_numpy(np.asarray(test_drug_feature, dtype=np.float64)).to(device)
    elif fp_type == 'neural':
        train_feature['drug'] = np.asarray(train_drug_feature)
        dev_feature['drug'] = np.asarray(dev_drug_feature)
        test_feature['drug'] = np.asarray(test_drug_feature)
    else:
        raise ValueError('Unknown fingerprint: %s' % fp_type)
    if drug_target is not None:
        train_feature['drug_target'] = torch.from_numpy(np.asarray(train_drug_target_feature, dtype=np.float64)).to(device)
        dev_feature['drug_target'] = torch.from_numpy(np.asarray(dev_drug_target_feature, dtype=np.float64)).to(device)
        test_feature['drug_target'] = torch.from_numpy(np.asarray(test_drug_target_feature, dtype=np.float64)).to(device)
    if use_pert_type:
        train_feature['pert_type'] = torch.from_numpy(np.asarray(train_pert_type_feature, dtype=np.float64)).to(device)
        dev_feature['pert_type'] = torch.from_numpy(np.asarray(dev_pert_type_feature, dtype=np.float64)).to(device)
        test_feature['pert_type'] = torch.from_numpy(np.asarray(test_pert_type_feature, dtype=np.float64)).to(device)
    if use_cell_id:
        train_feature['cell_id'] = torch.from_numpy(np.asarray(train_cell_id_feature, dtype=np.float64)).to(device)
        dev_feature['cell_id'] = torch.from_numpy(np.asarray(dev_cell_id_feature, dtype=np.float64)).to(device)
        test_feature['cell_id'] = torch.from_numpy(np.asarray(test_cell_id_feature, dtype=np.float64)).to(device)
    if use_pert_idose:
        train_feature['pert_idose'] = torch.from_numpy(np.asarray(train_pert_idose_feature, dtype=np.float64)).to(device)
        dev_feature['pert_idose'] = torch.from_numpy(np.asarray(dev_pert_idose_feature, dtype=np.float64)).to(device)
        test_feature['pert_idose'] = torch.from_numpy(np.asarray(test_pert_idose_feature, dtype=np.float64)).to(device)
    train_label_regression = torch.from_numpy(label_train).to(device)
    dev_label_regression = torch.from_numpy(label_dev).to(device)
    test_label_regression = torch.from_numpy(label_test).to(device)
    return train_feature, dev_feature, test_feature, train_label_regression, dev_label_regression, \
           test_label_regression, use_pert_type, use_cell_id, use_pert_idose


def transfrom_to_tensor_1(feature_train, label_train, feature_dev, label_dev, feature_test, label_test, drug, fp_type,
                          device, drug_target, cell):
    train_drug_feature = []
    dev_drug_feature = []
    test_drug_feature = []
    train_drug_target_feature = []
    dev_drug_target_feature = []
    test_drug_target_feature = []
    pert_type_set = sorted(list(set(feature_train[:, 1])))
    cell_id_set = sorted(list(set(feature_train[:, 2])))
    pert_idose_set = sorted(list(set(feature_train[:, 3])))
    # pert_type_set = ['trt_cp']
    # cell_id_set = ['HA1E', 'HT29', 'MCF7', 'YAPC', 'HELA', 'PC3', 'A375']
    # pert_idose_set = ['1.11 um', '0.37 um', '10.0 um', '0.04 um', '3.33 um', '0.12 um']
    use_pert_type = False
    use_cell_id = False
    use_pert_idose = False
    if len(pert_type_set) > 1:
        pert_type_dict = dict(zip(pert_type_set, list(range(len(pert_type_set)))))
        train_pert_type_feature = []
        dev_pert_type_feature = []
        test_pert_type_feature = []
        use_pert_type = True
    if len(cell_id_set) > 1:
        # cell_id_dict = dict(zip(cell_id_set, list(range(len(cell_id_set)))))
        train_cell_id_feature = []
        dev_cell_id_feature = []
        test_cell_id_feature = []
        use_cell_id = True
    if len(pert_idose_set) > 1:
        pert_idose_dict = dict(zip(pert_idose_set, list(range(len(pert_idose_set)))))
        train_pert_idose_feature = []
        dev_pert_idose_feature = []
        test_pert_idose_feature = []
        use_pert_idose = True
    print('Feature Summary:')
    print(pert_type_set)
    print(cell_id_set)
    print(pert_idose_set)

    for i, ft in enumerate(feature_train):
        if fp_type in ['pubchem', 'circular', 'random', 'drug-target']:
            drug_fp = drug[ft[0]]
            drug_fp = np.asarray(drug_fp, dtype=np.float64)
        elif fp_type == 'neural':
            drug_fp = drug[ft[0]]
        else:
            raise ValueError("Unknown fingerprint: %s" % fp_type)
        train_drug_feature.append(drug_fp)
        if drug_target is not None:
            train_drug_target_feature.append(drug_target[ft[0]])
        if use_pert_type:
            pert_type_feature = np.zeros(len(pert_type_set))
            pert_type_feature[pert_type_dict[ft[1]]] = 1
            train_pert_type_feature.append(np.array(pert_type_feature, dtype=np.float64))
        if use_cell_id:
            cell_id_feature = cell[ft[2]]
            train_cell_id_feature.append(np.array(cell_id_feature, dtype=np.float64))
        if use_pert_idose:
            pert_idose_feature = np.zeros(len(pert_idose_set))
            pert_idose_feature[pert_idose_dict[ft[3]]] = 1
            train_pert_idose_feature.append(np.array(pert_idose_feature, dtype=np.float64))

    for i, ft in enumerate(feature_dev):
        if fp_type in ['pubchem', 'circular', 'random', 'drug-target']:
            drug_fp = drug[ft[0]]
            drug_fp = np.asarray(drug_fp, dtype=np.float64)
        elif fp_type == 'neural':
            drug_fp = drug[ft[0]]
        else:
            raise ValueError("Unknown fingerprint: %s" % fp_type)
        dev_drug_feature.append(drug_fp)
        if drug_target is not None:
            dev_drug_target_feature.append(drug_target[ft[0]])
        if use_pert_type:
            pert_type_feature = np.zeros(len(pert_type_set))
            pert_type_feature[pert_type_dict[ft[1]]] = 1
            dev_pert_type_feature.append(np.array(pert_type_feature, dtype=np.float64))
        if use_cell_id:
            cell_id_feature = cell[ft[2]]
            dev_cell_id_feature.append(np.array(cell_id_feature, dtype=np.float64))
        if use_pert_idose:
            pert_idose_feature = np.zeros(len(pert_idose_set))
            pert_idose_feature[pert_idose_dict[ft[3]]] = 1
            dev_pert_idose_feature.append(np.array(pert_idose_feature, dtype=np.float64))

    for i, ft in enumerate(feature_test):
        if fp_type in ['pubchem', 'circular', 'random', 'drug-target']:
            drug_fp = drug[ft[0]]
            drug_fp = np.asarray(drug_fp, dtype=np.float64)
        elif fp_type == 'neural':
            drug_fp = drug[ft[0]]
        else:
            raise ValueError("Unknown fingerprint: %s" % fp_type)
        test_drug_feature.append(drug_fp)
        if drug_target is not None:
            test_drug_target_feature.append(drug_target[ft[0]])
        if use_pert_type:
            pert_type_feature = np.zeros(len(pert_type_set))
            pert_type_feature[pert_type_dict[ft[1]]] = 1
            test_pert_type_feature.append(np.array(pert_type_feature, dtype=np.float64))
        if use_cell_id:
            cell_id_feature = cell[ft[2]]
            test_cell_id_feature.append(np.array(cell_id_feature, dtype=np.float64))
        if use_pert_idose:
            pert_idose_feature = np.zeros(len(pert_idose_set))
            pert_idose_feature[pert_idose_dict[ft[3]]] = 1
            test_pert_idose_feature.append(np.array(pert_idose_feature, dtype=np.float64))

    train_feature = dict()
    dev_feature = dict()
    test_feature = dict()
    if fp_type in ['pubchem', 'circular', 'random', 'drug-target']:
        train_feature['drug'] = torch.from_numpy(np.asarray(train_drug_feature, dtype=np.float64)).to(device)
        dev_feature['drug'] = torch.from_numpy(np.asarray(dev_drug_feature, dtype=np.float64)).to(device)
        test_feature['drug'] = torch.from_numpy(np.asarray(test_drug_feature, dtype=np.float64)).to(device)
    elif fp_type == 'neural':
        train_feature['drug'] = np.asarray(train_drug_feature)
        dev_feature['drug'] = np.asarray(dev_drug_feature)
        test_feature['drug'] = np.asarray(test_drug_feature)
    else:
        raise ValueError('Unknown fingerprint: %s' % fp_type)
    if drug_target is not None:
        train_feature['drug_target'] = torch.from_numpy(np.asarray(train_drug_target_feature, dtype=np.float64)).to(device)
        dev_feature['drug_target'] = torch.from_numpy(np.asarray(dev_drug_target_feature, dtype=np.float64)).to(device)
        test_feature['drug_target'] = torch.from_numpy(np.asarray(test_drug_target_feature, dtype=np.float64)).to(device)
    if use_pert_type:
        train_feature['pert_type'] = torch.from_numpy(np.asarray(train_pert_type_feature, dtype=np.float64)).to(device)
        dev_feature['pert_type'] = torch.from_numpy(np.asarray(dev_pert_type_feature, dtype=np.float64)).to(device)
        test_feature['pert_type'] = torch.from_numpy(np.asarray(test_pert_type_feature, dtype=np.float64)).to(device)
    if use_cell_id:
        train_feature['cell_id'] = torch.from_numpy(np.asarray(train_cell_id_feature, dtype=np.float64)).to(device)
        dev_feature['cell_id'] = torch.from_numpy(np.asarray(dev_cell_id_feature, dtype=np.float64)).to(device)
        test_feature['cell_id'] = torch.from_numpy(np.asarray(test_cell_id_feature, dtype=np.float64)).to(device)
    if use_pert_idose:
        train_feature['pert_idose'] = torch.from_numpy(np.asarray(train_pert_idose_feature, dtype=np.float64)).to(device)
        dev_feature['pert_idose'] = torch.from_numpy(np.asarray(dev_pert_idose_feature, dtype=np.float64)).to(device)
        test_feature['pert_idose'] = torch.from_numpy(np.asarray(test_pert_idose_feature, dtype=np.float64)).to(device)
    train_label_regression = torch.from_numpy(label_train).to(device)
    dev_label_regression = torch.from_numpy(label_dev).to(device)
    test_label_regression = torch.from_numpy(label_test).to(device)
    return train_feature, dev_feature, test_feature, train_label_regression, dev_label_regression, \
           test_label_regression, use_pert_type, use_cell_id, use_pert_idose


def transfrom_to_tensor_cv(feature_train, label_train, feature_test, label_test, drug, fp_type, device, drug_target):
    train_drug_feature = []
    test_drug_feature = []
    train_drug_target_feature = []
    test_drug_target_feature = []
    pert_type_set = sorted(list(set(feature_train[:, 1])))
    cell_id_set = sorted(list(set(feature_train[:, 2])))
    pert_idose_set = sorted(list(set(feature_train[:, 3])))
    use_pert_type = False
    use_cell_id = False
    use_pert_idose = False
    if len(pert_type_set) > 1:
        pert_type_dict = dict(zip(pert_type_set, list(range(len(pert_type_set)))))
        train_pert_type_feature = []
        test_pert_type_feature = []
        use_pert_type = True
    if len(cell_id_set) > 1:
        cell_id_dict = dict(zip(cell_id_set, list(range(len(cell_id_set)))))
        train_cell_id_feature = []
        test_cell_id_feature = []
        use_cell_id = True
    if len(pert_idose_set) > 1:
        pert_idose_dict = dict(zip(pert_idose_set, list(range(len(pert_idose_set)))))
        train_pert_idose_feature = []
        test_pert_idose_feature = []
        use_pert_idose = True
    print('Feature Summary:')
    print(pert_type_set)
    print(cell_id_set)
    print(pert_idose_set)

    for i, ft in enumerate(feature_train):
        if fp_type in ['pubchem', 'circular', 'random', 'drug-target']:
            drug_fp = drug[ft[0]]
            drug_fp = np.asarray(drug_fp, dtype=np.float64)
        elif fp_type == 'neural':
            drug_fp = drug[ft[0]]
        else:
            raise ValueError("Unknown fingerprint: %s" % fp_type)
        train_drug_feature.append(drug_fp)
        if drug_target is not None:
            train_drug_target_feature.append(drug_target[ft[0]])
        if use_pert_type:
            pert_type_feature = np.zeros(len(pert_type_set))
            pert_type_feature[pert_type_dict[ft[1]]] = 1
            train_pert_type_feature.append(np.array(pert_type_feature, dtype=np.float64))
        if use_cell_id:
            cell_id_feature = np.zeros(len(cell_id_set))
            cell_id_feature[cell_id_dict[ft[2]]] = 1
            train_cell_id_feature.append(np.array(cell_id_feature, dtype=np.float64))
        if use_pert_idose:
            pert_idose_feature = np.zeros(len(pert_idose_set))
            pert_idose_feature[pert_idose_dict[ft[3]]] = 1
            train_pert_idose_feature.append(np.array(pert_idose_feature, dtype=np.float64))

    for i, ft in enumerate(feature_test):
        if fp_type in ['pubchem', 'circular', 'random', 'drug-target']:
            drug_fp = drug[ft[0]]
            drug_fp = np.asarray(drug_fp, dtype=np.float64)
        elif fp_type == 'neural':
            drug_fp = drug[ft[0]]
        else:
            raise ValueError("Unknown fingerprint: %s" % fp_type)
        test_drug_feature.append(drug_fp)
        if drug_target is not None:
            test_drug_target_feature.append(drug_target[ft[0]])
        if use_pert_type:
            pert_type_feature = np.zeros(len(pert_type_set))
            pert_type_feature[pert_type_dict[ft[1]]] = 1
            test_pert_type_feature.append(np.array(pert_type_feature, dtype=np.float64))
        if use_cell_id:
            cell_id_feature = np.zeros(len(cell_id_set))
            cell_id_feature[cell_id_dict[ft[2]]] = 1
            test_cell_id_feature.append(np.array(cell_id_feature, dtype=np.float64))
        if use_pert_idose:
            pert_idose_feature = np.zeros(len(pert_idose_set))
            pert_idose_feature[pert_idose_dict[ft[3]]] = 1
            test_pert_idose_feature.append(np.array(pert_idose_feature, dtype=np.float64))

    train_feature = dict()
    test_feature = dict()
    if fp_type in ['pubchem', 'circular', 'random', 'drug-target']:
        train_feature['drug'] = torch.from_numpy(np.asarray(train_drug_feature, dtype=np.float64)).to(device)
        test_feature['drug'] = torch.from_numpy(np.asarray(test_drug_feature, dtype=np.float64)).to(device)
    elif fp_type == 'neural':
        train_feature['drug'] = np.asarray(train_drug_feature)
        test_feature['drug'] = np.asarray(test_drug_feature)
    else:
        raise ValueError('Unknown fingerprint: %s' % fp_type)
    if drug_target is not None:
        train_feature['drug_target'] = torch.from_numpy(np.asarray(train_drug_target_feature, dtype=np.float64)).to(device)
        test_feature['drug_target'] = torch.from_numpy(np.asarray(test_drug_target_feature, dtype=np.float64)).to(device)
    if use_pert_type:
        train_feature['pert_type'] = torch.from_numpy(np.asarray(train_pert_type_feature, dtype=np.float64)).to(device)
        test_feature['pert_type'] = torch.from_numpy(np.asarray(test_pert_type_feature, dtype=np.float64)).to(device)
    if use_cell_id:
        train_feature['cell_id'] = torch.from_numpy(np.asarray(train_cell_id_feature, dtype=np.float64)).to(device)
        test_feature['cell_id'] = torch.from_numpy(np.asarray(test_cell_id_feature, dtype=np.float64)).to(device)
    if use_pert_idose:
        train_feature['pert_idose'] = torch.from_numpy(np.asarray(train_pert_idose_feature, dtype=np.float64)).to(device)
        test_feature['pert_idose'] = torch.from_numpy(np.asarray(test_pert_idose_feature, dtype=np.float64)).to(device)
    train_label_regression = torch.from_numpy(label_train).to(device)
    test_label_regression = torch.from_numpy(label_test).to(device)
    return train_feature, test_feature, train_label_regression, test_label_regression, use_pert_type, use_cell_id, \
           use_pert_idose


if __name__ == '__main__':
    filter = {"time": "24H", "pert_id": ['BRD-U41416256', 'BRD-U60236422'], "pert_type": ["trt_cp"],
              "cell_id": ['A375', 'A549', 'ASC.C', 'BT20', 'CD34', 'HA1E', 'HCC515', 'HELA', 'HEPG2', 'HME1', 'HS578T',
                          'HT29', 'LNCAP', 'MCF10A', 'MCF7', 'MDAMB231', 'NEU', 'NPC.TAK', 'PC3', 'SKBR3', 'SKL', 'YAPC'],
              "pert_idose": ["0.04 um", "0.12 um", "0.37 um", "1.11 um", "3.33 um", "10.0 um"]}
    dev_cell_id = ['LNCAP','MCF10A','MDAMB231','NEU','NPC.TAK','SKBR3','SKL']
    test_cell_id = ['A549','ASC.C','BT20','CD34','HCC515','HEPG2','HME1','HS578T']
    read_data_tradition('../data/signature_0_7.csv', filter, dev_cell_id, test_cell_id)
