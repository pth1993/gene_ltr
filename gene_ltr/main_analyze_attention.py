import numpy as np
from scipy.spatial import distance


def precision_k(label_test, label_predict, k):
    num_pos = 100
    num_neg = 100
    label_test = np.argsort(label_test, axis=1)
    label_predict = np.argsort(label_predict, axis=1)
    precision_k_neg = []
    precision_k_pos = []
    neg_test_set = label_test[:, :num_neg]
    pos_test_set = label_test[:, -num_pos:]
    neg_predict_set = label_predict[:, :k]
    pos_predict_set = label_predict[:, -k:]
    for i in range(len(neg_test_set)):
        neg_test = set(neg_test_set[i])
        pos_test = set(pos_test_set[i])
        neg_predict = set(neg_predict_set[i])
        pos_predict = set(pos_predict_set[i])
        precision_k_neg.append(len(neg_test.intersection(neg_predict)) / k)
        precision_k_pos.append(len(pos_test.intersection(pos_predict)) / k)
    return np.mean(precision_k_neg), np.mean(precision_k_pos)


def choose_mean_example(examples):
    num_example = len(examples)
    mean_value = (num_example - 1) / 2
    indexes = np.argsort(examples, axis=0)
    indexes = np.argsort(indexes, axis=0)
    indexes = np.mean(indexes, axis=1)
    distance = (indexes - mean_value)**2
    index = np.argmin(distance)
    return examples[index]


def get_id_test(input_file):
    with open(input_file) as f:
        for line in f:
            line = line.strip().split(',')
            if line[0] == 'test':
                test_id = line[1:]
            elif line[0] == 'dev':
                dev_id = line[1:]
            else:
                raise ValueError("Unknown dataset: %s" % line[0])
    return dev_id, test_id


def read_data(input_file, filter, dev_pert_id, test_pert_id):
    feature = []
    label = []
    data = dict()
    pert_id = []
    filter_drug = filter["pert_id"] + dev_pert_id + test_pert_id
    filter_cell = filter["cell_id"]
    with open(input_file, 'r') as f:
        f.readline()  # skip header
        for line in f:
            line = line.strip().split(',')
            assert len(line) == 983, "Wrong format"
            if filter["time"] in line[0] and line[1] not in filter_drug and line[2] in filter["pert_type"] \
                    and line[3] in filter_cell and line[4] in filter["pert_idose"]:
                ft = ','.join(line[1:5])
                lb = [float(i) for i in line[5:]]
                if ft in data.keys():
                    data[ft].append(lb)
                else:
                    data[ft] = [lb]
    for ft, lb in sorted(data.items()):
        ft = ft.split(',')
        feature.append(ft)
        pert_id.append(ft[0])
        if len(lb) == 1:
            label.append(lb[0])
        else:
            lb = choose_mean_example(lb)
            label.append(lb)
    return np.asarray(label, dtype=np.float64)


if __name__ == '__main__':
    filter = {"time": "24H", "pert_id": ['BRD-U41416256', 'BRD-U60236422'], "pert_type": ["trt_cp"],
              "cell_id": ["MCF7", "A375", "HT29", "PC3", "HA1E", "YAPC", "HELA"],
              "pert_idose": ["0.04 um", "0.12 um", "0.37 um", "1.11 um", "3.33 um", "10.0 um"]}
    attention = np.load('output/attention.npy')
    head0 = attention[0][0]
    head1 = attention[0][1]
    head2 = attention[0][2]
    head3 = attention[0][3]
    dev_pert_id, test_pert_id = get_id_test('data/pert_id_eval.txt')
    data = read_data('data/signature_0_7.csv', filter, dev_pert_id, test_pert_id)
    data = np.transpose(data)
    data = 1 - distance.cdist(data, data, 'correlation')
    neg, pos = precision_k(data, head0+head1+head2+head3, 100)
    print(neg, pos)
    print(np.max(np.sort(data)[:,-2]))