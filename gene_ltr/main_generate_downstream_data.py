import os
os.environ["CUDA_VISIBLE_DEVICES"] = "0"
import sys
sys.path.append(os.path.dirname(os.path.realpath(__file__)) + '/models')
sys.path.append(os.path.dirname(os.path.realpath(__file__)) + '/utils')
import nfp_drug_gene_attention_model_regression
import torch
import numpy as np
from molecules import Molecules
from sklearn.model_selection import KFold


def read_score(input_file):
    score = dict()
    with open(input_file, 'r') as f:
        for line in f:
            line = line.strip().split('\t')
            score[line[0]] = float(line[1])
    return score


def generate_lq_drug(input_file, output_file, score):
    hq_drug = []
    lq_drug = []
    drug = dict()
    with open(input_file, 'r') as f:
        header = f.readline()  # skip header
        for line in f:
            line = line.strip().split(',')
            assert len(line) == 983, "Wrong format"
            if filter["time"] in line[0] and line[1] not in filter["pert_id"] and line[2] in filter["pert_type"] \
                    and line[3] in filter["cell_id"] and line[4] in filter["pert_idose"]:
                if score[line[0]] > 0.7:
                    hq_drug.append(line[1])
                else:
                    lq_drug.append(line[1])
                if line[1] in drug:
                    drug[line[1]].append(line[3])
                else:
                    drug[line[1]] = [line[3]]
        hq_drug = set(hq_drug)
        lq_drug = set(lq_drug)
        lq_drug = list(lq_drug - lq_drug.intersection(hq_drug))
        output = []
        for d in lq_drug:
            if len(set(drug[d])) == 7:
                output.append(d)
    with open(output_file, 'w') as f:
        f.write(','.join(output))


def read_lq_drug(input_file):
    with open(input_file, 'r') as f:
        drug = f.readline().strip().split(',')
    return drug


def generate_original_data(input_file, lq_drug, output_file_list):
    output_file_list = [open(i, 'w') for i in output_file_list]
    data = [dict() for i in range(len(output_file_list))]
    with open(input_file, 'r') as f:
        header = f.readline()  # skip header
        for line in f:
            line = line.strip().split(',')
            assert len(line) == 983, "Wrong format"
            if filter["time"] in line[0] and line[1] not in filter["pert_id"] and line[2] in filter["pert_type"] \
                    and line[3] in filter["cell_id"] and line[4] in filter["pert_idose"]:
                if line[1] in lq_drug:
                    idx = filter["cell_id"].index(line[3])
                    data[idx][line[1]] = ','.join(line[5:])
                    # output_file_list[idx].write(','.join(line[5:]))
    for d in lq_drug:
        for i, f in enumerate(output_file_list):
            f.write(data[i][d] + '\n')
    for f in output_file_list:
        f.close()


def load_pre_trained_model(filter, pretrained_file, device):
    drug_input_dim = {'atom': 62, 'bond': 6}
    drug_embed_dim = 128
    conv_size = [16, 16]
    degree = [0, 1, 2, 3, 4, 5]
    gene_embed_dim = 128
    pert_type_emb_dim = 4
    cell_id_emb_dim = 4
    pert_idose_emb_dim = 4
    hid_dim = 128
    dropout = 0.1
    loss_type = 'point_wise_mse'
    intitializer = torch.nn.init.xavier_uniform_
    model = nfp_drug_gene_attention_model_regression.\
        NFPDrugGeneAttentionModelRegression(drug_input_dim=drug_input_dim,
                                            drug_emb_dim=drug_embed_dim,
                                            conv_size=conv_size, degree=degree,
                                            gene_input_dim=128, gene_emb_dim=gene_embed_dim,
                                            num_gene=978, hid_dim=hid_dim, dropout=dropout,
                                            loss_type=loss_type, device=device, initializer=intitializer,
                                            pert_type_input_dim=len(filter['pert_type']),
                                            cell_id_input_dim=7,
                                            pert_idose_input_dim=6,
                                            pert_type_emb_dim=pert_type_emb_dim,
                                            cell_id_emb_dim=cell_id_emb_dim,
                                            pert_idose_emb_dim=pert_idose_emb_dim,
                                            use_pert_type=False,
                                            use_cell_id=True, use_pert_idose=True)
    model.load_state_dict(torch.load(pretrained_file))
    model = model.to(device)
    model = model.double()
    model.eval()
    return model


def read_drug_string(input_file):
    with open(input_file, 'r') as f:
        drug = dict()
        for line in f:
            line = line.strip().split(',')
            assert len(line) == 2, "Wrong format"
            drug[line[0]] = line[1]
    return drug


def convert_smile_to_feature(smiles, device):
    molecules = Molecules(smiles)
    node_repr = torch.FloatTensor([node.data for node in molecules.get_node_list('atom')]).to(device).double()
    edge_repr = torch.FloatTensor([node.data for node in molecules.get_node_list('bond')]).to(device).double()
    return {'molecules': molecules, 'atom': node_repr, 'bond': edge_repr}


def transfrom_to_tensor(feature, drug, fp_type, device):
    drug_feature = []
    cell_id_set = ['HA1E', 'HT29', 'MCF7', 'YAPC', 'HELA', 'PC3', 'A375']
    pert_idose_set = ['1.11 um', '0.37 um', '10.0 um', '0.04 um', '3.33 um', '0.12 um']
    cell_id_dict = dict(zip(cell_id_set, list(range(len(cell_id_set)))))
    cell_id_feature = []
    pert_idose_dict = dict(zip(pert_idose_set, list(range(len(pert_idose_set)))))
    pert_idose_feature = []
    for i, ft in enumerate(feature):
        if fp_type in ['pubchem', 'circular', 'random', 'drug-target']:
            drug_fp = drug[ft[0]]
            drug_fp = np.asarray(drug_fp, dtype=np.float64)
        elif fp_type == 'neural':
            drug_fp = drug[ft[0]]
        else:
            raise ValueError("Unknown fingerprint: %s" % fp_type)
        drug_feature.append(drug_fp)
        cell_id_ft = np.zeros(len(cell_id_set))
        cell_id_ft[cell_id_dict[ft[2]]] = 1
        cell_id_feature.append(np.array(cell_id_ft, dtype=np.float64))
        pert_idose_ft = np.zeros(len(pert_idose_set))
        pert_idose_ft[pert_idose_dict[ft[3]]] = 1
        pert_idose_feature.append(np.array(pert_idose_ft, dtype=np.float64))
    tensor_feature = dict()
    if fp_type in ['pubchem', 'circular', 'random', 'drug-target']:
        tensor_feature['drug'] = torch.from_numpy(np.asarray(drug_feature, dtype=np.float64)).to(device)
    elif fp_type == 'neural':
        tensor_feature['drug'] = np.asarray(drug_feature)
    else:
        raise ValueError('Unknown fingerprint: %s' % fp_type)
    tensor_feature['cell_id'] = torch.from_numpy(np.asarray(cell_id_feature, dtype=np.float64)).to(device)
    tensor_feature['pert_idose'] = torch.from_numpy(np.asarray(pert_idose_feature, dtype=np.float64)).to(device)
    return tensor_feature


def create_mask_feature(data, device):
    batch_idx = data['molecules'].get_neighbor_idx_by_batch('atom')
    molecule_length = [len(idx) for idx in batch_idx]
    mask = torch.zeros(len(batch_idx), max(molecule_length)).to(device).double()
    for idx, length in enumerate(molecule_length):
        mask[idx][:length] = 1
    return mask


def read_gene(input_file, device):
    with open(input_file, 'r') as f:
        gene = []
        for line in f:
            line = line.strip().split(',')
            assert len(line) == 129, "Wrong format"
            gene.append([float(i) for i in line[1:]])
    return torch.from_numpy(np.asarray(gene, dtype=np.float64)).to(device)


def generate_predicted_data(lq_drug, drug_smile, gene, model, output_file_list, filter, device):
    output_file_list = [open(i, 'w') for i in output_file_list]
    batch_size = 32
    cell_id = filter['cell_id']
    pert_idose = filter['pert_idose'][0]
    for i, cell in enumerate(cell_id):
        feature = [[d, '_', cell, pert_idose] for d in lq_drug]
        tensor_feature = transfrom_to_tensor(feature, drug_smile, 'neural', device)
        num_data = len(tensor_feature['drug'])
        predict_np = np.empty([0, 978])
        with torch.no_grad():
            for idx in range(0, num_data, batch_size):
                excerpt = slice(idx, idx + batch_size)
                input_drug = convert_smile_to_feature(tensor_feature['drug'][excerpt], device)
                input_mask = create_mask_feature(input_drug, device)
                predict = model(input_drug, gene, input_mask, None, None,
                                tensor_feature['cell_id'][excerpt], tensor_feature['pert_idose'][excerpt])
                predict_np = np.concatenate((predict_np, predict.cpu().numpy()), axis=0)
        for vec in predict_np:
            output_file_list[i].write(','.join([str(j) for j in vec]) + '\n')
        output_file_list[i].close()


def generate_test_data(input_file, lq_drug, atc_label, target_label, output_file):
    data = dict()
    with open(input_file, 'r') as f:
        f.readline()
        for line in f:
            line = line.strip('\n').split('\t')
            data[line[0]] = {'target': line[2].split(','), 'atc': line[4].split(',')}
    with open(output_file, 'w') as f:
        for drug in lq_drug:
            label = []
            for atc in atc_label:
                if atc in data[drug]['atc']:
                    label.append('1')
                else:
                    label.append('0')
            for target in target_label:
                if target in data[drug]['target']:
                    label.append('1')
                else:
                    label.append('0')
            f.write(','.join(label) + '\n')


def check_label(input_file, lq_drug, atc_label, drug_target_label):
    data = dict()
    with open(input_file, 'r') as f:
        f.readline()
        for line in f:
            line = line.strip('\n').split('\t')
            data[line[0]] = {'target': line[2].split(','), 'atc': line[4].split(',')}
    atc_data = []
    for drug in lq_drug:
        label = []
        for atc in atc_label:
            if atc in data[drug]['atc']:
                label.append(1)
            else:
                label.append(0)
        atc_data.append(label)
    atc_data = np.array(atc_data)
    tmp = np.sum(atc_data, axis=0)
    print(tmp)
    tmp = np.where(tmp > 0, 1, 0)
    print(np.sum(tmp))

    drug_target_data = []
    for drug in lq_drug:
        label = []
        for drug_target in drug_target_label:
            if drug_target in data[drug]['target']:
                label.append(1)
            else:
                label.append(0)
        drug_target_data.append(label)
    drug_target_data = np.array(drug_target_data)
    tmp = np.sum(drug_target_data, axis=0)
    print(tmp)
    tmp = np.where(tmp > 0, 1, 0)
    print(np.sum(tmp))

    atc_ignore_list = []
    drug_target_ignore_list = []
    kf = KFold(n_splits=5, shuffle=True, random_state=10)
    for train_index, test_index in kf.split(atc_data):
        atc_dt = atc_data[test_index]
        drug_target_dt = drug_target_data[test_index]
        for i in range(np.shape(atc_dt)[1]):
            if sum(atc_dt[:, i]) < 5:
                atc_ignore_list.append(atc_label[i])
        for i in range(np.shape(drug_target_dt)[1]):
            if sum(drug_target_dt[:, i]) < 5:
                drug_target_ignore_list.append(drug_target_label[i])
    print(set(atc_label) - set(atc_ignore_list))
    print(set(drug_target_label) - set(drug_target_ignore_list))


def get_target_label(input_file):
    label = []
    with open(input_file) as f:
        for line in f:
            line = line.strip().split('\t')
            label.append(line[0])
    return label


if __name__ == '__main__':
    if torch.cuda.is_available():
        device = torch.device("cuda")
    else:
        device = torch.device("cpu")
    filter = {"time": "24H", "pert_id": ['BRD-U41416256', 'BRD-U60236422'], "pert_type": ["trt_cp"],
              "cell_id": ["MCF7", "A375", "HT29", "PC3", "HA1E", "YAPC", "HELA"],
              "pert_idose": ["10.0 um"]}
    original_file_list = ['data/down_stream_data/original_mcf7.csv', 'data/down_stream_data/original_a375.csv',
                          'data/down_stream_data/original_ht29.csv', 'data/down_stream_data/original_pc3.csv',
                          'data/down_stream_data/original_ha1e.csv', 'data/down_stream_data/original_yapc.csv',
                          'data/down_stream_data/original_hela.csv']
    predicted_file_list = ['data/down_stream_data/predicted_mcf7.csv', 'data/down_stream_data/predicted_a375.csv',
                          'data/down_stream_data/predicted_ht29.csv', 'data/down_stream_data/predicted_pc3.csv',
                          'data/down_stream_data/predicted_ha1e.csv', 'data/down_stream_data/predicted_yapc.csv',
                          'data/down_stream_data/predicted_hela.csv']
    pretrained_file = 'saved_model/nfp_drug_gene_attention_model/epoch_89.pt'
    atc_label = ['N', 'C', 'A', 'J', 'S', 'L', 'D', 'R', 'G', 'M']
    atc_label_full = ['N', 'C', 'L', 'A', 'J', 'S', 'D', 'G', 'R', 'M', 'P', 'B', 'H', 'V']
    # target_label = ['5-hydroxytryptamine receptor 2A', '5-hydroxytryptamine receptor 1A',
    #                 'Muscarinic acetylcholine receptor M2', 'Muscarinic acetylcholine receptor M1',
    #                 'Dopamine D2 receptor', 'Histamine H1 receptor', 'Alpha-1A adrenergic receptor',
    #                 'Alpha-2A adrenergic receptor']
    target_label = ['Histamine H1 receptor', 'Dopamine D2 receptor', '5-hydroxytryptamine receptor 2A',
                    'Alpha-1A adrenergic receptor']
    target_label_full = get_target_label('data/drug_target_label.txt')
    # score = read_score('data/pearson_score.txt')
    # generate_lq_drug('data/signature_all.csv', 'data/pert_id_lq.txt', score)
    drug = read_lq_drug('data/pert_id_lq.txt')
    # drug_smile = read_drug_string('data/drugs_smiles.csv')
    # gene = read_gene('data/gene_vector.csv', device)
    # model = load_pre_trained_model(filter, pretrained_file, device)
    # generate_original_data('data/signature_all.csv', drug, original_file_list)
    # generate_predicted_data(drug, drug_smile, gene, model, predicted_file_list, filter, device)
    generate_test_data('data/down_stream_data.csv', drug, atc_label, target_label, 'data/down_stream_data/label_new_1.csv')
    # check_label('data/down_stream_data.csv', drug, atc_label_full, target_label_full)
