from sklearn.model_selection import KFold
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import roc_auc_score
import numpy as np


def read_feature(input_file):
    feature = []
    with open(input_file, 'r') as f:
        for line in f:
            line = line.strip().split(',')
            line = [float(i) for i in line]
            feature.append(line)
    return np.array(feature)


def read_label(input_file):
    label = []
    with open(input_file, 'r') as f:
        for line in f:
            line = line.strip().split(',')
            line = [int(i) for i in line]
            label.append(line)
    return np.array(label)


if __name__ == '__main__':
    label = read_label('data/down_stream_data/label_refine.csv')
    file_list = ['data/down_stream_data/original_a375_refine.csv', 'data/down_stream_data/original_ha1e_refine.csv',
                 'data/down_stream_data/original_hela_refine.csv', 'data/down_stream_data/original_ht29_refine.csv',
                 'data/down_stream_data/original_mcf7_refine.csv', 'data/down_stream_data/original_pc3_refine.csv',
                 'data/down_stream_data/original_yapc_refine.csv', 'data/down_stream_data/predicted_a375_refine.csv',
                 'data/down_stream_data/predicted_ha1e_refine.csv', 'data/down_stream_data/predicted_hela_refine.csv',
                 'data/down_stream_data/predicted_ht29_refine.csv', 'data/down_stream_data/predicted_mcf7_refine.csv',
                 'data/down_stream_data/predicted_pc3_refine.csv', 'data/down_stream_data/predicted_yapc_refine.csv']
    lr = LogisticRegression(random_state=0, max_iter=10000)
    svm = SVC(kernel='linear', random_state=0)
    knn = KNeighborsClassifier(n_neighbors=15)
    dt = DecisionTreeClassifier(random_state=0)
    kf = KFold(n_splits=5, shuffle=True, random_state=10)
    with open('output/down_stream_output_refine.csv', 'w') as f:
        for file in file_list:
            print(file)
            feature = read_feature(file)
            auc_list = []
            for i in range(np.shape(label)[1]):
                for train_index, test_index in kf.split(feature):
                    feature_train = feature[train_index]
                    feature_test = feature[test_index]
                    label_train = label[:, i][train_index]
                    label_test = label[:, i][test_index]

                    lr.fit(feature_train, label_train)
                    score = lr.decision_function(feature_test)
                    auc = roc_auc_score(label_test, score)
                    auc_list.append(auc)

                    knn.fit(feature_train, label_train)
                    score = knn.predict_proba(feature_test)[:, 1]
                    auc = roc_auc_score(label_test, score)
                    auc_list.append(auc)

                    svm.fit(feature_train, label_train)
                    score = svm.decision_function(feature_test)
                    auc = roc_auc_score(label_test, score)
                    auc_list.append(auc)

                    dt.fit(feature_train, label_train)
                    score = dt.predict_proba(feature_test)[:, 1]
                    auc = roc_auc_score(label_test, score)
                    auc_list.append(auc)

            f.write(','.join([str(i) for i in auc_list]) + '\n')
