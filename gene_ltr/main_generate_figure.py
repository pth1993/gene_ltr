import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
import collections
import pandas as pd
from rdkit import Chem
from rdkit.Chem import AllChem
from rdkit.Chem import Draw


def generate_pearson_distribution(input_file):
    score = []
    with open(input_file) as f:
        for line in f:
            line = line.strip().split('\t')
            if float(line[1]) != 0.0:
                score.append(float(line[1]))
    sns.set(color_codes=True)
    fig, ax = plt.subplots(figsize=(7, 4))
    sns.distplot(score, hist=False, kde_kws={"shade": True}, color="b")
    ax.set_xlabel('Pearson Correlation Coefficient')
    ax.set_ylabel('Density')
    plt.show()
    fig.savefig('fig/sup_fig_1a_data_density_function.pdf')

    fig, ax = plt.subplots(figsize=(7, 4))
    sns.kdeplot(score, cumulative=True, shade=True, color="b")
    ax.set_xlabel('Pearson Correlation Coefficient')
    ax.set_ylabel('Cumulative Distribution')
    plt.show()
    fig.savefig('fig/sup_fig_1b_data_cummulative_distribution.pdf')


def generate_heatmap_drug_cell_combination(score_file, info_file):
    info = dict()
    drug_list = []
    cell_list = []
    with open(info_file) as f:
        f.readline()
        for line in f:
            line = line.strip().split('\t')
            if line[1] != 'DMSO':
                info[line[0]] = [line[1]] + line[4:7]
                drug_list.append(line[1])
                cell_list.append(line[4])
        drug_list = list(set(drug_list))
        cell_list = list(set(cell_list))
    data = np.zeros((len(drug_list), len(cell_list)))
    with open(score_file) as f:
        for line in f:
            line = line.strip().split('\t')
            score = float(line[1])
            [drug, cell, pert_idose, time_stamp] = info[line[0]]
            if pert_idose == '10.0 um' and time_stamp == '24 h':
                i = drug_list.index(drug)
                j = cell_list.index(cell)
                if score < 0.7:
                    data[i, j] = 2
                else:
                    data[i, j] = 1
    f, ax = plt.subplots(figsize=(5, 4))
    sns.heatmap(data, xticklabels=False, yticklabels=False, cmap=['w', 'r', 'g'], cbar=False)
    ax.set_xlabel('Cell')
    ax.set_ylabel('Drug')
    # plt.rc('xtick', labelsize=30)  # fontsize of the tick labels
    # plt.rc('ytick', labelsize=30)
    # plt.rcParams.update({'label.size': 20})
    plt.show()


def generate_barplot_chemical_cell_combination(score_file, info_file):
    info = dict()
    drug_list = []
    cell_list = []
    with open(info_file) as f:
        f.readline()
        for line in f:
            line = line.strip().split('\t')
            if line[1] != 'DMSO' and '.311' not in line[4] and '.101' not in line[4]:
                info[line[0]] = [line[1]] + line[4:7]
                cell_list.append(line[4])
            drug_list.append(line[1])
    cell_list = sorted(list(set(cell_list)))
    reliable_score = dict(zip(cell_list, [0] * len(cell_list)))
    unreliable_score = dict(zip(cell_list, [0] * len(cell_list)))
    num_drug = len(set(drug_list))
    with open(score_file) as f:
        for line in f:
            line = line.strip().split('\t')
            score = float(line[1])
            if score != 0.0:
                [drug, cell, pert_idose, time_stamp] = info[line[0]]
                if pert_idose == '10.0 um' and time_stamp == '24 h':
                    if score < 0.7:
                        unreliable_score[cell] += 1
                    else:
                        reliable_score[cell] += 1
    no_score = {}
    for cell in reliable_score:
        no_score[cell] = num_drug - reliable_score[cell] - unreliable_score[cell]
    data = []
    for cell in cell_list:
        data.append([cell, reliable_score[cell], unreliable_score[cell] + reliable_score[cell], num_drug])
    df = pd.DataFrame(data=data, columns=['Cell', 'Reliable exp.', 'Unreliable exp.', 'No exp.'])
    print(df)
    sns.set(style="whitegrid")
    fig, ax = plt.subplots(figsize=(7, 5))
    sns.set_color_codes("muted")
    sns.barplot(x='No exp.', y='Cell', data=df, label="No experiment", color="y")
    sns.barplot(x='Unreliable exp.', y='Cell', data=df, label="Unreliable experiment", color="r")
    sns.barplot(x='Reliable exp.', y='Cell', data=df, label="Reliable experiment", color="g")
    ax.legend(ncol=2, loc="lower right", frameon=True)
    # ax.set(xlim=(0, 24), ylabel="",
    #        xlabel="Automobile collisions per billion miles")
    ax.set(xlim=(0, 2200))
    ax.set_xlabel('Number of Experiment')
    ax.set_ylabel('Cell')
    sns.despine(left=True, bottom=True)
    plt.show()
    fig.savefig('fig/sup_fig_1c_experiment_per_cell.pdf')


def get_statistics(data_file, filter, pert_id_eval=None):
    feature = []
    if pert_id_eval:
        with open(pert_id_eval, 'r') as f:
            pert_id_eval = []
            for line in f:
                pert_id_eval += line.strip().split(',')
    else:
        pert_id_eval = []
    pert_id_eval += filter["pert_id"]

    with open(data_file, 'r') as f:
        f.readline()  # skip header
        for line in f:
            line = line.strip().split(',')
            assert len(line) == 983, "Wrong format"
            if filter["time"] in line[0] and line[1] not in pert_id_eval and line[2] in filter["pert_type"] \
                    and line[3] in filter["cell_id"] and line[4] in filter["pert_idose"]:
                ft = line[1:5]
                if ft not in feature:
                    feature.append(ft)
    feature = np.array(feature)
    pert_id = feature[:, 0]
    cell_id = feature[:, 2]
    pert_idose = feature[:, 3]
    print(len(set(pert_id)))
    counter = collections.Counter(cell_id)
    print(counter)
    counter = collections.Counter(pert_idose)
    print(counter)


def generate_noise_effect_graph():
    data = {'index': [-1, -0.5, -0.1, 0.1, 0.3, 0.5, 0.7, -1, -0.5, -0.1, 0.1, 0.3, 0.5, 0.7],
            'value': [0.1101, 0.1353, 0.0907, 0.0874, 0.2313, 0.3491, 0.3923, 0.0844, 0.0966, 0.0689, 0.0810, 0.2702,
                      0.3723, 0.3903],
            'Models': ['vanilla neural network', 'vanilla neural network', 'vanilla neural network',
                       'vanilla neural network', 'vanilla neural network', 'vanilla neural network',
                       'vanilla neural network',
                       'kNN', 'kNN', 'kNN', 'kNN', 'kNN', 'kNN', 'kNN']}
    df = pd.DataFrame(data=data)
    fig, ax = plt.subplots(figsize=(7, 4))
    sns.lineplot(x="index", y="value", hue='Models', style='Models', data=df, markers=True, dashes=False)
    ax.set_xlabel('APC threshold')
    ax.set_ylabel('Pearson correlation')
    plt.show()
    fig.savefig('fig/fig_4_noisy_data_effect.pdf')


def generate_down_stream_graph(input_file):
    data = []
    with open(input_file, 'r') as f:
        for line in f:
            line = line.strip().split(',')
            data.append([float(i) for i in line])
    data = np.array(data)
    data_tmp = np.reshape(data, (14, 14, 5, 4))
    data_tmp = np.transpose(data_tmp, (0, 1, 3, 2))
    data_tmp = np.mean(data_tmp, axis=-1)
    original_data = data_tmp[:7]
    print(np.mean(original_data, axis=(1, 2)))
    print(np.mean(original_data, axis=(0, 2)))
    print(np.mean(original_data, axis=(0, 1)))
    predicted_data = data_tmp[7:]
    print(np.mean(predicted_data, axis=(1, 2)))
    print(np.mean(predicted_data, axis=(0, 2)))
    print(np.mean(predicted_data, axis=(0, 1)))
    print(np.mean(predicted_data - original_data))
    auc = np.empty(0)
    for i in range(7):
        auc = np.concatenate([auc, data[i+7] - data[i]])
    auc = np.reshape(auc, (7, 14, 5, 4))
    auc = np.transpose(auc, (0, 1, 3, 2))
    auc = np.mean(auc, axis=-1)
    auc_atc = auc[:, :10, :]
    auc_drug_target = auc[:, 10:, :]
    check = np.where(auc < 0, 0, 1)
    auc = np.reshape(auc, 392)
    auc_atc = np.reshape(auc_atc, 280)
    auc_drug_target = np.reshape(auc_drug_target, 112)
    print(np.sum(check))

    # data = {'Cells': np.repeat(['A375', 'HA1E', 'HELA', 'HT29', 'MCF7', 'PC3', 'YAPC'], 30),
    #         'AUC predicted - AUC original': auc, 'ATC codes and targets':
    #             np.tile(np.repeat(['N', 'C', 'A', 'J', 'S', 'HRH1', 'HTR2A', 'ADRA1A', 'DNA', 'HTR2C'], 3), 7),
    #         'Models': np.tile(['LR', 'kNN', 'SVM'], 70)}
    # data = {'Cells': np.repeat(['A375', 'HA1E', 'HELA', 'HT29', 'MCF7', 'PC3', 'YAPC'], 54),
    #         'AUC predicted - AUC original': auc, 'ATC codes and targets':
    #             np.tile(np.repeat(['J', 'R', 'N', 'C', 'D', 'A', 'L', 'M', 'G', 'S', '5-hydroxytryptamine receptor 2A',
    #                                '5-hydroxytryptamine receptor 1A', 'Muscarinic acetylcholine receptor M2',
    #                                'Muscarinic acetylcholine receptor M1', 'Dopamine D2 receptor',
    #                                'Histamine H1 receptor', 'Alpha-1A adrenergic receptor',
    #                                'Alpha-2A adrenergic receptor'], 3), 7),
    #         'Models': np.tile(['LR', 'kNN', 'SVM'], 126)}
    data = {'Cells': np.repeat(['A375', 'HA1E', 'HELA', 'HT29', 'MCF7', 'PC3', 'YAPC'], 56),
            'AUC predicted - AUC original': auc, 'ATC codes and targets':
                np.tile(np.repeat(['N', 'C', 'A', 'J', 'S', 'L', 'D', 'R', 'G', 'M', 'HRH1', 'DRD2', 'HTR2A',
                                   'ADRA1A'], 4), 7),
            'Models': np.tile(['LR', 'kNN', 'SVM', 'DT'], 98)}
    data_atc = {'Cells': np.repeat(['A375', 'HA1E', 'HELA', 'HT29', 'MCF7', 'PC3', 'YAPC'], 40),
            'AUC predicted - AUC original': auc_atc, 'ATC codes':
                np.tile(np.repeat(['N', 'C', 'A', 'J', 'S', 'L', 'D', 'R', 'G', 'M'], 4), 7),
            'Models': np.tile(['LR', 'kNN', 'SVM', 'DT'], 70)}
    data_drug_target = {'Cells': np.repeat(['A375', 'HA1E', 'HELA', 'HT29', 'MCF7', 'PC3', 'YAPC'], 16),
            'AUC predicted - AUC original': auc_drug_target, 'Drug-targets':
                np.tile(np.repeat(['HRH1', 'DRD2', 'HTR2A', 'ADRA1A'], 4), 7),
            'Models': np.tile(['LR', 'kNN', 'SVM', 'DT'], 28)}
    df = pd.DataFrame(data=data)
    df_atc = pd.DataFrame(data=data_atc)
    df_drug_target = pd.DataFrame(data=data_drug_target)
    sns.set(font_scale=1.4)
    sns.set_style('whitegrid')
    fig, ax = plt.subplots(figsize=(7, 4))
    sns.boxplot(x="Cells", y="AUC predicted - AUC original", data=df)
    plt.show()
    fig.savefig('fig/fig_5a_downstream_per_cell.pdf')
    # fig, ax = plt.subplots(figsize=(7, 4))
    # sns.boxplot(x="ATC codes and targets", y="AUC predicted - AUC original",
    #             data=df)
    # plt.show()
    fig, ax = plt.subplots(figsize=(7, 4))
    sns.boxplot(x="Models", y="AUC predicted - AUC original", data=df)
    plt.show()
    fig.savefig('fig/fig_5b_downstream_per_model.pdf')
    fig, ax = plt.subplots(figsize=(7, 4))
    sns.boxplot(x="ATC codes", y="AUC predicted - AUC original", data=df_atc)
    plt.show()
    fig.savefig('fig/fig_5c_downstream_per_atc_code.pdf')
    fig, ax = plt.subplots(figsize=(7, 4))
    sns.boxplot(x="Drug-targets", y="AUC predicted - AUC original", data=df_drug_target)
    plt.show()
    fig.savefig('fig/fig_5d_downstream_per_drug_target.pdf')


def read_data_test(input_file, filter):
    feature = []
    data = dict()
    with open(input_file, 'r') as f:
        f.readline()  # skip header
        for line in f:
            line = line.strip().split(',')
            assert len(line) == 983, "Wrong format"
            if filter["time"] in line[0] and line[1] not in filter["pert_id"] and line[2] in filter["pert_type"] \
                    and line[3] in filter["cell_id"] and line[4] in filter["pert_idose"]:
                ft = ','.join(line[1:5])
                lb = [float(i) for i in line[5:]]
                if ft in data.keys():
                    data[ft].append(lb)
                else:
                    data[ft] = [lb]
    for ft, lb in sorted(data.items()):
        ft = ft.split(',')
        feature.append(ft[2:])
    return np.asarray(feature)


def generate_result_specific(input_file, signature_file, filter):
    score = []
    with open(input_file, 'r') as f:
        for line in f:
            score.append(line)
    score = [float(i) for i in score[-2].strip().split(',')]
    feature = read_data_test(signature_file, filter)
    data = {'Cell': feature[:, 0], 'Chemical dose': feature[:, 1], 'Pearson Correlation': score}
    df = pd.DataFrame(data)
    sns.set(font_scale=1.2)
    sns.set_style('whitegrid')
    fig, ax = plt.subplots(figsize=(7, 4))
    sns.barplot(x="Cell", y="Pearson Correlation", data=df)
    plt.show()
    fig.savefig('fig/sup_fig_2a_deepce_per_cell.pdf')
    fig, ax = plt.subplots(figsize=(7, 4))
    sns.barplot(x="Chemical dose", y="Pearson Correlation", data=df)
    plt.show()
    fig.savefig('fig/sup_fig_2b_deepce_per_dose.pdf')


def draw_mol_from_smiles():
    smiles_list = ['COC1=C(Br)C2=C(C=C1)C(O[C@@H]1C[C@H](N(C1)C(=O)[C@@H](NC(=O)OC1CCCC1)C(C)(C)C)C(=O)N[C@@]1(C[C@H]1C=C)C(O)=O)=CC(=N2)C1=CSC(NC(=O)C(C)C)=N1',
                   'CC[C@@H]1NC(=O)[C@H]([C@H](O)[C@H](C)C\C=C\C)N(C)C(=O)[C@H](C(C)C)N(C)C(=O)[C@H](CC(C)C)N(C)C(=O)[C@H](CC(C)C)N(C)C(=O)[C@@H](C)NC(=O)[C@H](C)NC(=O)[C@H](CC(C)C)N(C)C(=O)[C@@H](NC(=O)[C@H](C(C)C)N(CC)C(=O)[C@@H](C)N(C)C1=O)C(C)C',
                   'CC[C@H](C)[C@@H]1N(C)C(=O)CN(C)C(=O)[C@H](CC)NC(=O)[C@H]([C@H](O)[C@H](C)C\C=C\C)N(C)C(=O)[C@H](C(C)C)N(C)C(=O)[C@H](CC(C)C)N(C)C(=O)[C@H](CC(C)C)N(C)C(=O)[C@@H](C)NC(=O)[C@H](C)NC(=O)[C@H](CC(C)C)N(C)C(=O)[C@@H](NC1=O)C(C)C',
                   '[H][C@@]1(NC(=O)C(=N/O)\C2=NSC(N)=N2)C(=O)N2C(C(O)=O)=C(CS[C@]12[H])\C=C1/CCN(C1=O)[C@]1([H])CCN(C1)C(=O)OCC1=C(C)OC(=O)O1',
                   '[H][C@]1(NC(=O)[C@@H](NC(=O)[C@@H]2C[C@@H](O)CN2C(=O)[C@@H](NC(=O)[C@H](C[C@@H](O)[C@@H](O)NC(=O)[C@@H]2[C@@H](O)[C@@H](C)CN2C1=O)NC(=O)C1=CC=C(C=C1)C1=CC=C(C=C1)C1=CC=C(OCCCCC)C=C1)[C@@H](C)O)[C@H](O)[C@@H](O)C1=CC=C(O)C=C1)[C@@H](C)O',
                   'O[C@@](CN1C=NN=N1)(C1=CC=C(F)C=C1F)C(F)(F)C1=CC=C(C=N1)C1=CC=C(OCC(F)(F)F)C=C1',
                   'CC[C@@H]1NC(=O)[C@H]([C@H](O)[C@H](C)C\C=C\C=C)N(C)C(=O)[C@H](C(C)C)N(C)C(=O)[C@H](CC(C)C)N(C)C(=O)[C@H](CC(C)C)N(C)C(=O)[C@@H](C)NC(=O)[C@H](C)NC(=O)[C@H](CC(C)C)N(C)C(=O)[C@@H](NC(=O)[C@H](CC(C)C)N(C)C(=O)CN(C)C1=O)C(C)C',
                   'CC[C@@H]1NC(=O)[C@H]([C@H](O)[C@H](C)C\C=C\C)N(C)C(=O)[C@H](C(C)C)N(C)C(=O)[C@H](CC(C)C)N(C)C(=O)[C@H](CC(C)C)N(C)C(=O)[C@@H](C)NC(=O)[C@H](C)NC(=O)[C@H](CC(C)C)N(C)C(=O)[C@@H](NC(=O)[C@H](CC(C)C)N(C)C(=O)CN(C)C1=O)C(C)C',
                   'C\C=C\C[C@@H](C)C(=O)[C@@H]1N(C)C(=O)[C@H](C(C)C)N(C)C(=O)[C@H](CC(C)C)N(C)C(=O)[C@H](CC(C)C)N(C)C(=O)[C@@H](C)NC(=O)[C@H](C)NC(=O)[C@H](CC(C)C)N(C)C(=O)[C@@H](NC(=O)[C@H](CC(C)C)N(C)C(=O)CN(C)C(=O)[C@@H](NC1=O)C(C)C)C(C)C',
                   'CN1N=NC(=N1)N(CC1=CC(=CC(=C1)C(F)(F)F)C(F)(F)F)[C@H]1CCCN(C[C@H]2CC[C@@H](CC2)C(O)=O)C2=C1C=C(C)C=C2C']
    drug_list = ['faldaprevir', 'alisporivir', 'nim811', 'ceftobiprole_medocaril', 'anidulafungin', 'oteseconazole',
                 'voclosporin', 'cyclosporine', 'valspodar', 'evacetrapib']
    sup_smiles_list = ['[H][C@]1(CCCN1C(=O)[C@@H](NC(=O)OC)C(C)C)C1=NC=C(N1)C1=CC2=C(C=C1)N1[C@@H](OC3=C(C=CC(=C3)C3=CN=C(N3)[C@]3([H])CCCN3C(=O)[C@@H](NC(=O)OC)C(C)C)C1=C2)C1=CC=CC=C1',
                       'CO[C@H](C)[C@H](NC(=O)OC)C(=O)N1CCC[C@H]1C1=NC2=C(N1)C=C(F)C(=C2)[C@H]1CC[C@@H](N1C1=CC(F)=C(N2CCC(CC2)C2=CC=C(F)C=C2)C(F)=C1)C1=CC2=C(NC(=N2)[C@@H]2CCCN2C(=O)[C@@H](NC(=O)OC)[C@@H](C)OC)C=C1F',
                       'COC[C@H]1C[C@H](N(C1)C(=O)[C@H](NC(=O)OC)C1=CC=CC=C1)C1=NC=C(N1)C1=CC=C2C(COC3=CC4=C(C=CC5=C4NC(=N5)[C@@H]4CC[C@H](C)N4C(=O)[C@@H](NC(=O)OC)C(C)C)C=C23)=C1',
                       'COC(=O)N[C@@H](C(C)C)C(=O)N1CCC[C@H]1C1=NC=C(N1)C1=CC=C2N3[C@@H](OC4=C(C3=CC2=C1)C(F)=CC(=C4)C1=CN=C(N1)[C@@H]1CCCN1C(=O)[C@@H](NC(=O)OC)C(C)C)C1=CN=C(S1)C1CC1',
                       'COC(=O)N[C@@H](C(C)C)C(=O)N1CCC[C@H]1C1=NC2=CC=C(C=C2N1)C1=CSC2=C1SC=C2C1=CC=C(C=C1)C1=CN=C(N1)[C@@H]1CCCN1C(=O)[C@H](NC(=O)OC)C1=CC=CC=C1',
                       'COC(=O)N[C@@H](C(C)C)C(=O)N1[C@@H](C[C@@H]2CCCC[C@H]12)C1=NC2=CC=C(C=C2N1)C1=CC2=CC=C1CCC1=CC=C(CC2)C(=C1)C1=CC=C2N=C(NC2=C1)[C@@H]1C[C@@H]2CCCC[C@@H]2N1C(=O)[C@@H](NC(=O)OC)C(C)C',
                       'COC(=O)N[C@@H](C(C)C)C(=O)N1CCC[C@H]1C1=NC=C(N1)C1=CC=C(C=C1)C1=CC=C(C2=CN=C(N2)[C@@H]2CCCN2C(=O)[C@@H](NC(=O)OC)C(C)C)C2=C1OCO2',
                       '[H][C@@]12COCCN1C(=O)C1=C(OCOC(=O)OC)C(=O)C=CN1N2[C@H]1C2=CC=C(F)C(F)=C2CSC2=CC=CC=C12',
                       '[H][C@@]12CC3=CC=C(OC4=C5C(CC[N+](C)(C)[C@]5([H])CC5=CC(OC6=C(OC)C=C(CC[N+]1(C)C)C2=C6)=C(OC)C=C5)=CC(OC)=C4OC)C=C3',
                       '[H][C@@]12CCCN1C(=O)[C@H](NC(=O)[C@@H](NC(=O)C1=C3N=C4C(OC3=C(C)C=C1)=C(C)C(=O)C(N)=C4C(=O)N[C@H]1[C@@H](C)OC(=O)[C@H](C(C)C)N(C)C(=O)CN(C)C(=O)[C@]3([H])CCCN3C(=O)[C@H](NC1=O)C(C)C)[C@@H](C)OC(=O)[C@H](C(C)C)N(C)C(=O)CN(C)C2=O)C(C)C',
                       'COC(=O)C1=CN=C2N1CCC1=CC=CC=C1C2=C1CCN(CCC2=CC=C(OCC3=CC=C4C=CC=CC4=N3)C=C2)CC1',
                       '[H][C@]12CC3=C(NC4=CC=CC=C34)[C@H](N1C(=O)CN(C)C2=O)C1=CC2=C(OCO2)C=C1',
                       '[H][C@]1(NC(=O)CNC(=O)C2=C(COC)SC(=N2)[C@@H](NC(=O)C2=C(C)SC(=N2)[C@H](CC(=O)NC)NC(=O)C2=CSC(=N2)C2=CC=C(N=C2C2=CSC(=N2)C2=CSC1=N2)C1=NC(=CS1)C1=N[C@@H](CO1)C(=O)N1CCC[C@H]1C(N)=O)C(C)C)[C@@H](O)C1=CC=CC=C1',
                       'O[C@@H]1[C@@H](O)[C@@H](CC2=CC=CC=C2)N(CC2=CC=CC(=C2)C(=O)NC2=NC3=C(N2)C=CC=C3)C(=O)N(CC2=CC(=CC=C2)C(=O)NC2=NC3=C(N2)C=CC=C3)[C@@H]1CC1=CC=CC=C1',
                       'O=C(N([C@@H](C1=NC2=NC=CC=C2C(N1C3=CC=C(OCC)C=C3)=O)C)CC4=CC=CN=C4)CC5=CC=C(OC(F)(F)F)C=C5']
    sup_drug_list = ['elbasvir', 'pibrentasvir', 'velpatasvir', 'ruzasvir', 'samatasvir', 'odalasvir', 'coblopasvir',
                     'baloxavir_marboxil', 'metocurine', 'dactinomycin', 'laniquidar', 'tadalafil', 'ge2270a', 'sd146',
                     'amg487']
    mol_list = [Chem.MolFromSmiles(s) for s in smiles_list]
    sup_mol_list = [Chem.MolFromSmiles(s) for s in sup_smiles_list]
    for i, m in enumerate(mol_list):
        Draw.MolToFile(m, 'fig/tab_2_drug_repurposing_' + drug_list[i] + '.pdf')
    for i, m in enumerate(sup_mol_list):
        Draw.MolToFile(m, 'fig/sup_tab_9_drug_repurposing_' + sup_drug_list[i] + '.pdf')


if __name__ == '__main__':
    filter = {"time": "24H", "pert_id": ['BRD-U41416256', 'BRD-U60236422'], "pert_type": ["trt_cp"],
              "cell_id": ["MCF7", "A375", "HT29", "PC3", "HA1E", "YAPC", "HELA"],
              "pert_idose": ["0.04 um", "0.12 um", "0.37 um", "1.11 um", "3.33 um", "10.0 um"]}
    # generate_pearson_distribution('data/pearson_score.txt')
    # # generate_heatmap_drug_cell_combination('data/pearson_score.txt', 'data/sig_info.txt')
    # generate_barplot_chemical_cell_combination('data/pearson_score.txt', 'data/sig_info.txt')
    # # get_statistics('data/signature_augmented_0_4.csv', filter, 'data/pert_id_eval.txt')
    # generate_noise_effect_graph()
    # generate_down_stream_graph('output/down_stream_output_refine.csv')
    # generate_result_specific('output/183907_nfp_drug_gene_attention_model_regression_0_1.txt', 'data/signature_test.csv', filter)
    draw_mol_from_smiles()