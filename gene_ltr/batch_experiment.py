import shlex
import subprocess
import random
import os

eid = str(random.randint(1, 1000000))

# if not (os.path.exists('output/fold_0') and os.path.exists('output/fold_1') and os.path.exists('output/fold_2')
#         and os.path.exists('output/fold_3') and os.path.exists('output/fold_4')):
#     os.makedirs('output/fold_0')
#     os.makedirs('output/fold_1')
#     os.makedirs('output/fold_2')
#     os.makedirs('output/fold_3')
#     os.makedirs('output/fold_4')
if not os.path.exists('output'):
    os.makedirs('output')
# with open('output/' + eid + '_base_model_regression_random_hq.txt', 'w') as f:
#     subprocess.check_call(shlex.split('python main_base_model_regression.py --drug_file '
#                                       '"data/drugs_random.csv" --fp_type "random" '
#                                       '--train_file "data/signature_0_7.csv" --model_name "random_hq"'), stdout=f)
# with open('output/' + eid + '_base_model_regression_pubchem_hq.txt', 'w') as f:
#     subprocess.check_call(shlex.split('python main_base_model_regression.py --drug_file '
#                                       '"data/drugs_pubchem.csv" --fp_type "pubchem" '
#                                       '--train_file "data/signature_0_7.csv" --model_name "pubchem_hq"'), stdout=f)
# with open('output/' + eid + '_base_model_regression_circular_hq.txt', 'w') as f:
#     subprocess.check_call(shlex.split('python main_base_model_regression.py --drug_file '
#                                       '"data/drugs_circular.csv" --fp_type "circular" '
#                                       '--train_file "data/signature_0_7.csv" --model_name "circular_hq"'), stdout=f)
# with open('output/' + eid + '_base_model_regression_drug_target_hq.txt', 'w') as f:
#     subprocess.check_call(shlex.split('python main_base_model_regression.py --drug_file '
#                                       '"data/drugs_target_diy.csv" --fp_type "drug-target" '
#                                       '--train_file "data/signature_0_7.csv" --model_name "drug_target_hq"'), stdout=f)
# with open('output/' + eid + '_base_model_regression_ltip_hq.txt', 'w') as f:
#     subprocess.check_call(shlex.split('python main_base_model_regression.py --drug_file '
#                                       '"data/drugs_target_ltip.csv" --fp_type "drug-target" '
#                                       '--train_file "data/signature_0_7.csv" --model_name "ltip_hq"'), stdout=f)

# with open('output/' + eid + '_base_model_regression_pubchem_augmented.txt', 'w') as f:
#     subprocess.check_call(shlex.split('python main_base_model_regression.py --drug_file '
#                                       '"data/drugs_pubchem.csv" --fp_type "pubchem" '
#                                       '--train_file "data/signature_augmented_0_4.csv" --model_name "pubchem_augmented"'), stdout=f)
# with open('output/' + eid + '_base_model_regression_circular_augmented.txt', 'w') as f:
#     subprocess.check_call(shlex.split('python main_base_model_regression.py --drug_file '
#                                       '"data/drugs_circular.csv" --fp_type "circular" '
#                                       '--train_file "data/signature_augmented_0_4.csv" --model_name "circular_augmented"'), stdout=f)
# with open('output/' + eid + '_base_model_regression_drug_target_augmented.txt', 'w') as f:
#     subprocess.check_call(shlex.split('python main_base_model_regression.py --drug_file '
#                                       '"data/drugs_target_diy.csv" --fp_type "drug-target" '
#                                       '--train_file "data/signature_augmented_0_4.csv" --model_name "drug_target_augmented"'), stdout=f)
# with open('output/' + eid + '_base_model_regression_ltip_augmented.txt', 'w') as f:
#     subprocess.check_call(shlex.split('python main_base_model_regression.py --drug_file '
#                                       '"data/drugs_target_ltip.csv" --fp_type "drug-target" '
#                                       '--train_file "data/signature_augmented_0_4.csv" --model_name "ltip_augmented"'), stdout=f)

# with open('output/' + eid + '_linear_regression_none_pubchem.txt', 'w') as f:
#     subprocess.check_call(shlex.split('python main_linear_regression.py --drug_file '
#                                       '"data/drugs_pubchem.csv" --fp_type "pubchem" '
#                                       '--regularization_type "none" --train_file "data/signature_0_7.csv" '
#                                       '--model_name "none_pubchem"'), stdout=f)
# with open('output/' + eid + '_linear_regression_none_circular.txt', 'w') as f:
#     subprocess.check_call(shlex.split('python main_linear_regression.py --drug_file '
#                                       '"data/drugs_circular.csv" --fp_type "circular" '
#                                       '--regularization_type "none" --train_file "data/signature_0_7.csv" '
#                                       '--model_name "none_circular"'), stdout=f)
# with open('output/' + eid + '_linear_regression_none_drug_target.txt', 'w') as f:
#     subprocess.check_call(shlex.split('python main_linear_regression.py --drug_file '
#                                       '"data/drugs_target_diy.csv" --fp_type "drug-target" '
#                                       '--regularization_type "none" --train_file "data/signature_0_7.csv" '
#                                       '--model_name "none_drug_target"'), stdout=f)
# with open('output/' + eid + '_linear_regression_none_ltip.txt', 'w') as f:
#     subprocess.check_call(shlex.split('python main_linear_regression.py --drug_file '
#                                       '"data/drugs_target_ltip.csv" --fp_type "drug-target" '
#                                       '--regularization_type "none" --train_file "data/signature_0_7.csv" '
#                                       '--model_name "none_ltip"'), stdout=f)
#
# with open('output/' + eid + '_linear_regression_l1_pubchem.txt', 'w') as f:
#     subprocess.check_call(shlex.split('python main_linear_regression.py --drug_file '
#                                       '"data/drugs_pubchem.csv" --fp_type "pubchem" '
#                                       '--regularization_type "l1" --train_file "data/signature_0_7.csv" '
#                                       '--model_name "l1_pubchem"'), stdout=f)
# with open('output/' + eid + '_linear_regression_l1_circular.txt', 'w') as f:
#     subprocess.check_call(shlex.split('python main_linear_regression.py --drug_file '
#                                       '"data/drugs_circular.csv" --fp_type "circular" '
#                                       '--regularization_type "l1" --train_file "data/signature_0_7.csv" '
#                                       '--model_name "l1_circular"'), stdout=f)
# with open('output/' + eid + '_linear_regression_l1_drug_target.txt', 'w') as f:
#     subprocess.check_call(shlex.split('python main_linear_regression.py --drug_file '
#                                       '"data/drugs_target_diy.csv" --fp_type "drug-target" '
#                                       '--regularization_type "l1" --train_file "data/signature_0_7.csv" '
#                                       '--model_name "l1_drug_target"'), stdout=f)
# with open('output/' + eid + '_linear_regression_l1_ltip.txt', 'w') as f:
#     subprocess.check_call(shlex.split('python main_linear_regression.py --drug_file '
#                                       '"data/drugs_target_ltip.csv" --fp_type "drug-target" '
#                                       '--regularization_type "l1" --train_file "data/signature_0_7.csv" '
#                                       '--model_name "l1_ltip"'), stdout=f)
#
# with open('output/' + eid + '_linear_regression_l2_pubchem.txt', 'w') as f:
#     subprocess.check_call(shlex.split('python main_linear_regression.py --drug_file '
#                                       '"data/drugs_pubchem.csv" --fp_type "pubchem" '
#                                       '--regularization_type "l2" --train_file "data/signature_0_7.csv" '
#                                       '--model_name "l2_pubchem"'), stdout=f)
# with open('output/' + eid + '_linear_regression_l2_circular.txt', 'w') as f:
#     subprocess.check_call(shlex.split('python main_linear_regression.py --drug_file '
#                                       '"data/drugs_circular.csv" --fp_type "circular" '
#                                       '--regularization_type "l2" --train_file "data/signature_0_7.csv" '
#                                       '--model_name "l2_circular"'), stdout=f)
# with open('output/' + eid + '_linear_regression_l2_drug_target.txt', 'w') as f:
#     subprocess.check_call(shlex.split('python main_linear_regression.py --drug_file '
#                                       '"data/drugs_target_diy.csv" --fp_type "drug-target" '
#                                       '--regularization_type "l2" --train_file "data/signature_0_7.csv" '
#                                       '--model_name "l2_drug_target"'), stdout=f)
# with open('output/' + eid + '_linear_regression_l2_ltip.txt', 'w') as f:
#     subprocess.check_call(shlex.split('python main_linear_regression.py --drug_file '
#                                       '"data/drugs_target_ltip.csv" --fp_type "drug-target" '
#                                       '--regularization_type "l2" --train_file "data/signature_0_7.csv" '
#                                       '--model_name "l2_ltip"'), stdout=f)

# with open('output/' + eid + '_nfp_no_attention_model_regression.txt', 'w') as f:
#     subprocess.check_call(shlex.split('python main_nfp_model_regression.py --drug_file '
#                                       '"data/drugs_smiles.csv" --fp_type "neural" '
#                                       '--train_file "data/signature_0_7.csv" --model_name "_no_attention"'), stdout=f)
# with open('output/' + eid + '_nfp_gene_attention_model_regression.txt', 'w') as f:
#     subprocess.check_call(shlex.split('python main_nfp_gene_attention_model_regression.py --drug_file '
#                                       '"data/drugs_smiles.csv" --fp_type "neural" --dropout 0.1 '
#                                       '--train_file "data/signature_0_7.csv" --model_name "_gene_attention"'), stdout=f)
# with open('output/' + eid + '_nfp_drug_attention_model_regression.txt', 'w') as f:
#     subprocess.check_call(shlex.split('python main_nfp_drug_attention_model_regression.py --drug_file '
#                                       '"data/drugs_smiles.csv" --fp_type "neural" --dropout 0.1 '
#                                       '--train_file "data/signature_0_7.csv" --model_name "_drug_attention"'), stdout=f)
# with open('output/' + eid + '_nfp_drug_gene_attention_model_regression.txt', 'w') as f:
#     subprocess.check_call(shlex.split('python main_nfp_drug_gene_attention_model_regression.py --drug_file '
#                                       '"data/drugs_smiles.csv" --fp_type "neural" --dropout 0.1 '
#                                       '--train_file "data/signature_0_7.csv" --model_name "_drug_gene_attention"'), stdout=f)

with open('output/' + eid + '_nfp_drug_gene_attention_model_regression_augmented.txt', 'w') as f:
    subprocess.check_call(shlex.split('python main_nfp_drug_gene_attention_model_regression.py --drug_file '
                                      '"data/drugs_smiles.csv" --fp_type "neural" --dropout 0.1 '
                                      '--train_file "data/signature_augmented_0_4.csv" '
                                      '--model_name "drug_gene_attention_augmented"'), stdout=f)

# with open('output/' + eid + '_nfp_gene_attention_model_regression_0_3_semi.txt', 'w') as f:
#     subprocess.check_call(shlex.split('python main_nfp_gene_attention_model_regression.py --drug_file '
#                                       '"data/drugs_smiles.csv" --fp_type "neural" --dropout 0.3'), stdout=f)
# with open('output/' + eid + '_nfp_gene_attention_model_regression_0_7_semi.txt', 'w') as f:
#     subprocess.check_call(shlex.split('python main_nfp_gene_attention_model_regression.py --drug_file '
#                                       '"data/drugs_smiles.csv" --fp_type "neural" --dropout 0.7'), stdout=f)
# with open('output/' + eid + '_nfp_gene_attention_model_regression_0_9_semi.txt', 'w') as f:
#     subprocess.check_call(shlex.split('python main_nfp_gene_attention_model_regression.py --drug_file '
#                                       '"data/drugs_smiles.csv" --fp_type "neural" --dropout 0.9'), stdout=f)
# with open('output/' + eid + '_nfp_model_regression_drug_target.txt', 'w') as f:
#     subprocess.check_call(shlex.split('python main_nfp_model_regression.py --drug_file '
#                                       '"data/drugs_smiles.csv" --drug_target_file '
#                                       '"data/drugs_target_700.csv" --fp_type "neural"'), stdout=f)
# with open('output/' + eid + '_nfp_model_regression_ltip.txt', 'w') as f:
#     subprocess.check_call(shlex.split('python main_nfp_model_regression.py --drug_file '
#                                       '"data/drugs_smiles.csv" --drug_target_file '
#                                       '"data/drugs_target_ltip.csv" --fp_type "neural"'), stdout=f)

