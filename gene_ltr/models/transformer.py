import torch.nn as nn


class Transformer(nn.Module):
    def __init__(self, encoder, decoder, device):
        super().__init__()
        self.encoder = encoder
        self.decoder = decoder
        self.device = device

    def forward(self, src, trg, src_mask, trg_mask):
        # src = [batch size, src len]
        # trg = [batch size, trg len]
        src_mask = src_mask.unsqueeze(1).unsqueeze(2)
        # src_mask = [batch size, 1, 1, src len]
        # trg_mask = [batch size, 1, trg len, trg len]
        enc_src = self.encoder(src, src_mask)
        # enc_src = [batch size, src len, hid dim]
        output, attention = self.decoder(trg, enc_src, trg_mask, src_mask)
        # output = [batch size, trg len, output dim]
        # attention = [batch size, n heads, trg len, src len]
        return output, attention


class TransformerD(nn.Module):
    def __init__(self, decoder, device):
        super().__init__()
        self.decoder = decoder
        self.device = device

    def forward(self, src, trg, src_mask, trg_mask):
        # src = [batch size, src len]
        # trg = [batch size, trg len]
        src_mask = src_mask.unsqueeze(1).unsqueeze(2)
        # src_mask = [batch size, 1, 1, src len]
        # trg_mask = [batch size, 1, trg len, trg len]
        # enc_src = self.encoder(src, src_mask)
        # enc_src = [batch size, src len, hid dim]
        output, attention = self.decoder(trg, src, trg_mask, src_mask)
        # output = [batch size, trg len, output dim]
        # attention = [batch size, n heads, trg len, src len]
        return output, attention