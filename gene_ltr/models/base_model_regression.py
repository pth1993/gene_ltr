import torch
import torch.nn as nn
from ltr_loss import point_wise_mse, list_wise_listnet, list_wise_listmle, pair_wise_ranknet, list_wise_rankcosine, \
    list_wise_ndcg, combine_loss, pearson


class BaseModelRegression(nn.Module):
    def __init__(self, drug_input_dim, drug_emb_dim, gene_input_dim, gene_emb_dim, num_gene, hid_dim, dropout, loss_type,
                 device, initializer=None, pert_type_input_dim=None, cell_id_input_dim=None, pert_idose_input_dim=None,
                 pert_type_emb_dim=None, cell_id_emb_dim=None, pert_idose_emb_dim=None, use_pert_type=False,
                 use_cell_id=False, use_pert_idose=False, drug_target_input_dim=None, drug_target_emb_dim=None):
        super(BaseModelRegression, self).__init__()
        self.use_pert_type = use_pert_type
        self.use_cell_id = use_cell_id
        self.use_pert_idose = use_pert_idose
        self.drug_emb_dim = drug_emb_dim
        self.gene_emb_dim = gene_emb_dim
        # self.drug_embed = nn.Embedding(drug_input_dim, drug_emb_dim, padding_idx=pad_idx)
        self.drug_embed = nn.Linear(drug_input_dim, drug_emb_dim)
        # self.gene_embed = nn.Parameter(gene_weight)
        self.gene_embed = nn.Linear(gene_input_dim, gene_emb_dim)
        self.linear_dim = self.drug_emb_dim + self.gene_emb_dim
        if drug_target_input_dim is not None:
            self.drug_target_embed = nn.Linear(drug_target_input_dim, drug_target_emb_dim)
            self.linear_dim += drug_target_emb_dim
        if self.use_pert_type:
            # self.pert_type_embed = nn.Embedding(pert_type_input_dim, pert_type_emb_dim)
            self.pert_type_embed = nn.Linear(pert_type_input_dim, pert_type_emb_dim)
            self.linear_dim += pert_type_emb_dim
        if self.use_cell_id:
            # self.cell_id_embed = nn.Embedding(cell_id_input_dim, cell_id_emb_dim)
            self.cell_id_embed = nn.Linear(cell_id_input_dim, cell_id_emb_dim)
            self.linear_dim += cell_id_emb_dim
        if self.use_pert_idose:
            # self.pert_idose_embed = nn.Embedding(pert_idose_input_dim, pert_idose_emb_dim)
            self.pert_idose_embed = nn.Linear(pert_idose_input_dim, pert_idose_emb_dim)
            self.linear_dim += pert_idose_emb_dim
        self.linear_1 = nn.Linear(self.linear_dim, hid_dim)
        self.linear_2 = nn.Linear(hid_dim, 1)
        self.dropout = nn.Dropout(dropout)
        self.relu = nn.ReLU()
        self.num_gene = num_gene
        self.loss_type = loss_type
        self.initializer = initializer
        self.device = device
        self.init_weights()

    def init_weights(self):
        if self.initializer is None:
            return
        for name, parameter in self.named_parameters():
            if parameter.dim() == 1:
                nn.init.constant_(parameter, 0.)
            else:
                self.initializer(parameter)

    def forward(self, input_drug, input_gene, input_drug_target, input_pert_type, input_cell_id, input_pert_idose):
        # input_drug = [batch * length]
        # gene_embed = [num_gene * gene_emb_dim]
        num_batch = input_drug.size()[0]
        drug_embed = self.drug_embed(input_drug)
        # # drug_embed = [batch * length * drug_emb_dim]
        # drug_embed = torch.sum(drug_embed, dim=1)
        # drug_embed = [batch * drug_emb_dim]
        if input_drug_target is not None:
            drug_target_embed = self.drug_target_embed(input_drug_target)
            drug_embed = torch.cat((drug_embed, drug_target_embed), dim=1)
        if self.use_pert_type:
            pert_type_embed = self.pert_type_embed(input_pert_type)
            # pert_type_embed = pert_type_embed.squeeze(1)
            drug_embed = torch.cat((drug_embed, pert_type_embed), dim=1)
        if self.use_cell_id:
            cell_id_embed = self.cell_id_embed(input_cell_id)
            # cell_id_embed = cell_id_embed.squeeze(1)
            drug_embed = torch.cat((drug_embed, cell_id_embed), dim=1)
        if self.use_pert_idose:
            pert_idose_embed = self.pert_idose_embed(input_pert_idose)
            # pert_idose_embed = pert_idose_embed.squeeze(1)
            drug_embed = torch.cat((drug_embed, pert_idose_embed), dim=1)
        # drug_embed = drug_embed + pert_type_embed + cell_id_embed + pert_idose_embed
        drug_embed = self.relu(drug_embed)
        # drug_embed = [batch * drug_emb_dim]
        drug_embed = drug_embed.unsqueeze(1)
        # drug_embed = [batch * 1 * drug_emb_dim]
        drug_embed = drug_embed.repeat(1, self.num_gene, 1)
        # drug_embed = [batch * num_gene * drug_emb_dim]
        gene_embed = self.gene_embed(input_gene)
        # gene_embed = [num_gene * gene_emb_dim]
        # gene_embed = self.gene_embed.unsqueeze(0)
        gene_embed = gene_embed.unsqueeze(0)
        # gene_embed = [1 * num_gene * gene_emb_dim]
        gene_embed = gene_embed.repeat(num_batch, 1, 1)
        # gene_embed = [batch * num_gene * gene_emb_dim]
        gene_embed = self.relu(gene_embed)
        embed = torch.cat((drug_embed, gene_embed), dim=2)
        # embed = [batch * num_gene * (drug_emb_dim + gene_emb_dim)]
        out = self.linear_1(embed)
        # out = [batch * num_gene * hid_dim]
        out = self.relu(out)
        # out = [batch * num_gene * hid_dim]
        out = self.linear_2(out)
        # out = [batch * num_gene * 1]
        out = out.squeeze(2)
        # out = [batch * num_gene]
        return out

    def loss(self, label, predict):
        if self.loss_type == 'point_wise_mse':
            loss = point_wise_mse(label, predict)
        elif self.loss_type == 'pair_wise_ranknet':
            loss = pair_wise_ranknet(label, predict, self.device)
        elif self.loss_type == 'list_wise_listnet':
            loss = list_wise_listnet(label, predict)
        elif self.loss_type == 'list_wise_listmle':
            loss = list_wise_listmle(label, predict, self.device)
        elif self.loss_type == 'list_wise_rankcosine':
            loss = list_wise_rankcosine(label, predict)
        elif self.loss_type == 'list_wise_ndcg':
            loss = list_wise_ndcg(label, predict)
        elif self.loss_type == 'pearson':
            loss = pearson(label, predict)
        elif self.loss_type == 'combine':
            loss = combine_loss(label, predict, self.device)
        else:
            raise ValueError('Unknown loss: %s' % self.loss_type)
        return loss
