import torch
import torch.nn as nn
from ltr_loss import point_wise_mse


class LinearRegression(nn.Module):
    def __init__(self, drug_input_dim, gene_input_dim, regularization_type=None, regularization_factor=None,
                 initializer=None, pert_type_input_dim=None, cell_id_input_dim=None, pert_idose_input_dim=None,
                 use_pert_type=False, use_cell_id=False, use_pert_idose=False):
        super(LinearRegression, self).__init__()
        self.use_pert_type = use_pert_type
        self.use_cell_id = use_cell_id
        self.use_pert_idose = use_pert_idose
        self.input_dim = drug_input_dim + gene_input_dim
        if self.use_pert_type:
            self.input_dim += pert_type_input_dim
        if self.use_cell_id:
            self.input_dim += cell_id_input_dim
        if self.use_pert_idose:
            self.input_dim += pert_idose_input_dim
        self.linear = nn.Linear(self.input_dim, 1)
        self.initializer = initializer
        self.init_weights()
        self.regularization_type = regularization_type
        self.regularization_factor = regularization_factor

    def init_weights(self):
        if self.initializer is None:
            return
        for name, parameter in self.named_parameters():
            if parameter.dim() == 1:
                nn.init.constant_(parameter, 0.)
            else:
                self.initializer(parameter)

    def forward(self, input_drug, input_gene, input_pert_type, input_cell_id, input_pert_idose):
        # input_drug = [batch * drug_input_dim]
        # input_gene = [num_gene * gene_input_dim]
        # input_pert_type = [batch * pert_type_input_dim]
        # input_cell_id = [batch * cell_id_input_dim]
        # input_pert_idose = [batch * pert_idose_input_dim]
        num_batch = input_drug.size()[0]
        num_gene = input_gene.size()[0]
        if self.use_pert_type:
            input_drug = torch.cat((input_drug, input_pert_type), dim=1)
        if self.use_cell_id:
            input_drug = torch.cat((input_drug, input_cell_id), dim=1)
        if self.use_pert_idose:
            input_drug = torch.cat((input_drug, input_pert_idose), dim=1)
        # input_drug = [batch * (drug_input_dim + pert_type_input_dim + cell_id_input_dim + pert_idose_input_dim)]
        input_drug = input_drug.unsqueeze(1)
        # input_drug = [batch * 1 * (drug_input_dim + pert_type_input_dim + cell_id_input_dim + pert_idose_input_dim)]
        input_drug = input_drug.repeat(1, num_gene, 1)
        # input_drug = [batch * num_gene * (drug_input_dim + pert_type_input_dim +
        # cell_id_input_dim + pert_idose_input_dim)]
        input_gene = input_gene.unsqueeze(0)
        # input_gene = [1 * num_gene * gene_input_dim]
        input_gene = input_gene.repeat(num_batch, 1, 1)
        # input_gene = [batch * num_gene * gene_emb_dim]
        embed = torch.cat((input_drug, input_gene), dim=2)
        # embed = [batch * num_gene * (drug_input_dim + gene_input_dim + pert_type_input_dim +
        # cell_id_input_dim + pert_idose_input_dim)]
        out = self.linear(embed)
        # out = [batch * num_gene * 1]
        out = out.squeeze(2)
        # out = [batch * num_gene]
        return out

    def loss(self, label, predict):
        loss = point_wise_mse(label, predict)
        if self.regularization_type == 'l1':
            reg_loss = 0
            l1_crit = nn.L1Loss(reduction='sum')
            for name, param in self.named_parameters():
                if 'weight' in name:
                    reg_loss += l1_crit(param, torch.zeros_like(param))
            loss = loss + self.regularization_factor * reg_loss
        return loss


if __name__ == '__main__':
    import numpy as np
    import seaborn as sns
    import matplotlib.pyplot as plt
    x = np.random.normal(size=10000)
    sns.set(color_codes=True)
    sns.distplot(x)
    plt.show()