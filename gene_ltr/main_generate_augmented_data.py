import os
os.environ["CUDA_VISIBLE_DEVICES"] = "0"
import sys
sys.path.append(os.path.dirname(os.path.realpath(__file__)) + '/models')
sys.path.append(os.path.dirname(os.path.realpath(__file__)) + '/utils')
import nfp_gene_attention_model_regression
import nfp_drug_gene_attention_model_regression
import torch
import numpy as np
from molecules import Molecules
from scipy.stats import pearsonr
from datetime import datetime
from sklearn.decomposition import PCA


def get_eval_pert_id(input_file):
    with open(input_file) as f:
        for line in f:
            line = line.strip().split(',')
            if line[0] == 'test':
                test_id = line[1:]
            elif line[0] == 'dev':
                dev_id = line[1:]
            else:
                raise ValueError("Unknown dataset: %s" % line[0])
    return dev_id, test_id


def read_sig_info(input_file):
    sig_id = dict()
    with open(input_file) as f:
        f.readline()
        for line in f:
            line = line.strip().split('\t')
            assert len(line) == 8, 'Wrong format'
            sig_id[line[0]] = line[-1].split('|')
    return sig_id


def load_pre_trained_model(filter, pretrained_file, device):
    drug_input_dim = {'atom': 62, 'bond': 6}
    drug_embed_dim = 128
    conv_size = [16, 16]
    degree = [0, 1, 2, 3, 4, 5]
    gene_embed_dim = 128
    pert_type_emb_dim = 4
    cell_id_emb_dim = 4
    pert_idose_emb_dim = 4
    hid_dim = 128
    dropout = 0.1
    loss_type = 'point_wise_mse'
    intitializer = torch.nn.init.xavier_uniform_
    model = nfp_drug_gene_attention_model_regression.\
        NFPDrugGeneAttentionModelRegression(drug_input_dim=drug_input_dim,
                                            drug_emb_dim=drug_embed_dim,
                                            conv_size=conv_size, degree=degree,
                                            gene_input_dim=128, gene_emb_dim=gene_embed_dim,
                                            num_gene=978, hid_dim=hid_dim, dropout=dropout,
                                            loss_type=loss_type, device=device, initializer=intitializer,
                                            pert_type_input_dim=len(filter['pert_type']),
                                            cell_id_input_dim=len(filter['cell_id']),
                                            pert_idose_input_dim=len(filter['pert_idose']),
                                            pert_type_emb_dim=pert_type_emb_dim,
                                            cell_id_emb_dim=cell_id_emb_dim,
                                            pert_idose_emb_dim=pert_idose_emb_dim,
                                            use_pert_type=False,
                                            use_cell_id=True, use_pert_idose=True)
    # model.load_state_dict(torch.load(pretrained_file, map_location="cpu"))
    model.load_state_dict(torch.load(pretrained_file))
    model = model.to(device)
    model = model.double()
    model.eval()
    return model


def get_high_quality_exp(input_file, threshold):
    score = dict()
    with open(input_file) as f:
        for line in f:
            line = line.strip().split('\t')
            sc = float(line[1])
            if sc > threshold:
                score[line[0]] = float(sc)
    return score


def calculate_pearson(v1, v2):
    score = pearsonr(v1, v2)[0]
    return score


def read_drug_string(input_file):
    with open(input_file, 'r') as f:
        drug = dict()
        for line in f:
            line = line.strip().split(',')
            assert len(line) == 2, "Wrong format"
            drug[line[0]] = line[1]
    return drug


def convert_smile_to_feature(smiles, device):
    molecules = Molecules(smiles)
    node_repr = torch.FloatTensor([node.data for node in molecules.get_node_list('atom')]).to(device).double()
    edge_repr = torch.FloatTensor([node.data for node in molecules.get_node_list('bond')]).to(device).double()
    return {'molecules': molecules, 'atom': node_repr, 'bond': edge_repr}


def read_gene(input_file, device):
    with open(input_file, 'r') as f:
        gene = []
        for line in f:
            line = line.strip().split(',')
            assert len(line) == 129, "Wrong format"
            gene.append([float(i) for i in line[1:]])
    return torch.from_numpy(np.asarray(gene, dtype=np.float64)).to(device)


def transfrom_to_tensor(feature, drug, fp_type, device):
    drug_feature = []
    cell_id_set = ['HA1E', 'HT29', 'MCF7', 'YAPC', 'HELA', 'PC3', 'A375']
    pert_idose_set = ['1.11 um', '0.37 um', '10.0 um', '0.04 um', '3.33 um', '0.12 um']
    cell_id_dict = dict(zip(cell_id_set, list(range(len(cell_id_set)))))
    cell_id_feature = []
    pert_idose_dict = dict(zip(pert_idose_set, list(range(len(pert_idose_set)))))
    pert_idose_feature = []
    # print('Feature Summary:')
    # print(cell_id_set)
    # print(pert_idose_set)
    for i, ft in enumerate(feature):
        if fp_type in ['pubchem', 'circular', 'random', 'drug-target']:
            drug_fp = drug[ft[0]]
            drug_fp = np.asarray(drug_fp, dtype=np.float64)
        elif fp_type == 'neural':
            drug_fp = drug[ft[0]]
        else:
            raise ValueError("Unknown fingerprint: %s" % fp_type)
        drug_feature.append(drug_fp)
        cell_id_ft = np.zeros(len(cell_id_set))
        cell_id_ft[cell_id_dict[ft[2]]] = 1
        cell_id_feature.append(np.array(cell_id_ft, dtype=np.float64))
        pert_idose_ft = np.zeros(len(pert_idose_set))
        pert_idose_ft[pert_idose_dict[ft[3]]] = 1
        pert_idose_feature.append(np.array(pert_idose_ft, dtype=np.float64))
    tensor_feature = dict()
    if fp_type in ['pubchem', 'circular', 'random', 'drug-target']:
        tensor_feature['drug'] = torch.from_numpy(np.asarray(drug_feature, dtype=np.float64)).to(device)
    elif fp_type == 'neural':
        tensor_feature['drug'] = np.asarray(drug_feature)
    else:
        raise ValueError('Unknown fingerprint: %s' % fp_type)
    tensor_feature['cell_id'] = torch.from_numpy(np.asarray(cell_id_feature, dtype=np.float64)).to(device)
    tensor_feature['pert_idose'] = torch.from_numpy(np.asarray(pert_idose_feature, dtype=np.float64)).to(device)
    return tensor_feature


def read_data_lv4(input_file, filter, dev_pert_id, test_pert_id):
    data = dict()
    filter_drug = filter["pert_id"] + dev_pert_id + test_pert_id
    with open(input_file, 'r') as f:
        f.readline()  # skip header
        for line in f:
            line = line.strip().split(',')
            assert len(line) == 983, "Wrong format"
            if filter["time"] in line[0] and line[1] not in filter_drug and line[2] in filter["pert_type"] \
                    and line[3] in filter["cell_id"] and line[4] in filter["pert_idose"]:
                lb = [float(i) for i in line[5:]]
                data[line[0]] = lb
    return data


def write_signature(header, feature, label, output_file):
    with open(output_file, 'w') as f:
        f.write(header)
        for i, (ft, lb) in enumerate(zip(feature, label)):
            ft = ','.join(ft)
            lb = ','.join([str(i) for i in lb])
            f.write(ft + ',' + lb + '\n')


def read_data_lv5(input_file, filter, dev_pert_id, test_pert_id, high_quality_exp, model, drug, device, gene, id_map, data_lv4,
                  threshold):
    data = dict()
    data_temp = dict()
    filter_drug = filter["pert_id"] + dev_pert_id + test_pert_id
    hq_exp = high_quality_exp.keys()
    with open(input_file, 'r') as f:
        header = f.readline()  # skip header
        for line in f:
            line = line.strip().split(',')
            assert len(line) == 983, "Wrong format"
            if filter["time"] in line[0] and line[1] not in filter_drug and line[2] in filter["pert_type"] \
                    and line[3] in filter["cell_id"] and line[4] in filter["pert_idose"]:
                ft = tuple(line[0:5])
                lb = [float(i) for i in line[5:]]
                if line[0] in hq_exp:
                    data[ft] = lb
                else:
                    data_temp[ft] = lb
        hq_idx = [i[1:] for i in list(data.keys())]
        feature = []
        sig_id = []
        for key, value in data_temp.items():
            if key[1:] not in hq_idx:
                sig_id.append(key[0])
                feature.append(list(key[1:]))
        # sig_id = sig_id[::-1]
        # feature = feature[::-1]
        tensor_feature = transfrom_to_tensor(feature, drug, 'neural', device)
        batch_size = 64
        num_data = len(tensor_feature['drug'])
        # print(num_data)
        predict_np = np.empty([0, 978])
        with torch.no_grad():
            for idx in range(0, num_data, batch_size):
                excerpt = slice(idx, idx + batch_size)
                input_drug = convert_smile_to_feature(tensor_feature['drug'][excerpt], device)
                input_mask = create_mask_feature(input_drug, device)
                predict = model(input_drug, gene, input_mask, None, None,
                                tensor_feature['cell_id'][excerpt], tensor_feature['pert_idose'][excerpt])
                predict_np = np.concatenate((predict_np, predict.cpu().numpy()), axis=0)
                # if idx == batch_size * 10:
                #     break
        # print('filter_data')
        # print(np.shape(sig_id))
        # print(np.shape(feature))
        # print(np.shape(predict_np))
        new_id = []
        new_feature = []
        new_label = []
        score_data = []
        for id, ft, predict_vec in zip(sig_id, feature, predict_np):
            l4_ids = id_map[id]
            score_list = []
            score_temp = []
            vec_list = []
            for l4_id in l4_ids:
                if l4_id in data_lv4.keys():
                    vec = data_lv4[l4_id]
                    score = calculate_pearson(predict_vec, vec)
                    score_temp.append(score)
                    if score > threshold:
                        score_list.append(score)
                        vec_list.append(vec)
            if len(score_temp) > 0:
                score_data.append(max(score_temp))
            if len(score_list) > 0:
                max_id = np.argmax(score_list)
                lb = vec_list[max_id]
                new_id.append(id)
                new_feature.append(ft)
                new_label.append(lb)
        # print(len(score_data))
        # print(np.mean([1 if i > threshold else 0 for i in score_data]))
        new_feature = [[i]+j for i, j in zip(new_id, new_feature)]
        print(np.shape(new_feature))
        for ft, lb in data.items():
            new_feature.append(list(ft))
            new_label.append(lb)
    return header, new_feature, new_label


def choose_mean_example(examples):
    num_example = len(examples)
    mean_value = (num_example - 1) / 2
    indexes = np.argsort(examples, axis=0)
    indexes = np.argsort(indexes, axis=0)
    indexes = np.mean(indexes, axis=1)
    distance = (indexes - mean_value)**2
    index = np.argmin(distance)
    return examples[index]


def create_mask_feature(data, device):
    batch_idx = data['molecules'].get_neighbor_idx_by_batch('atom')
    molecule_length = [len(idx) for idx in batch_idx]
    mask = torch.zeros(len(batch_idx), max(molecule_length)).to(device).double()
    for idx, length in enumerate(molecule_length):
        mask[idx][:length] = 1
    return mask


if __name__ == '__main__':
    start_time = datetime.now()
    if torch.cuda.is_available():
        device = torch.device("cuda")
    else:
        device = torch.device("cpu")
    filter = {"time": "24H", "pert_id": ['BRD-U41416256', 'BRD-U60236422'], "pert_type": ["trt_cp"],
              "cell_id": ["MCF7", "A375", "HT29", "PC3", "HA1E", "YAPC", "HELA"],
              "pert_idose": ["0.04 um", "0.12 um", "0.37 um", "1.11 um", "3.33 um", "10.0 um"]}
    pretrained_file = 'saved_model/nfp_drug_gene_attention_model/epoch_89.pt'
    # pretrained_file = 'saved_model/3/epoch_48.pt'
    # get eval pert id
    dev_pert_id, test_pert_id = get_eval_pert_id('data/pert_id_eval.txt')
    # read sig info
    sig_id = read_sig_info('data/sig_info.txt')
    high_quality_exp = get_high_quality_exp('data/pearson_score.txt', 0.7)
    drug = read_drug_string('data/drugs_smiles.csv')
    gene = read_gene('data/gene_vector.csv', device)
    data_lv4 = read_data_lv4('data/l4_signature.csv', filter, dev_pert_id, test_pert_id)
    model = load_pre_trained_model(filter, pretrained_file, device)
    header, new_feature, new_label = read_data_lv5('data/signature_all.csv', filter, dev_pert_id, test_pert_id,
                                                   high_quality_exp, model, drug, device, gene, sig_id, data_lv4, 0.6)
    write_signature(header, new_feature, new_label, 'data/signature_augmented_0_6.csv')
    end_time = datetime.now()
    print(end_time - start_time)
