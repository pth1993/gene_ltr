import numpy as np
from scipy.spatial.distance import cdist
from sklearn.metrics import mean_squared_error
from scipy.stats import pearsonr, spearmanr
import argparse


def read_drug(input_file, num_feature):
    drug = []
    drug_vec = []
    with open(input_file, 'r') as f:
        for line in f:
            line = line.strip().split(',')
            assert len(line) == num_feature + 1, "Wrong format"
            bin_vec = [float(i) for i in line[1:]]
            drug.append(line[0])
            drug_vec.append(bin_vec)
    drug_vec = np.asarray(drug_vec, dtype=np.float64)
    index = []
    for i in range(np.shape(drug_vec)[1]):
        if len(set(drug_vec[:, i])) > 1:
            index.append(i)
    drug_vec = drug_vec[:, index]
    drug = dict(zip(drug, drug_vec))
    return drug, len(index)


def choose_mean_example(examples):
    num_example = len(examples)
    mean_value = (num_example - 1) / 2
    indexes = np.argsort(examples, axis=0)
    indexes = np.argsort(indexes, axis=0)
    indexes = np.mean(indexes, axis=1)
    distance = (indexes - mean_value)**2
    index = np.argmin(distance)
    return examples[index]


def split_data_by_pert_id(input_file):
    with open(input_file) as f:
        test_pert_id = f.readline().strip().split(',')[1:]
        dev_pert_id = f.readline().strip().split(',')[1:]
        return dev_pert_id, test_pert_id


def read_data(input_file_train, input_file_dev, input_file_test, filter, drug, dev_pert_id, test_pert_id):
    data_train = dict()
    data_dev = dict()
    data_test = dict()
    pert_id = []
    filter_pert_id = dev_pert_id + test_pert_id + filter["pert_id"]
    with open(input_file_train, 'r') as f:
        f.readline()
        for line in f:
            line = line.strip().split(',')
            assert len(line) == 983, "Wrong format"
            if filter["time"] in line[0] and line[1] not in filter_pert_id and line[2] in filter["pert_type"] \
                    and line[3] in filter["cell_id"] and line[4] in filter["pert_idose"]:
                pert_id.append(line[1])
                ft = ','.join(line[1:5])
                lb = [float(i) for i in line[5:]]
                try:
                    data_train[ft].append(lb)
                except KeyError:
                    data_train[ft] = [lb]
    train_pert_id = sorted(list(set(pert_id)))
    
    pert_id = []
    with open(input_file_dev, 'r') as f:
        f.readline()
        for line in f:
            line = line.strip().split(',')
            assert len(line) == 983, "Wrong format"
            if filter["time"] in line[0] and line[1] in dev_pert_id and line[2] in filter["pert_type"] \
                    and line[3] in filter["cell_id"] and line[4] in filter["pert_idose"]:
                pert_id.append(line[1])
                ft = ','.join(line[1:5])
                lb = [float(i) for i in line[5:]]
                try:
                    data_dev[ft].append(lb)
                except KeyError:
                    data_dev[ft] = [lb]
    dev_pert_id = sorted(list(set(pert_id)))

    pert_id = []
    with open(input_file_test, 'r') as f:
        f.readline()
        for line in f:
            line = line.strip().split(',')
            assert len(line) == 983, "Wrong format"
            if filter["time"] in line[0] and line[1] in test_pert_id and line[2] in filter["pert_type"] \
                    and line[3] in filter["cell_id"] and line[4] in filter["pert_idose"]:
                pert_id.append(line[1])
                ft = ','.join(line[1:5])
                lb = [float(i) for i in line[5:]]
                try:
                    data_test[ft].append(lb)
                except KeyError:
                    data_test[ft] = [lb]
    test_pert_id = sorted(list(set(pert_id)))

    drug_ft_train = []
    drug_ft_dev = []
    drug_ft_test = []
    for id in train_pert_id:
        drug_ft_train.append(drug[id])
    drug_ft_train = np.asarray(drug_ft_train, dtype=np.float64)
    for id in dev_pert_id:
        drug_ft_dev.append(drug[id])
    drug_ft_dev = np.asarray(drug_ft_dev, dtype=np.float64)
    for id in test_pert_id:
        drug_ft_test.append(drug[id])
    drug_ft_test = np.asarray(drug_ft_test, dtype=np.float64)
    data_train_new = dict()
    data_dev_new = dict()
    data_test_new = dict()
    train_pert_dict = dict(zip(list(range(len(train_pert_id))), train_pert_id))
    dev_pert_dict = dict(zip(dev_pert_id, list(range(len(dev_pert_id)))))
    test_pert_dict = dict(zip(test_pert_id, list(range(len(test_pert_id)))))

    for ft, lb in sorted(data_train.items()):
        if len(lb) == 1:
            data_train_new[ft] = lb[0]
        else:
            data_train_new[ft] = choose_mean_example(lb)
    for ft, lb in sorted(data_dev.items()):
        if len(lb) == 1:
            data_dev_new[ft] = lb[0]
        else:
            data_dev_new[ft] = choose_mean_example(lb)
    for ft, lb in sorted(data_test.items()):
        if len(lb) == 1:
            data_test_new[ft] = lb[0]
        else:
            data_test_new[ft] = choose_mean_example(lb)
    return data_train_new, drug_ft_train, data_dev_new, drug_ft_dev, data_test_new, drug_ft_test, train_pert_dict, \
           dev_pert_dict, test_pert_dict


def calculate_distance(input_a, input_b, distance):
    output = cdist(input_a, input_b, distance)
    return output


def get_neighbor(distance):
    return np.argsort(distance)


def kNN(data_train, data_test, train_pert_dict, test_pert_dict, neighbor, k_list):
    data_predict = []
    data_test_list = []
    for ft, lb in sorted(data_test.items()):
        data_test_list.append(lb)
        ft = ft.split(',')
        drug_idx = test_pert_dict[ft[0]]
        nb_list = neighbor[drug_idx]
        cnt = 0
        sum_vector = np.zeros(978)
        output_list = []
        for nb_idx in nb_list:
            nb = train_pert_dict[nb_idx]
            ft_train = ','.join([nb] + ft[1:])
            if ft_train in data_train.keys():
                sum_vector += data_train[ft_train]
                cnt += 1
                if cnt in k_list:
                    output_list.append(sum_vector/cnt)
        # output_list.append(sum_vector / cnt)
        data_predict.append(output_list)
    data_predict = np.array(data_predict)
    data_predict_list = np.split(data_predict, len(k_list), axis=1)
    data_shape = np.shape(data_predict_list[0])
    data_predict_list = [a.reshape(data_shape[0], data_shape[2]) for a in data_predict_list]
    return data_predict_list, np.array(data_test_list)


def precision_k(label_test, label_predict, k):
    num_pos = 100
    num_neg = 100
    label_test = np.argsort(label_test, axis=1)
    label_predict = np.argsort(label_predict, axis=1)
    precision_k_neg = []
    precision_k_pos = []
    neg_test_set = label_test[:, :num_neg]
    pos_test_set = label_test[:, -num_pos:]
    neg_predict_set = label_predict[:, :k]
    pos_predict_set = label_predict[:, -k:]
    for i in range(len(neg_test_set)):
        neg_test = set(neg_test_set[i])
        pos_test = set(pos_test_set[i])
        neg_predict = set(neg_predict_set[i])
        pos_predict = set(pos_predict_set[i])
        precision_k_neg.append(len(neg_test.intersection(neg_predict)) / k)
        precision_k_pos.append(len(pos_test.intersection(pos_predict)) / k)
    return np.mean(precision_k_neg), np.mean(precision_k_pos)


def rmse_calculation(label_test, label_predict):
    return np.sqrt(mean_squared_error(label_test, label_predict))


def correlation(label_test, label_predict, correlation_type):
    if correlation_type == 'pearson':
        corr = pearsonr
    elif correlation_type == 'spearman':
        corr = spearmanr
    else:
        raise ValueError("Unknown correlation type: %s" % correlation_type)
    score = []
    for lb_test, lb_predict in zip(label_test, label_predict):
        score.append(corr(lb_test, lb_predict)[0])
    # print(','.join([str(i)  for i in score]))
    return np.mean(score)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='kNN')
    parser.add_argument('--train_file')
    args = parser.parse_args()
    gene_expression_file_train = args.train_file
    filter = {"time": "24H", "pert_id": ['BRD-U41416256', 'BRD-U60236422'], "pert_type": ["trt_cp"],
              "cell_id": ["A375", "HT29", "MCF7", "PC3", "HA1E", "YAPC", "HELA"],
              "pert_idose": ["0.04 um", "0.04 um", "0.12 um", "0.37 um", "1.11 um", "3.33 um", "10.0 um"]}
    dist_list = ['cosine', 'euclidean', 'correlation', 'rogerstanimoto', 'jaccard']
    ft_list = ['pubchem', 'circular', 'drug_target', 'ltip']
    best_index = [1, 2, 3, 0]
    # dist_list = ['rogerstanimoto']
    drug_pubchem = read_drug('data/drugs_pubchem.csv', 881)[0]
    drug_circular = read_drug('data/drugs_circular.csv', 1024)[0]
    drug_drug_target = read_drug('data/drugs_target_diy.csv', 978)[0]
    drug_ltip = read_drug('data/drugs_target_ltip.csv', 978)[0]
    k_list = [11]
    patk = [100]
    dev_pert_id, test_pert_id = split_data_by_pert_id('data/pert_id_eval.txt')
    for i, drug in enumerate([drug_pubchem, drug_circular, drug_drug_target, drug_ltip]):
    # for i, drug in enumerate([drug_pubchem]):
        if i == 0:
            print("Drug feature: pubchem")
        elif i == 1:
            print("Drug feature: circular")
        elif i == 2:
            print("Drug feature: drug-target")
        elif i == 3:
            print("Drug feature: ltip")
        pearson_list_dev = []
        rmse_list_dev = []
        spearman_list_dev = []
        precision_list_dev = []
        pearson_list_test = []
        rmse_list_test = []
        spearman_list_test = []
        precision_list_test = []
        for dist in dist_list:
            # if i == 0:
            #     print("Drug feature: pubchem, dist: %s" % dist)
            # elif i == 1:
            #     print("Drug feature: circular, dist: %s" % dist)
            # elif i == 2:
            #     print("Drug feature: drug-target, dist: %s" % dist)
            # elif i == 3:
            #     print("Drug feature: ltip, dist: %s" % dist)
            data_train, drug_ft_train, data_dev, drug_ft_dev, data_test, drug_ft_test, train_pert_dict, dev_pert_dict, \
            test_pert_dict = read_data(gene_expression_file_train, 'data/signature_dev.csv',
                                       'data/signature_test.csv', filter, drug, dev_pert_id, test_pert_id)
            
            distance = calculate_distance(drug_ft_dev, drug_ft_train, dist)
            neighbor = get_neighbor(distance)
            data_predict_list, data_dev_list = \
                kNN(data_train, data_dev,  train_pert_dict, dev_pert_dict, neighbor, k_list)
            for j, data_predict in enumerate(data_predict_list):
                rmse = rmse_calculation(data_dev_list, data_predict)
                rmse_list_dev.append(rmse)
                pearson = correlation(data_dev_list, data_predict, 'pearson')
                pearson_list_dev.append(pearson)
                spearman = correlation(data_dev_list, data_predict, 'spearman')
                spearman_list_dev.append(spearman)
                for k in patk:
                    precision_neg, precision_pos = precision_k(data_dev_list, data_predict, k)
                    precision_list_dev.append([precision_pos, precision_neg])
            
            distance = calculate_distance(drug_ft_test, drug_ft_train, dist)
            neighbor = get_neighbor(distance)
            data_predict_list, data_test_list = \
                kNN(data_train, data_test,  train_pert_dict, test_pert_dict, neighbor, k_list)
            if dist == dist_list[best_index[i]]:
                print('Best: %s' % dist)
                np.savetxt("predicted/knn_model_%s_augmented.csv" % ft_list[i], np.array(data_predict_list)[0],
                           delimiter=",", fmt='%.6f')

            for j, data_predict in enumerate(data_predict_list):
                # print(dist)
                # print("Num Neighbors: %d" % k_list[j])
                rmse = rmse_calculation(data_test_list, data_predict)
                rmse_list_test.append(rmse)
                # print('RMSE: %.4f' % rmse)
                pearson = correlation(data_test_list, data_predict, 'pearson')
                pearson_list_test.append(pearson)
                # print('Pearson\'s correlation: %.4f' % pearson)
                spearman = correlation(data_test_list, data_predict, 'spearman')
                spearman_list_test.append(spearman)
                # print('Spearman\'s correlation: %.4f' % spearman)
                for k in patk:
                    precision_neg, precision_pos = precision_k(data_test_list, data_predict, k)
                    precision_list_test.append([precision_pos, precision_neg])
                    # print("Precision@%d Positive: %.4f" % (k, precision_pos))
                    # print("Precision@%d Negative: %.4f" % (k, precision_neg))
        best_dev_index = np.argmax(pearson_list_dev)
        print('Best index %d' % best_dev_index)
        print('Pearson\'s correlation: %.4f' % pearson_list_test[best_dev_index])
        print('Spearman\'s correlation: %.4f' % spearman_list_test[best_dev_index])
        print('RMSE: %.4f' % rmse_list_test[best_dev_index])
        precision_pos, precision_neg = precision_list_test[best_dev_index]
        print("Precision@%d Positive: %.4f" % (k, precision_pos))
        print("Precision@%d Negative: %.4f" % (k, precision_neg))
