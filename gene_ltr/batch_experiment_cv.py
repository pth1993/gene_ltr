import shlex
import subprocess
import random
import os

eid = str(random.randint(1, 1000000))

if not os.path.exists('output_cv'):
    os.makedirs('output_cv')
    os.makedirs('output_cv/fold_0')
    os.makedirs('output_cv/fold_1')
    os.makedirs('output_cv/fold_2')
    os.makedirs('output_cv/fold_3')
    os.makedirs('output_cv/fold_4')

for i in range(5):
    # with open('output_cv/fold_%d/' % i + eid + '_base_model_regression_pubchem.txt', 'w') as f:
    #     subprocess.check_call(shlex.split('python cv_script/main_base_model_regression.py --drug_file '
    #                                       '"data/drugs_pubchem.csv" --fp_type "pubchem" '
    #                                       '--data_file "data/signature_0_7.csv" --fold "%d"' % i), stdout=f)
    # with open('output_cv/fold_%d/' % i + eid + '_base_model_regression_circular.txt', 'w') as f:
    #     subprocess.check_call(shlex.split('python cv_script/main_base_model_regression.py --drug_file '
    #                                       '"data/drugs_circular.csv" --fp_type "circular" '
    #                                       '--data_file "data/signature_0_7.csv" --fold "%d"' % i), stdout=f)
    # with open('output_cv/fold_%d/' % i + eid + '_base_model_regression_drug_target.txt', 'w') as f:
    #     subprocess.check_call(shlex.split('python cv_script/main_base_model_regression.py --drug_file '
    #                                       '"data/drugs_target_diy.csv" --fp_type "drug-target" '
    #                                       '--data_file "data/signature_0_7.csv" --fold "%d"' % i), stdout=f)
    # with open('output_cv/fold_%d/' % i + eid + '_base_model_regression_ltip.txt', 'w') as f:
    #     subprocess.check_call(shlex.split('python cv_script/main_base_model_regression.py --drug_file '
    #                                       '"data/drugs_target_ltip.csv" --fp_type "drug-target" '
    #                                       '--data_file "data/signature_0_7.csv" --fold "%d"' % i), stdout=f)
    # with open('output_cv/fold_%d/' % i + eid + '_base_model_regression_random.txt', 'w') as f:
    #     subprocess.check_call(shlex.split('python cv_script/main_base_model_regression.py --drug_file '
    #                                       '"data/drugs_random.csv" --fp_type "random" '
    #                                       '--data_file "data/signature_0_7.csv" --fold "%d"' % i), stdout=f)
    
    # with open('output_cv/fold_%d/' % i + eid + '_linear_regression_none_pubchem.txt', 'w') as f:
    #     subprocess.check_call(shlex.split('python cv_script/main_linear_regression.py --drug_file '
    #                                       '"data/drugs_pubchem.csv" --fp_type "pubchem" '
    #                                       '--regularization_type "none" --data_file "data/signature_0_7.csv" '
    #                                       '--fold "%d"' % i), stdout=f)
    # with open('output_cv/fold_%d/' % i + eid + '_linear_regression_none_circular.txt', 'w') as f:
    #     subprocess.check_call(shlex.split('python cv_script/main_linear_regression.py --drug_file '
    #                                       '"data/drugs_circular.csv" --fp_type "circular" '
    #                                       '--regularization_type "none" --data_file "data/signature_0_7.csv" '
    #                                       '--fold "%d"' % i), stdout=f)
    # with open('output_cv/fold_%d/' % i + eid + '_linear_regression_none_drug_target.txt', 'w') as f:
    #     subprocess.check_call(shlex.split('python cv_script/main_linear_regression.py --drug_file '
    #                                       '"data/drugs_target_diy.csv" --fp_type "drug-target" '
    #                                       '--regularization_type "none" --data_file "data/signature_0_7.csv" '
    #                                       '--fold "%d"' % i), stdout=f)
    # with open('output_cv/fold_%d/' % i + eid + '_linear_regression_none_ltip.txt', 'w') as f:
    #     subprocess.check_call(shlex.split('python cv_script/main_linear_regression.py --drug_file '
    #                                       '"data/drugs_target_ltip.csv" --fp_type "drug-target" '
    #                                       '--regularization_type "none" --data_file "data/signature_0_7.csv" '
    #                                       '--fold "%d"' % i), stdout=f)
    #
    # with open('output_cv/fold_%d/' % i + eid + '_linear_regression_l1_pubchem.txt', 'w') as f:
    #     subprocess.check_call(shlex.split('python cv_script/main_linear_regression.py --drug_file '
    #                                       '"data/drugs_pubchem.csv" --fp_type "pubchem" '
    #                                       '--regularization_type "l1" --data_file "data/signature_0_7.csv" '
    #                                       '--fold "%d"' % i), stdout=f)
    # with open('output_cv/fold_%d/' % i + eid + '_linear_regression_l1_circular.txt', 'w') as f:
    #     subprocess.check_call(shlex.split('python cv_script/main_linear_regression.py --drug_file '
    #                                       '"data/drugs_circular.csv" --fp_type "circular" '
    #                                       '--regularization_type "l1" --data_file "data/signature_0_7.csv" '
    #                                       '--fold "%d"' % i), stdout=f)
    # with open('output_cv/fold_%d/' % i + eid + '_linear_regression_l1_drug_target.txt', 'w') as f:
    #     subprocess.check_call(shlex.split('python cv_script/main_linear_regression.py --drug_file '
    #                                       '"data/drugs_target_diy.csv" --fp_type "drug-target" '
    #                                       '--regularization_type "l1" --data_file "data/signature_0_7.csv" '
    #                                       '--fold "%d"' % i), stdout=f)
    # with open('output_cv/fold_%d/' % i + eid + '_linear_regression_l1_ltip.txt', 'w') as f:
    #     subprocess.check_call(shlex.split('python cv_script/main_linear_regression.py --drug_file '
    #                                       '"data/drugs_target_ltip.csv" --fp_type "drug-target" '
    #                                       '--regularization_type "l1" --data_file "data/signature_0_7.csv" '
    #                                       '--fold "%d"' % i), stdout=f)
    #
    # with open('output_cv/fold_%d/' % i + eid + '_linear_regression_l2_pubchem.txt', 'w') as f:
    #     subprocess.check_call(shlex.split('python cv_script/main_linear_regression.py --drug_file '
    #                                       '"data/drugs_pubchem.csv" --fp_type "pubchem" '
    #                                       '--regularization_type "l2" --data_file "data/signature_0_7.csv" '
    #                                       '--fold "%d"' % i), stdout=f)
    # with open('output_cv/fold_%d/' % i + eid + '_linear_regression_l2_circular.txt', 'w') as f:
    #     subprocess.check_call(shlex.split('python cv_script/main_linear_regression.py --drug_file '
    #                                       '"data/drugs_circular.csv" --fp_type "circular" '
    #                                       '--regularization_type "l2" --data_file "data/signature_0_7.csv" '
    #                                       '--fold "%d"' % i), stdout=f)
    # with open('output_cv/fold_%d/' % i + eid + '_linear_regression_l2_drug_target.txt', 'w') as f:
    #     subprocess.check_call(shlex.split('python cv_script/main_linear_regression.py --drug_file '
    #                                       '"data/drugs_target_diy.csv" --fp_type "drug-target" '
    #                                       '--regularization_type "l2" --data_file "data/signature_0_7.csv" '
    #                                       '--fold "%d"' % i), stdout=f)
    # with open('output_cv/fold_%d/' % i + eid + '_linear_regression_l2_ltip.txt', 'w') as f:
    #     subprocess.check_call(shlex.split('python cv_script/main_linear_regression.py --drug_file '
    #                                       '"data/drugs_target_ltip.csv" --fp_type "drug-target" '
    #                                       '--regularization_type "l2" --data_file "data/signature_0_7.csv" '
    #                                       '--fold "%d"' % i), stdout=f)
    
    # with open('output_cv/fold_%d/' % i + eid + '_nfp_no_attention_model_regression.txt', 'w') as f:
    #     subprocess.check_call(shlex.split('python cv_script/main_nfp_model_regression.py --drug_file '
    #                                       '"data/drugs_smiles.csv" --fp_type "neural" '
    #                                       '--data_file "data/signature_0_7.csv" --fold "%d"' % i), stdout=f)
    # with open('output_cv/fold_%d/' % i + eid + '_nfp_drug_attention_model_regression.txt', 'w') as f:
    #     subprocess.check_call(shlex.split('python cv_script/main_nfp_drug_attention_model_regression.py --drug_file '
    #                                       '"data/drugs_smiles.csv" --fp_type "neural" --dropout 0.1 '
    #                                       '--data_file "data/signature_0_7.csv" --fold "%d"' % i), stdout=f)
    # with open('output_cv/fold_%d/' % i + eid + '_nfp_gene_attention_model_regression.txt', 'w') as f:
    #     subprocess.check_call(shlex.split('python cv_script/main_nfp_gene_attention_model_regression.py --drug_file '
    #                                       '"data/drugs_smiles.csv" --fp_type "neural" --dropout 0.1 '
    #                                       '--data_file "data/signature_0_7.csv" --fold "%d"' % i), stdout=f)
    with open('output_cv/fold_%d/' % i + eid + '_nfp_drug_gene_attention_model_regression.txt', 'w') as f:
        subprocess.check_call(shlex.split('python cv_script/main_nfp_drug_gene_attention_model_regression.py --drug_file '
                                          '"data/drugs_smiles.csv" --fp_type "neural" --dropout 0.1 '
                                          '--data_file "data/signature_0_7.csv" --fold "%d"' % i), stdout=f)
