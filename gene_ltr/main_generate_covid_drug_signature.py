import warnings
warnings.filterwarnings("ignore")
import os
os.environ["CUDA_VISIBLE_DEVICES"] = "0"
import sys
sys.path.append(os.path.dirname(os.path.realpath(__file__)) + '/models')
sys.path.append(os.path.dirname(os.path.realpath(__file__)) + '/utils')
import nfp_drug_gene_attention_model_regression
import torch
import numpy as np
from molecules import Molecules



def load_pre_trained_model(filter, pretrained_file, device):
    drug_input_dim = {'atom': 62, 'bond': 6}
    drug_embed_dim = 128
    conv_size = [16, 16]
    degree = [0, 1, 2, 3, 4, 5]
    gene_embed_dim = 128
    pert_type_emb_dim = 4
    cell_id_emb_dim = 4
    pert_idose_emb_dim = 4
    hid_dim = 128
    dropout = 0.1
    loss_type = 'point_wise_mse'
    intitializer = torch.nn.init.xavier_uniform_
    model = nfp_drug_gene_attention_model_regression.\
        NFPDrugGeneAttentionModelRegression(drug_input_dim=drug_input_dim,
                                            drug_emb_dim=drug_embed_dim,
                                            conv_size=conv_size, degree=degree,
                                            gene_input_dim=128, gene_emb_dim=gene_embed_dim,
                                            num_gene=978, hid_dim=hid_dim, dropout=dropout,
                                            loss_type=loss_type, device=device, initializer=intitializer,
                                            pert_type_input_dim=len(filter['pert_type']),
                                            cell_id_input_dim=8,
                                            pert_idose_input_dim=6,
                                            pert_type_emb_dim=pert_type_emb_dim,
                                            cell_id_emb_dim=cell_id_emb_dim,
                                            pert_idose_emb_dim=pert_idose_emb_dim,
                                            use_pert_type=False,
                                            use_cell_id=True, use_pert_idose=True)
    if str(device) == 'cpu':
        model.load_state_dict(torch.load(pretrained_file, map_location='cpu'))
    else:
        model.load_state_dict(torch.load(pretrained_file))
    model = model.to(device)
    model = model.double()
    model.eval()
    return model


def read_drug_string(input_file, ignore_drug):
    with open(input_file, 'r') as f:
        drug = dict()
        for line in f:
            line = line.strip().split(',')
            assert len(line) == 2, "Wrong format"
            if line[0] not in ignore_drug:
                drug[line[0]] = line[1]
    return drug


def convert_smile_to_feature(smiles, device):
    molecules = Molecules(smiles)
    node_repr = torch.FloatTensor([node.data for node in molecules.get_node_list('atom')]).to(device).double()
    edge_repr = torch.FloatTensor([node.data for node in molecules.get_node_list('bond')]).to(device).double()
    return {'molecules': molecules, 'atom': node_repr, 'bond': edge_repr}


def transfrom_to_tensor(feature, drug, fp_type, device):
    drug_feature = []
    # cell_id_set = ['HA1E', 'HT29', 'MCF7', 'YAPC', 'HELA', 'PC3', 'A375']
    # pert_idose_set = ['1.11 um', '0.37 um', '10.0 um', '0.04 um', '3.33 um', '0.12 um']
    cell_id_set = ['A375', 'A549', 'HA1E', 'HELA', 'HT29', 'MCF7', 'PC3', 'YAPC']
    pert_idose_set = ['0.04 um', '0.12 um', '0.37 um', '1.11 um', '10.0 um', '3.33 um']
    cell_id_dict = dict(zip(cell_id_set, list(range(len(cell_id_set)))))
    cell_id_feature = []
    pert_idose_dict = dict(zip(pert_idose_set, list(range(len(pert_idose_set)))))
    pert_idose_feature = []
    for i, ft in enumerate(feature):
        if fp_type in ['pubchem', 'circular', 'random', 'drug-target']:
            drug_fp = drug[ft[0]]
            drug_fp = np.asarray(drug_fp, dtype=np.float64)
        elif fp_type == 'neural':
            drug_fp = drug[ft[0]]
        else:
            raise ValueError("Unknown fingerprint: %s" % fp_type)
        drug_feature.append(drug_fp)
        cell_id_ft = np.zeros(len(cell_id_set))
        cell_id_ft[cell_id_dict[ft[2]]] = 1
        cell_id_feature.append(np.array(cell_id_ft, dtype=np.float64))
        pert_idose_ft = np.zeros(len(pert_idose_set))
        pert_idose_ft[pert_idose_dict[ft[3]]] = 1
        pert_idose_feature.append(np.array(pert_idose_ft, dtype=np.float64))
    tensor_feature = dict()
    if fp_type in ['pubchem', 'circular', 'random', 'drug-target']:
        tensor_feature['drug'] = torch.from_numpy(np.asarray(drug_feature, dtype=np.float64)).to(device)
    elif fp_type == 'neural':
        tensor_feature['drug'] = np.asarray(drug_feature)
    else:
        raise ValueError('Unknown fingerprint: %s' % fp_type)
    tensor_feature['cell_id'] = torch.from_numpy(np.asarray(cell_id_feature, dtype=np.float64)).to(device)
    tensor_feature['pert_idose'] = torch.from_numpy(np.asarray(pert_idose_feature, dtype=np.float64)).to(device)
    return tensor_feature


def create_mask_feature(data, device):
    batch_idx = data['molecules'].get_neighbor_idx_by_batch('atom')
    molecule_length = [len(idx) for idx in batch_idx]
    mask = torch.zeros(len(batch_idx), max(molecule_length)).to(device).double()
    for idx, length in enumerate(molecule_length):
        mask[idx][:length] = 1
    return mask


def read_gene(input_file, device):
    with open(input_file, 'r') as f:
        gene = []
        for line in f:
            line = line.strip().split(',')
            assert len(line) == 129, "Wrong format"
            gene.append([float(i) for i in line[1:]])
    return torch.from_numpy(np.asarray(gene, dtype=np.float64)).to(device)


def generate_predicted_data(drug, drug_list, gene, model, output_file_list, filter, device):
    output_file_list = [open(i, 'w') for i in output_file_list]
    batch_size = 32
    cell_id = filter['cell_id']
    pert_idose = filter['pert_idose'][0]
    for i, cell in enumerate(cell_id):
        feature = [[d, '_', cell, pert_idose] for d in drug_list]
        tensor_feature = transfrom_to_tensor(feature, drug, 'neural', device)
        num_data = len(tensor_feature['drug'])
        predict_np = np.empty([0, 978])
        with torch.no_grad():
            for idx in range(0, num_data, batch_size):
                excerpt = slice(idx, idx + batch_size)
                input_drug = convert_smile_to_feature(tensor_feature['drug'][excerpt], device)
                input_mask = create_mask_feature(input_drug, device)
                predict = model(input_drug, gene, input_mask, None, None,
                                tensor_feature['cell_id'][excerpt], tensor_feature['pert_idose'][excerpt])
                predict_np = np.concatenate((predict_np, predict.cpu().numpy()), axis=0)
        for vec in predict_np:
            output_file_list[i].write(','.join([str(j) for j in vec]) + '\n')
        output_file_list[i].close()


def test_parse_smile(drug, device):
    for d, v in drug.items():
        convert_smile_to_feature([v], device)


def get_drug_name(drug, drug_list_file):
    map_drug = dict()
    with open(drug_list_file, 'r') as f:
        for line in f:
            line = line.strip().split('\t')
            if line[0]:
                map_drug[line[0]] = line[1]
    out = []
    for d in drug:
        out.append(map_drug[d])
    print(sorted(out))


def write_drug_list(drug, output_file):
    print(len(drug))
    with open(output_file, 'w') as f:
        f.write(','.join(list(sorted(drug.keys()))))
    return sorted(drug.keys())


def choose_mean_example(examples):
    num_example = len(examples)
    mean_value = (num_example - 1) / 2
    indexes = np.argsort(examples, axis=0)
    indexes = np.argsort(indexes, axis=0)
    indexes = np.mean(indexes, axis=1)
    distance = (indexes - mean_value)**2
    index = np.argmin(distance)
    return examples[index]


def get_ground_truth_signature(input_file, filter):
    feature = []
    label = []
    data = dict()
    pert_id = []
    with open(input_file, 'r') as f:
        f.readline()  # skip header
        for line in f:
            line = line.strip().split(',')
            assert len(line) == 983, "Wrong format"
            if filter["time"] in line[0] and line[1] not in filter['pert_id'] and line[2] in filter["pert_type"] \
                    and line[3] in filter['cell_id'] and line[4] in filter["pert_idose"]:
                ft = ','.join(line[1:5])
                lb = [float(i) for i in line[5:]]
                if ft in data.keys():
                    data[ft].append(lb)
                else:
                    data[ft] = [lb]
    for ft, lb in sorted(data.items()):
        ft = ft.split(',')
        feature.append(ft)
        pert_id.append(ft[0])
        if len(lb) == 1:
            label.append(lb[0])
        else:
            lb = choose_mean_example(lb)
            label.append(lb)
    return np.asarray(label, dtype=np.float64), np.asarray(feature)


def get_hq_drug_list(input_file):
    with open(input_file, 'r') as f:
        drug = f.readline().strip().split(',')
    return drug


def generate_high_quality_signature(drug, filter, drug_sig, exp_info, output_file):
    out = dict()
    for sig, exp in zip(drug_sig, exp_info):
        if exp[2] in out:
            if exp[0] in out[exp[2]]:
                out[exp[2]][exp[0]][exp[3]] = sig
            else:
                out[exp[2]][exp[0]] = {exp[3]: sig}
        else:
            out[exp[2]] = {exp[0]: {exp[3]: sig}}
    for i, c in enumerate(filter['cell_id']):
        with open(output_file[i], 'w') as f:
            for d in drug:
                if d in out[c]:
                    if '10.0 um' in out[c][d]:
                        f.write('HQ-' + d + ',' + ','.join([str(i) for i in out[c][d]['10.0 um']]) + '\n')
                    elif '3.33 um' in out[c][d]:
                        f.write('HQ-' + d + ',' + ','.join([str(i) for i in out[c][d]['3.33 um']]) + '\n')
                    elif '1.11 um' in out[c][d]:
                        f.write('HQ-' + d + ',' + ','.join([str(i) for i in out[c][d]['1.11 um']]) + '\n')
                    elif '0.37 um' in out[c][d]:
                        f.write('HQ-' + d + ',' + ','.join([str(i) for i in out[c][d]['0.37 um']]) + '\n')
                    elif '0.12 um' in out[c][d]:
                        f.write('HQ-' + d + ',' + ','.join([str(i) for i in out[c][d]['0.12 um']]) + '\n')
                    elif '0.04 um' in out[c][d]:
                        f.write('HQ-' + d + ',' + ','.join([str(i) for i in out[c][d]['0.04 um']]) + '\n')


if __name__ == '__main__':
    # ignore_drug = ['DB00200', 'DB00225', 'DB00325', 'DB00515', 'DB00526', 'DB00958', 'DB01663', 'DB01703', 'DB01929',
    #                'DB01999', 'DB02188', 'DB02667', 'DB02724', 'DB03350', 'DB03436', 'DB03492', 'DB03614', 'DB03934',
    #                'DB04100', 'DB04156', 'DB04231', 'DB04444', 'DB06783', 'DB08276', 'DB09385', 'DB11104', 'DB11191',
    #                'DB11630', 'DB12397', 'DB12453', 'DB13145', 'DB14198', 'DB14497', 'DB14515']
    # pretrained_file = 'saved_model/nfp_drug_gene_attention_model_a549_new_test_lr_00002/epoch_86.pt'
    # if torch.cuda.is_available():
    #     device = torch.device("cuda")
    # else:
    #     device = torch.device("cpu")
    # filter = {"time": "24H", "pert_id": ['BRD-U41416256', 'BRD-U60236422'], "pert_type": ["trt_cp"],
    #           "cell_id": ["MCF7", "A375", "HT29", "PC3", "HA1E", "YAPC", "HELA", "A549"],
    #           "pert_idose": ["10.0 um"]}
    # predicted_file_list = ['data/covid_data/predicted_mcf7.csv', 'data/covid_data/predicted_a375.csv',
    #                       'data/covid_data/predicted_ht29.csv', 'data/covid_data/predicted_pc3.csv',
    #                       'data/covid_data/predicted_ha1e.csv', 'data/covid_data/predicted_yapc.csv',
    #                       'data/covid_data/predicted_hela.csv', 'data/covid_data/predicted_a549.csv']
    # drug = read_drug_string('data/covid-19_drug_list.csv', ignore_drug)
    # drug_list = write_drug_list(drug, 'data/covid_data/covid-19_drug_list_refine.csv')
    # gene = read_gene('data/gene_vector.csv', device)
    # model = load_pre_trained_model(filter, pretrained_file, device)
    # generate_predicted_data(drug, drug_list, gene, model, predicted_file_list, filter, device)

    filter = {"time": "24H", "pert_id": ['BRD-U41416256', 'BRD-U60236422'], "pert_type": ["trt_cp"],
              "cell_id": ["MCF7", "A375", "HT29", "PC3", "HA1E", "YAPC", "HELA", "A549"],
              "pert_idose": ["0.04 um", "0.12 um", "0.37 um", "1.11 um", "3.33 um", "10.0 um"]}
    hq_file_list = ['data/covid_data/hq_mcf7.csv', 'data/covid_data/hq_a375.csv',
                    'data/covid_data/hq_ht29.csv', 'data/covid_data/hq_pc3.csv',
                    'data/covid_data/hq_ha1e.csv', 'data/covid_data/hq_yapc.csv',
                    'data/covid_data/hq_hela.csv', 'data/covid_data/hq_a549.csv']
    hq_drug = get_hq_drug_list('data/pert_id_0_7_a549.txt')
    drug_sig, exp_info = get_ground_truth_signature('data/signature_0_7.csv', filter)
    generate_high_quality_signature(hq_drug, filter, drug_sig, exp_info, hq_file_list)
