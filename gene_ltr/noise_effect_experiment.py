import shlex
import subprocess
import random
import os

eid = str(random.randint(1, 1000000))

if not os.path.exists('output'):
    os.makedirs('output')
# vanilla neural network
# with open('output/' + eid + '_base_model_regression_pubchem__0_5.txt', 'w') as f:
#     subprocess.check_call(shlex.split('python main_base_model_regression.py --drug_file '
#                                       '"data/drugs_pubchem.csv" --fp_type "pubchem" '
#                                       '--train_file "data/signature__0_5.csv"'), stdout=f)
# with open('output/' + eid + '_base_model_regression_pubchem__0_1.txt', 'w') as f:
#     subprocess.check_call(shlex.split('python main_base_model_regression.py --drug_file '
#                                       '"data/drugs_pubchem.csv" --fp_type "pubchem" '
#                                       '--train_file "data/signature__0_1.csv"'), stdout=f)
# with open('output/' + eid + '_base_model_regression_pubchem_0_1.txt', 'w') as f:
#     subprocess.check_call(shlex.split('python main_base_model_regression.py --drug_file '
#                                       '"data/drugs_pubchem.csv" --fp_type "pubchem" '
#                                       '--train_file "data/signature_0_1.csv"'), stdout=f)
# with open('output/' + eid + '_base_model_regression_pubchem_0_3.txt', 'w') as f:
#     subprocess.check_call(shlex.split('python main_base_model_regression.py --drug_file '
#                                       '"data/drugs_pubchem.csv" --fp_type "pubchem" '
#                                       '--train_file "data/signature_0_3.csv"'), stdout=f)
# with open('output/' + eid + '_base_model_regression_pubchem_0_5.txt', 'w') as f:
#     subprocess.check_call(shlex.split('python main_base_model_regression.py --drug_file '
#                                       '"data/drugs_pubchem.csv" --fp_type "pubchem" '
#                                       '--train_file "data/signature_0_5.csv"'), stdout=f)

# kNN
with open('output/' + eid + '_knn_pubchem__0_5.txt', 'w') as f:
    subprocess.check_call(shlex.split('python main_knn.py --train_file "data/signature__0_5.csv"'), stdout=f)
with open('output/' + eid + '_knn_pubchem__0_1.txt', 'w') as f:
    subprocess.check_call(shlex.split('python main_knn.py --train_file "data/signature__0_1.csv"'), stdout=f)
with open('output/' + eid + '_knn_pubchem_0_1.txt', 'w') as f:
    subprocess.check_call(shlex.split('python main_knn.py --train_file "data/signature__0_1.csv"'), stdout=f)
with open('output/' + eid + '_knn_pubchem_0_3.txt', 'w') as f:
    subprocess.check_call(shlex.split('python main_knn.py --train_file "data/signature_0_3.csv"'), stdout=f)
with open('output/' + eid + '_knn_pubchem_0_5.txt', 'w') as f:
    subprocess.check_call(shlex.split('python main_knn.py --train_file "data/signature_0_5.csv"'), stdout=f)
